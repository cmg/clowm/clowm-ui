# CloWM-UI

## Description

This is the Frontend to manage your S3 Buckets and start Workflows.

## Environment Variables

The docker container replaces them in the `env.template.js` file and moves that file to the same location as
the `index.html`.
When accessing the website, these variables will be loaded dynamically into the application.

| Variable         | Default | Value    | Description                                      |
|------------------|---------|----------|--------------------------------------------------|
| `API_BASE_URL`   | unset   | HTTP URL | Base URL for the CloWM Service API               |
| `S3_URL`         | unset   | HTTP URL | URL of the S3 storage to interact with           |
| `DEV_SYSTEM`     | `false` | boolean  | Flag if the service is installed on a Dev system |
| `MATOMO_HOST`    | unset   | HTTP URL | URL to a Matomo instance to enable tracking      |
| `MATOMO_SITE_ID` | unset   | integer  | Site id of the website on the Matomo instance    |

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information.
