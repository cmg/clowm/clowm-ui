# Development Setup

## Install Dependencies
Install dependencies with `npm`.
```shell
npm install
```

## Environment Setup
This has to be done only once. Export the environment variables with the appropriate values and use the `envsubst` command
to populate the template and create the file the `public/env.js`
```shell
export API_BASE_URL=http://localhost:9999/api
export S3_URL=http://localhost:9998
envsubst < src/assets/env.template.js > public/env.js
```
## Backend Setup
Clone the [repository](https://gitlab.ub.uni-bielefeld.de/denbi/object-storage-access) for the backend and set up the
service for developing.

## Run linter and type check
```shell
npm run lint
npm run type-check
```

## Run Dev Server
Start the dev service with `npm`
```shell
npm run dev
```

## Create axios client
To create the axios client from the OpenAPI definition of the backend, start the backend and execute the following commands
```shell
curl -o openapi-clowm.json -L http://localhost:9999/api/openapi.json
npm run generate-client
```
