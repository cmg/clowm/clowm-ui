## Version 1.0.1

### General
* Improved UI when browsing a non-existing bucket or a bucket without permissions for
* Improved UI when not browsing a bucket
* Load environment variables dynamically instead of injecting them into the build process.
You can now restart the docker container with new env variables without rebuilding it.
The browser cache for this file is disabled to immediately see the changes #27
### Fixes
* Show correct number of visible objects when searching for objects #28
### Internal
* Use a central repository for the buckets and current user permissions to simplify complex interactions between components #29
* Add healthcheck to docker container 
