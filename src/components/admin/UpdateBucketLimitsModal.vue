<script setup lang="ts">
import { computed, onMounted, reactive, ref, watch } from "vue";
import type { BucketOut, BucketSizeLimits } from "@/client/types.gen";
import BootstrapModal from "@/components/modals/BootstrapModal.vue";
import { filesize } from "filesize";
import { useBucketStore } from "@/stores/buckets";
import BootstrapToast from "@/components/BootstrapToast.vue";
import { Modal, Toast, Tooltip } from "bootstrap";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";

const props = defineProps<{
  modalId: string;
  bucket: BucketOut;
}>();

type DataUnits = "MiB" | "GiB" | "TiB" | "KB" | "MB" | "GB" | "TB";
const unitCalculatorState = reactive<{
  selectedUnit: DataUnits;
  input: number;
}>({
  selectedUnit: "GB",
  input: 1,
});
const unitCalculatorResult = computed<number>(() => {
  switch (unitCalculatorState.selectedUnit) {
    case "KB":
      return 1000 * (unitCalculatorState.input / 1024);
    case "MB":
      return 1000000 * (unitCalculatorState.input / 1024);
    case "GB":
      return 1000000000 * (unitCalculatorState.input / 1024);
    case "TB":
      return 1000000000000 * (unitCalculatorState.input / 1024);
    case "MiB":
      return 1024 * unitCalculatorState.input;
    case "GiB":
      return 1048576 * unitCalculatorState.input;
    case "TiB":
      return 1073741824 * unitCalculatorState.input;
    default:
      return 1;
  }
});

const sizeState = reactive<{
  limits: BucketSizeLimits;
  loading: boolean;
}>({
  limits: {
    size_limit: undefined,
    object_limit: undefined,
  },
  loading: false,
});

const emit = defineEmits<{
  (e: "updated-limits", bucket: BucketOut): void;
}>();

let successToast: Toast;
let modal: Modal | null = null;
const sizeForm = ref<HTMLFormElement | undefined>(undefined);

watch(
  () => props.bucket,
  (newBucket, oldBucket) => {
    if (newBucket.name != oldBucket.name) {
      sizeState.limits.size_limit = newBucket.size_limit;
      sizeState.limits.object_limit = newBucket.object_limit;
    }
  },
);

const bucketRepository = useBucketStore();

const randomIDSuffix = Math.random().toString(16).substring(2, 8);
const formId = `update-bucket-limits-modal-${randomIDSuffix}`;
const successToastId = `update-bucket-limits-success-toast-${randomIDSuffix}`;

function updateLimits() {
  if (sizeForm.value?.checkValidity()) {
    sizeState.loading = true;
    bucketRepository
      .updateBucketLimits(
        props.bucket.name,
        sizeState.limits.size_limit ? sizeState.limits.size_limit : null,
        sizeState.limits.object_limit ? sizeState.limits.object_limit : null,
      )
      .then((bucket) => {
        successToast?.show();
        emit("updated-limits", bucket);
        modal?.hide();
      })
      .finally(() => {
        sizeState.loading = false;
      });
  }
}

function copyFromCalculatorToForm() {
  sizeState.limits.size_limit = Math.round(unitCalculatorResult.value);
}

onMounted(() => {
  successToast = Toast.getOrCreateInstance(`#${successToastId}`);
  modal = Modal.getOrCreateInstance(`#${props.modalId}`);
  new Tooltip(`#copy-to-size-limit-form-${randomIDSuffix}`);
});
</script>

<template>
  <bootstrap-toast :toast-id="successToastId">
    Updated limits for bucket {{ bucket.name }}
  </bootstrap-toast>
  <bootstrap-modal
    :modal-id="props.modalId"
    :static-backdrop="true"
    modal-label="Update bucket quotas"
    size-modifier-modal="lg"
  >
    <template #header
      >Update quotas for bucket <i>{{ bucket.name }}</i></template
    >
    <template #body>
      <form :id="formId" ref="sizeForm" @submit.prevent="updateLimits()">
        <div class="row mb-2">
          <div class="col-5">
            <label :for="`#size-limit-${randomIDSuffix}`" class="form-label"
              >Size quota:
              <template v-if="sizeState.limits.size_limit"
                >{{ filesize(1024 * sizeState.limits.size_limit) }} or
                {{ filesize(1024 * sizeState.limits.size_limit, { base: 2 }) }}
              </template>
              <template v-else>No quota</template>
            </label>
            <div class="d-flex align-content-center">
              <input
                :id="`size-limit-${randomIDSuffix}`"
                v-model="sizeState.limits.size_limit"
                style="text-align: right"
                type="number"
                placeholder="No quota"
                min="1"
                max="4294967295"
                step="1"
                class="form-control"
                aria-label="size limit"
              />
              <div class="ms-1 fs-5">KiB</div>
            </div>
          </div>
          <div class="col-6 offset-md-1">
            <label :for="`#object-limit-${randomIDSuffix}`" class="form-label"
              >Number of objects quota
            </label>
            <input
              :id="`object-limit-${randomIDSuffix}`"
              v-model="sizeState.limits.object_limit"
              type="number"
              class="form-control"
              placeholder="No quota"
              min="1"
              max="4294967295"
              step="1"
              aria-label="object limit"
            />
          </div>
        </div>
      </form>
      <h4 class="mt-4">Unit calculator</h4>
      <div class="d-flex justify-content-center align-content-center">
        <div class="input-group w-fit">
          <input
            v-model="unitCalculatorState.input"
            class="form-control"
            style="text-align: right"
            type="number"
            min="1"
            max="1000"
            step="0.01"
          />
          <select
            v-model="unitCalculatorState.selectedUnit"
            class="form-select"
          >
            <option>KB</option>
            <option>MiB</option>
            <option>MB</option>
            <option>GiB</option>
            <option>GB</option>
            <option>TiB</option>
            <option>TB</option>
          </select>
        </div>
        <div class="fs-4 mx-4">
          <font-awesome-icon icon="fa-solid fa-arrow-right-long" />
        </div>
        <div class="d-flex align-content-center">
          <div class="input-group">
            <span
              :id="`copy-to-size-limit-form-${randomIDSuffix}`"
              class="input-group-text hover-info cursor-pointer"
              data-bs-toggle="tooltip"
              data-bs-title="Copy to form"
              @click="copyFromCalculatorToForm()"
              ><font-awesome-icon icon="fa-solid fa-copy"
            /></span>
            <input
              style="text-align: right"
              type="number"
              class="form-control"
              readonly
              :value="unitCalculatorResult"
            />
          </div>
          <div class="ms-1 fs-5">KiB</div>
        </div>
      </div>
    </template>
    <template #footer>
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
        Close
      </button>
      <button
        type="submit"
        :form="formId"
        class="btn btn-primary"
        :disabled="sizeState.loading"
      >
        Save
      </button>
    </template>
  </bootstrap-modal>
</template>

<style scoped></style>
