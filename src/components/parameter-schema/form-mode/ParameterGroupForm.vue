<script setup lang="ts">
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { computed, onMounted, type PropType } from "vue";
import MarkdownRenderer from "@/components/MarkdownRenderer.vue";
import {
  type FlatWorkflowParameters,
  type ParameterCombinationDependencies,
} from "@/types/WorkflowParameters";
import ParameterInput from "@/components/parameter-schema/form-mode/ParameterInput.vue";
import { Tooltip } from "bootstrap";
import { md5 } from "@/utils/md5";
import {
  ParameterVisibility,
  type ParameterVisibilityMapping,
} from "@/client/types.gen.ts";

const model = defineModel<FlatWorkflowParameters>({ required: true });

const groupId = Math.random().toString(16).substring(2, 8);

const props = defineProps({
  parameterGroupName: {
    type: String,
    required: true,
  },

  resourceParameters: {
    type: Array as PropType<string[]>,
    required: false,
  },
  mapping: Object as PropType<Record<string, Record<string, string | number>>>,
  schemaProperties: {
    type: Object,
    required: true,
  },
  parameterVisibility: {
    type: Object as PropType<ParameterVisibilityMapping>,
    required: true,
  },
  currentVisibility: {
    type: Number,
    default: ParameterVisibility.SIMPLE,
  },
});
const title = computed<string>(() => parameterGroup.value["title"]);
const icon = computed<string>(() => parameterGroup.value["fa_icon"]);
const description = computed<string>(() => parameterGroup.value["description"]);
const groupHidden = computed<boolean>(() =>
  Object.keys(parameters.value).reduce(
    (acc: boolean, val: string) => acc && !parameterVisible(val),
    true,
  ),
);
const parameters = computed<Record<string, never>>(
  () => parameterGroup.value["properties"],
);

const parameterGroup = computed(
  () => props.schemaProperties[props.parameterGroupName],
);

// check if the oneOf constraint is not violated
function oneOfValidity(paramName: string): string | undefined {
  if (
    parameterOneOfDependenciesPerParameter.value[paramName] != undefined &&
    (parameterOneOfDependenciesPerParameter.value[paramName].group.reduce(
      (acc, param) => acc || model.value[param] != undefined,
      model.value[paramName] != undefined,
    )
      ? 1
      : 0) +
      parameterOneOfDependenciesPerParameter.value[paramName].dependencies
        .flat()
        .reduce((acc, b) => acc + (model.value[b] != undefined ? 1 : 0), 0) >
      1
  ) {
    return "oneOf violated";
  }
  return undefined;
}

/*
Object with parameter name as key and list of parameters that make the parameter required if they are set as value
JSONSchema DependentRequired
 */
const parameterDependenciesPerParameter = computed<Record<string, string[]>>(
  () => {
    const r: Record<string, string[]> = {};
    for (const groupName of Object.keys(props.schemaProperties)) {
      const dep =
        props.schemaProperties[groupName]["dependentRequired"] ??
        props.schemaProperties[groupName]["dependencies"] ??
        {};
      for (const paramName of Object.keys(dep)) {
        for (const depParam of dep[paramName]) {
          if (parameters.value[depParam] != undefined) {
            if (r[depParam] == undefined) {
              r[depParam] = [paramName];
            } else {
              r[depParam].push(depParam);
            }
          }
        }
      }
    }
    return r;
  },
);

// Object with parameter name as key and boolean flag if the parameter is required as value
const parameterRequired = computed<Record<string, boolean>>(() => {
  const r: Record<string, boolean> = {};
  for (const parameterName of Object.keys(parameters.value)) {
    r[parameterName] =
      parameterDirectlyRequired(parameterName) || // parameter is directly required
      parameterDependentRequired(parameterName) || // parameter is required depending on other parameters
      parameterAnyOfRequired(parameterName) || // parameter is required in a anyOf parameter group
      parameterOneOfRequired(parameterName); // parameter is required in a oneOf parameter group
  }
  return r;
});

// evaluate if the parameter is required directly
function parameterDirectlyRequired(parameterName: string): boolean {
  return parameterGroup.value["required"]?.includes(parameterName) ?? false;
}

// evaluate if a parameter is required due to a anyOf constraint
function parameterDependentRequired(parameterName: string): boolean {
  return parameterDependenciesPerParameter.value[parameterName] // parameter is required when another parameter is set
    ?.reduce(
      (acc: boolean, param: string) => acc || model.value[param] != undefined,
      false,
    );
}

// evaluate of a parameter is required du to a dependentRequired constraint
function parameterAnyOfRequired(parameterName: string): boolean {
  return (
    parameterAnyOfDependenciesPerParameter.value[parameterName] != undefined &&
    parameterAnyOfDependenciesPerParameter.value[parameterName].dependencies
      .flat()
      .reduce(
        (acc, groupParam) => acc && model.value[groupParam] == undefined,
        true,
      ) &&
    (model.value[parameterName] == undefined ||
      parameterAnyOfDependenciesPerParameter.value[parameterName].group.reduce(
        (acc, groupParam) => acc && model.value[groupParam] == undefined,
        true,
      ))
  );
}

// evaluate of a parameter is required du to a dependentRequired constraint
function parameterOneOfRequired(parameterName: string): boolean {
  return (
    parameterOneOfDependenciesPerParameter.value[parameterName] != undefined &&
    parameterOneOfDependenciesPerParameter.value[parameterName].dependencies
      .flat()
      .reduce(
        (acc, groupParam) => acc && model.value[groupParam] == undefined,
        true,
      ) &&
    (model.value[parameterName] == undefined ||
      parameterOneOfDependenciesPerParameter.value[parameterName].group.reduce(
        (acc, groupParam) => acc && model.value[groupParam] == undefined,
        true,
      ))
  );
}

// object with parameter name as key and anyOf dependencies as value
const parameterAnyOfDependenciesPerParameter = computed<
  Record<string, ParameterCombinationDependencies>
>(() => {
  const b: string[][] =
    parameterGroup.value["anyOf"]
      ?.map((b: Record<string, string[]>) => b["required"] ?? [])
      ?.filter((b: string[]) => b.length > 0) ?? [];
  const r: Record<string, ParameterCombinationDependencies> = {};
  for (const paramName of b.flat()) {
    r[paramName] = {
      group:
        b.find((a) => a.includes(paramName))?.filter((a) => a != paramName) ??
        [],
      dependencies: b.filter((a) => !a.includes(paramName)),
    };
  }
  return r;
});

// object with parameter name as key and oneOf dependencies as value
const parameterOneOfDependenciesPerParameter = computed<
  Record<string, ParameterCombinationDependencies>
>(() => {
  const b: string[][] =
    parameterGroup.value["oneOf"]
      ?.map((b: Record<string, string[]>) => b["required"] ?? [])
      ?.filter((b: string[]) => b.length > 0) ?? [];
  const r: Record<string, ParameterCombinationDependencies> = {};
  for (const paramName of b.flat()) {
    r[paramName] = {
      group:
        b.find((a) => a.includes(paramName))?.filter((a) => a != paramName) ??
        [],
      dependencies: b.filter((a) => !a.includes(paramName)),
    };
  }
  return r;
});

// object with parameter name as key and tooltip string for dependentRequired label as value
const dependentTooltip = computed<Record<string, string>>(() => {
  const r: Record<string, string> = {};
  for (const paramName of Object.keys(parameters.value)) {
    let labelText = "";
    if (parameterDependenciesPerParameter.value[paramName] != undefined) {
      labelText +=
        "Required when the following parameters are set: <ul class='ps-4 mb-0 text-start'>" +
        parameterDependenciesPerParameter.value[paramName]
          .map((a) => "<li>--" + a + "</li>")
          .join("") +
        "</ul></li>";
    }
    if (labelText.length > 0) {
      r[paramName] = labelText;
    }
  }
  return r;
});

// object with parameter name as key and tooltip string for anyOf label as value
const oneOfTooltip = computed<Record<string, string>>(() => {
  const r: Record<string, string> = {};
  for (const paramName of Object.keys(parameters.value)) {
    if (parameterOneOfDependenciesPerParameter.value[paramName] != undefined) {
      let labelText = `Set exactly one of the following parameters:<ul class="text-start mb-0"><li>--${paramName}`;
      if (
        parameterOneOfDependenciesPerParameter.value[paramName].group.length > 0
      ) {
        labelText +=
          ", " +
          parameterOneOfDependenciesPerParameter.value[paramName].group
            .map((a) => "--" + a)
            .join(",");
      }
      labelText += "</li>";
      labelText +=
        parameterOneOfDependenciesPerParameter.value[paramName].dependencies
          .map((a) => "<li>" + a.map((b) => "--" + b).join(",") + "</li>")
          .join("") + "</ul>";
      r[paramName] = labelText;
    }
  }
  return r;
});

// object with parameter name as key and tooltip string for anyOf label as value
const anyOfTooltip = computed<Record<string, string>>(() => {
  const r: Record<string, string> = {};
  for (const paramName of Object.keys(parameters.value)) {
    if (parameterAnyOfDependenciesPerParameter.value[paramName] != undefined) {
      let labelText = `Set one or multiple of the following parameters:<ul class="text-start mb-0"><li>--${paramName}`;
      if (
        parameterAnyOfDependenciesPerParameter.value[paramName].group.length > 0
      ) {
        labelText +=
          ", " +
          parameterAnyOfDependenciesPerParameter.value[paramName].group
            .map((a) => "--" + a)
            .join(",");
      }
      labelText += "</li>";
      labelText +=
        parameterAnyOfDependenciesPerParameter.value[paramName].dependencies
          .map((a) => "<li>" + a.map((b) => "--" + b).join(",") + "</li>")
          .join("") + "</ul>";
      r[paramName] = labelText;
    }
  }
  return r;
});
// object with parameter name as key and id for html attributes as value
const parameterIds = computed<Record<string, string>>(() => {
  const r: Record<string, string> = {};
  for (const parameterName of Object.keys(parameters.value)) {
    r[parameterName] = md5(parameterName);
  }
  return r;
});

function parameterVisible(paramName: string): boolean {
  return props.parameterVisibility[paramName] <= props.currentVisibility;
}

onMounted(() => {
  document
    ?.querySelector(`#group-${groupId}`)
    ?.querySelectorAll("[data-bs-toggle=tooltip]")
    .forEach((tooltipTriggerEl) => {
      Tooltip.getOrCreateInstance(tooltipTriggerEl);
    });
});
</script>

<template>
  <div :id="`group-${groupId}`" class="card mb-3" :hidden="groupHidden">
    <h3 :id="props.parameterGroupName" class="card-header">
      <font-awesome-icon v-if="icon" :icon="icon" class="me-2" />
      {{ title }}
    </h3>
    <div class="card-body">
      <h5 v-if="description" class="card-title">{{ description }}</h5>
      <template
        v-for="(parameter, parameterName) in parameters"
        :key="parameterName"
      >
        <div :hidden="!parameterVisible(parameterName)">
          <code
            class="p-2 rounded-top border-bottom-0 border bg-secondary-subtle border-secondary"
          >
            --{{ parameter["name"] ?? parameterName }}
          </code>
          <span
            v-if="parameterDirectlyRequired(parameterName)"
            class="rounded p-1 bg-warning ms-2"
          >
            required
          </span>
          <span
            v-if="anyOfTooltip[parameterName] != undefined"
            class="rounded p-1 bg-info ms-2 label"
            data-bs-toggle="tooltip"
            data-bs-placement="bottom"
            :data-bs-title="anyOfTooltip[parameterName]"
            data-bs-custom-class="parameter-form-tooltip"
            data-bs-html="true"
            >anyOf</span
          >
          <span
            v-if="oneOfTooltip[parameterName] != undefined"
            class="rounded p-1 bg-info ms-2 label"
            data-bs-toggle="tooltip"
            data-bs-placement="bottom"
            :data-bs-title="oneOfTooltip[parameterName]"
            data-bs-custom-class="parameter-form-tooltip"
            data-bs-html="true"
            >oneOf</span
          >
          <span
            v-if="dependentTooltip[parameterName] != undefined"
            class="rounded p-1 bg-info ms-2 label"
            data-bs-toggle="tooltip"
            data-bs-placement="bottom"
            :data-bs-title="dependentTooltip[parameterName]"
            data-bs-custom-class="parameter-form-tooltip"
            data-bs-html="true"
          >
            dependency
          </span>
          <div
            class="input-group"
            :class="{ 'mb-3': parameter['description'] == undefined }"
          >
            <span
              v-if="parameter['fa_icon']"
              class="input-group-text border-end-0 border border-secondary"
            >
              <font-awesome-icon :icon="parameter['fa_icon']" />
            </span>
            <parameter-input
              :id="parameterIds[parameterName]"
              v-model="model[parameterName]"
              :parameter="parameter"
              :required="parameterRequired[parameterName]"
              border="secondary"
              :resource-parameter="
                props.resourceParameters?.includes(parameterName)
              "
              :allow-raw="props.currentVisibility > ParameterVisibility.SIMPLE"
              :mapping="props.mapping?.[parameterName]"
              :custom-validity="oneOfValidity(parameterName)"
            />
            <span
              v-if="parameter['help_text']"
              class="input-group-text cursor-pointer px-2 border border-secondary"
              data-bs-toggle="collapse"
              :data-bs-target="'#help-collapse-' + parameterIds[parameterName]"
              aria-expanded="false"
              :aria-controls="'help-collapse-' + parameterIds[parameterName]"
            >
              <font-awesome-icon
                class="cursor-pointer"
                icon="fa-solid fa-circle-question"
              />
            </span>
          </div>
          <div v-if="parameter['description']" class="d-inline-block">
            <markdown-renderer :markdown="parameter['description']" />
          </div>
          <div
            v-if="parameter['help_text']"
            :id="'help-collapse-' + parameterIds[parameterName]"
            class="collapse"
          >
            <div class="p-2 pb-0 mx-2 mb-3 flex-shrink-1 border rounded">
              <markdown-renderer
                class="helpTextCode"
                :markdown="parameter['help_text']"
              />
              <p v-if="parameter['pattern']">
                Pattern: <code>{{ parameter["pattern"] }}</code>
              </p>
            </div>
          </div>
        </div>
      </template>
    </div>
  </div>
</template>

<style>
div.card-body {
  backdrop-filter: brightness(1.2);
}

span.cursor-pointer:hover {
  color: var(--bs-info);
}

.label {
  cursor: help;
}
</style>
