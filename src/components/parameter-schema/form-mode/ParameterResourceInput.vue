<script setup lang="ts">
import { ResourceVersionStatus } from "@/client";
import { computed, onMounted, reactive, ref, watch } from "vue";
import { useResourceStore } from "@/stores/resources";
import type { ExtendedColors, SizeModifierType } from "@/types/PropTypes";

const model = defineModel({
  required: true,
});

const resourceRegex = /CLDB-([\da-f]{32})\/(latest|[\da-f]{32})([/\S]*)/g;

const props = defineProps<{
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parameter: Record<string, any>;
  required?: boolean;
  sizeModifier?: SizeModifierType;
  border?: ExtendedColors;
  allowRaw?: boolean;
  id: string;
  customValidity?: string;
}>();

const emit = defineEmits<{
  (e: "switch-to-raw"): void;
}>();

const resourceRepository = useResourceStore();

type ResourcePath = {
  resource_id: string;
  resource_version_id: string;
  suffix?: string;
};

const resource = reactive<ResourcePath>({
  resource_id: "",
  resource_version_id: "",
  suffix: undefined,
});

const helpTextPresent = computed<boolean>(() => props.parameter["help_text"]);

const baseDynamicClass = computed<string[]>(() =>
  props.border ? ["border", `border-${props.border}`] : [],
);

const selectDynamicClass = computed<string[]>(() => {
  const cssClasses = [...baseDynamicClass.value];
  if (props.sizeModifier) {
    cssClasses.push(`form-select-${props.sizeModifier}`);
  }
  return cssClasses;
});
const inputDynamicClass = computed<string[]>(() => {
  const cssClasses = [...baseDynamicClass.value];
  if (props.sizeModifier) {
    cssClasses.push(`form-control-${props.sizeModifier}`);
  }
  if (!helpTextPresent.value && !props.allowRaw) {
    cssClasses.push("rounded-end");
  }
  return cssClasses;
});

function updateResourceId(rid: string) {
  resource.resource_id = rid;
  resource.resource_version_id = "";
  model.value = translateToModel();
}

function updateResourceVersionId(rvid: string) {
  resource.resource_version_id = rvid;
  model.value = translateToModel();
  resourceRepository.fetchResourceTree(resource.resource_id, rvid);
}

watch(model, (newVal, oldVal) => {
  if (newVal != oldVal && newVal !== translateToModel()) {
    /* eslint-disable @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    parseModel(newVal);
  }
});

watch(
  () => props.customValidity,
  (newVal, oldVal) => {
    if (newVal != oldVal) {
      resourceSelect.value?.setCustomValidity(newVal ?? "");
      versionSelect.value?.setCustomValidity(newVal ?? "");
      pathInput.value?.setCustomValidity(newVal ?? "");
    }
  },
);

const resourceSelect = ref<HTMLSelectElement | undefined>(undefined);
const versionSelect = ref<HTMLSelectElement | undefined>(undefined);
const pathInput = ref<HTMLInputElement | undefined>(undefined);

function parseModel(val?: string) {
  if (val == undefined || val.length === 0) {
    Object.assign(resource, {
      resource_id: "",
      resource_version_id: "",
      suffix: undefined,
    });
  } else {
    const match = resourceRegex.exec(val);
    if (match) {
      const tempResource: ResourcePath = {
        resource_id: "",
        resource_version_id: "",
      };
      tempResource.resource_id = hexToUUID(match[1]);
      tempResource.suffix = match[3];
      tempResource.resource_version_id =
        match[2].length === 32
          ? hexToUUID(match[2])
          : resourceRepository.getLatestVersion(tempResource.resource_id);
      if (
        resourceRepository.resourceMapping[tempResource.resource_id] ==
          undefined ||
        resourceRepository.versionMapping[tempResource.resource_version_id] ==
          undefined
      ) {
        // Missing resource
        emit("switch-to-raw");
        return;
      }
      Object.assign(resource, tempResource);
    } else {
      // Not resource path
      emit("switch-to-raw");
    }
  }
}

function hexToUUID(hex?: string): string {
  if (hex) {
    return `${hex.slice(0, 8)}-${hex.slice(8, 12)}-${hex.slice(12, 16)}-${hex.slice(16, 20)}-${hex.slice(20, 32)}`;
  }
  return "";
}

watch(
  () => resource.suffix,
  (newVal, oldVal) => {
    if (newVal !== oldVal) {
      model.value = translateToModel();
    }
  },
);

function translateToModel(): string {
  if (resource.resource_version_id.length === 0) {
    return "";
  }
  let val =
    resourceRepository.versionMapping[resource.resource_version_id]
      ?.cluster_path ?? "";
  if (resource.suffix != undefined && val.length > 0) {
    val = val + resource.suffix;
  }
  return val;
}

onMounted(() => {
  /* eslint-disable @typescript-eslint/ban-ts-comment */
  // @ts-ignore
  parseModel(model.value);
  resourceSelect.value?.setCustomValidity(props.customValidity ?? "");
  versionSelect.value?.setCustomValidity(props.customValidity ?? "");
  pathInput.value?.setCustomValidity(props.customValidity ?? "");
});
</script>

<template>
  <select
    :id="id + 'Resource'"
    ref="resourceSelect"
    class="form-select"
    :class="selectDynamicClass"
    :required="props.required"
    :value="resource.resource_id"
    @change="
      (event) => updateResourceId((event.target as HTMLSelectElement)?.value)
    "
  >
    >
    <option selected disabled value="">Please select a resource</option>
    <option
      v-for="availableResource in resourceRepository.resources"
      :key="availableResource.resource_id"
      :value="availableResource.resource_id"
    >
      {{ availableResource.name }}
    </option>
  </select>
  <select
    :id="id + 'ResourceVersion'"
    ref="versionSelect"
    class="form-select"
    :class="selectDynamicClass"
    :required="resource.resource_id.length > 0"
    :value="resource.resource_version_id"
    :disabled="resource.resource_id.length === 0"
    @change="
      (event) =>
        updateResourceVersionId((event.target as HTMLSelectElement)?.value)
    "
  >
    <option disabled selected value="">Please select a version</option>
    <option
      v-for="version in resourceRepository.resourceMapping[resource.resource_id]
        ?.versions ?? []"
      :key="version.resource_version_id"
      :value="version.resource_version_id"
    >
      {{ version.release }}
      {{ version.status === ResourceVersionStatus.LATEST ? "- Latest" : "" }}
    </option>
  </select>
  <input
    :id="id + 'ResourcePath'"
    ref="pathInput"
    v-model="resource.suffix"
    type="text"
    class="form-control"
    :class="inputDynamicClass"
    placeholder="/optional/path/in/resource/..."
    minlength="2"
    maxlength="256"
    pattern="\/\S*"
    :list="'resource-tree-options-' + id"
  />
  <datalist :id="'resource-tree-options-' + id">
    <option
      v-for="file in resourceRepository.resourceTreeList[
        resource.resource_version_id
      ] ?? []"
      :key="file"
      :value="file"
    />
  </datalist>
  <button
    v-if="allowRaw"
    type="button"
    class="btn btn-outline-secondary"
    @click="emit('switch-to-raw')"
  >
    Raw
  </button>
</template>

<style scoped></style>
