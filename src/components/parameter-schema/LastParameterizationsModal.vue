<script setup lang="ts">
import BootstrapModal from "@/components/modals/BootstrapModal.vue";
import { computed, nextTick, onMounted, reactive, ref } from "vue";
import { useUserStore } from "@/stores/users.ts";
import { useWorkflowExecutionStore } from "@/stores/workflowExecutions.ts";
import type { WorkflowExecutionOut } from "@/client";
import { useNameStore } from "@/stores/names.ts";
import dayjs from "dayjs";
import type {
  FlatWorkflowParameters,
  NestedWorkflowParameters,
  TemporaryParams,
} from "@/types/WorkflowParameters.ts";
import { flattenParameters } from "@/utils/Workflow.ts";
import ParameterTable from "@/components/workflows/ParameterTable.vue";
import { type RouteLocationRaw, useRouter } from "vue-router";
import { Modal } from "bootstrap";

const props = defineProps<{
  modalId: string;
  workflowId?: string;
}>();

const userRepository = useUserStore();
const executionRepository = useWorkflowExecutionStore();
const nameRepository = useNameStore();
const router = useRouter();

let parameterModal: Modal | null = null;

let loadingObserver: IntersectionObserver;
const endOfTableElement = ref<HTMLTableCaptionElement | undefined>(undefined);

const formState = reactive<{
  loading: boolean;
  executions: WorkflowExecutionOut[];
  params?: NestedWorkflowParameters;
  selectedExecution?: WorkflowExecutionOut;
}>({
  loading: false,
  executions: [],
  params: undefined,
  selectedExecution: undefined,
});

const emit = defineEmits<{
  (e: "parameters-selected", params: TemporaryParams): void;
}>();

const statusToColorMapping = {
  PENDING: "bg-warning",
  SCHEDULED: "bg-warning",
  RUNNING: "bg-info",
  CANCELED: "bg-danger",
  SUCCESS: "bg-success",
  ERROR: "bg-danger",
};

const parameterTableParams = computed<TemporaryParams>(() => {
  return {
    params: formState.params,
    metaParams: {
      notes: formState.selectedExecution?.notes,
      provenance_s3_path: formState.selectedExecution?.provenance_s3_path,
      debug_s3_path: formState.selectedExecution?.debug_s3_path,
      logs_s3_path: formState.selectedExecution?.logs_s3_path,
    },
  };
});

const executionIterator = ref<
  AsyncGenerator<WorkflowExecutionOut[], void, never> | undefined
>(undefined);

function loadExecution() {
  loadingObserver.disconnect();
  formState.loading = true;
  formState.executions = [];
  executionIterator.value = executionRepository.fetchExecutionsRaw(
    userRepository.currentUID,
    undefined,
    undefined,
    props.workflowId,
    undefined,
    undefined,
    30,
  );
  loadNextExecutions();
}

function loadNextExecutions() {
  formState.loading = true;
  executionIterator.value
    ?.next()
    .then((executions) => {
      if (executions.done ?? false) {
        return;
      }
      formState.executions.push(...executions.value);
      nextTick(() => {
        if (endOfTableElement.value != undefined) {
          loadingObserver.observe(endOfTableElement.value);
        }
      });
    })
    .finally(() => {
      formState.loading = false;
    });
}

const flatParameters = computed<FlatWorkflowParameters | undefined>(() => {
  if (formState.params == undefined) {
    return undefined;
  }
  return flattenParameters(formState.params);
});

function emitParameters() {
  if (formState.params != undefined) {
    emit("parameters-selected", {
      params: formState.params,
      metaParams: {
        notes: formState.selectedExecution?.notes,
        provenance_s3_path: parentDir(
          formState.selectedExecution?.provenance_s3_path,
        ),
        debug_s3_path: parentDir(formState.selectedExecution?.debug_s3_path),
        logs_s3_path: parentDir(formState.selectedExecution?.logs_s3_path),
      },
    });
    formState.selectedExecution = undefined;
    formState.params = undefined;
  }
}

function loadParameters(execution: WorkflowExecutionOut) {
  executionRepository
    .fetchExecutionParameters(execution.execution_id)
    .then((params) => {
      formState.selectedExecution = execution;
      formState.params = params;
    });
}

function handleLinkClick(route: RouteLocationRaw) {
  parameterModal?.hide();
  router.push(route);
}

onMounted(() => {
  parameterModal = Modal.getOrCreateInstance("#" + props.modalId);
  loadingObserver = new IntersectionObserver(
    (entries) => {
      if (entries.length > 0 && entries[0].isIntersecting) {
        loadingObserver.unobserve(entries[0].target);
        loadNextExecutions();
      }
    },
    {
      threshold: 0,
      rootMargin: "0px 0px 150px 0px",
    },
  );
  loadExecution();
});

function parentDir(s3Path?: string | null): string | undefined {
  if (s3Path == undefined) {
    return undefined;
  }
  return s3Path.split("/").slice(0, -1).join("/");
}
</script>

<template>
  <bootstrap-modal
    :modal-id="props.modalId"
    :static-backdrop="true"
    modal-label="List Last Parameterization Modal"
    size-modifier-modal="xl"
  >
    <template #header
      >Last parameterization for
      <b>{{ nameRepository.getName(props.workflowId) }}</b></template
    >
    <template #body>
      <div class="overflow-y-auto" style="max-height: 40vh">
        <table ref="executionTable" class="table table-hover align-middle">
          <caption ref="endOfTableElement">
            Displaying
            {{
              formState.executions.length
            }}
            Workflow Execution
          </caption>
          <thead>
            <tr>
              <th scope="col">Version</th>
              <th scope="col">Status</th>
              <th scope="col">Started</th>
              <th scope="col">Duration</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <template v-if="formState.executions.length > 0">
              <tr
                v-for="execution in formState.executions"
                :id="`last-parameterization-execution-${execution.execution_id}`"
                :key="execution.execution_id"
              >
                <td>
                  <span v-if="execution.workflow_version_id">
                    {{ nameRepository.getName(execution.workflow_version_id) }}
                  </span>
                  <span v-else>Deleted Workflow</span>
                </td>
                <td>
                  <span
                    class="rounded-pill py-1 px-2 text-light"
                    :class="statusToColorMapping[execution.status]"
                  >
                    {{ execution.status }}</span
                  >
                </td>
                <td>
                  {{
                    dayjs
                      .unix(execution.start_time)
                      .format("MMM DD, YYYY HH:mm")
                  }}
                </td>
                <td>
                  <template v-if="execution.end_time">
                    {{
                      dayjs
                        .duration(
                          execution.end_time - execution.start_time,
                          "seconds",
                        )
                        .humanize()
                    }}
                  </template>
                  <template v-else>
                    {{ dayjs.unix(execution.start_time).toNow(true) }}
                  </template>
                </td>
                <td class="text-end">
                  <button
                    type="button"
                    class="btn btn-secondary"
                    @click="loadParameters(execution)"
                  >
                    View
                  </button>
                </td>
              </tr>
            </template>
            <tr v-else-if="!formState.loading">
              <td colspan="9" class="text-center">
                <i>No workflow executions</i>
              </td>
            </tr>
            <tr v-if="formState.loading">
              <td colspan="9" class="text-center">
                <div
                  class="spinner-border text-secondary spinner-border-sm"
                  role="status"
                >
                  <span class="visually-hidden">Loading...</span>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div
        v-if="flatParameters"
        class="overflow-y-auto mt-2"
        style="max-height: 50vh"
      >
        <h5>
          Parameters
          <span v-if="formState.selectedExecution != undefined"
            >from
            {{
              dayjs
                .unix(formState.selectedExecution.start_time)
                .format("MMM DD, YYYY HH:mm")
            }}</span
          >:
        </h5>
        <parameter-table
          :parameters="parameterTableParams"
          @link-clicked="handleLinkClick"
        />
      </div>
    </template>
    <template #footer>
      <button
        type="button"
        class="btn btn-success"
        data-bs-dismiss="modal"
        :disabled="formState.params == undefined"
        @click="emitParameters"
      >
        Load
      </button>
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
        Close
      </button>
    </template>
  </bootstrap-modal>
</template>

<style scoped></style>
