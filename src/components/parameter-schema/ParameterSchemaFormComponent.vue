<script setup lang="ts">
import { computed, onMounted, type PropType, reactive, ref, watch } from "vue";
import ParameterGroupForm from "@/components/parameter-schema/form-mode/ParameterGroupForm.vue";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import type { ValidateFunction } from "ajv";
import Ajv2020 from "ajv/dist/2020";
import Ajv2019 from "ajv/dist/2019";
import Ajv from "ajv/dist/ajv";
import ParameterInput from "@/components/parameter-schema/form-mode/ParameterInput.vue";
import { Toast } from "bootstrap";
import { useBucketStore } from "@/stores/buckets";
import { useS3KeyStore } from "@/stores/s3keys";
import BootstrapToast from "@/components/BootstrapToast.vue";
import { useResourceStore } from "@/stores/resources";
import type { ClowmInfo } from "@/types/ClowmInfo";
import {
  type FlatWorkflowParameters,
  type NestedWorkflowParameters,
  type TemporaryParams,
  type WorkflowMetaParameters,
} from "@/types/WorkflowParameters";
import { useWorkflowExecutionStore } from "@/stores/workflowExecutions";
import { NextflowVersion, ParameterVisibility } from "@/client/types.gen";
import type {
  ParameterExtensionOutput,
  ParameterVisibilityMapping,
} from "@/client/types.gen";
import { flattenParameters, nestParameters } from "@/utils/Workflow";
import GroupNav from "@/components/parameter-schema/GroupNav.vue";

const bucketRepository = useBucketStore();
const resourceRepository = useResourceStore();
const keyRepository = useS3KeyStore();
const executionRepository = useWorkflowExecutionStore();

// Props
// =============================================================================
const props = defineProps({
  schema: {
    type: Object,
  },
  clowmInfo: {
    type: Object as PropType<ClowmInfo>,
    required: false,
  },
  parameterExtension: {
    type: Object as PropType<ParameterExtensionOutput>,
    required: false,
  },
  loading: {
    type: Boolean,
  },
  allowNotes: {
    type: Boolean,
  },
  nextflowVersion: {
    type: Object as PropType<NextflowVersion>,
    required: true,
  },
  workflowId: {
    type: String,
    required: false,
  },
});

const emit = defineEmits<{
  (
    e: "start-workflow",
    parameters: NestedWorkflowParameters,
    metaParameters: WorkflowMetaParameters,
  ): void;
}>();

// Bootstrap Elements
// =============================================================================
let errorToast: Toast | null = null;
let parameterLoadToast: Toast | null = null;

// JSON Schema package
// =============================================================================
const ajvVersions = {
  "https://json-schema.org/draft/2020-12/schema": new Ajv2020({
    strict: false,
  }),
  "https://json-schema.org/draft/2019-09/schema": new Ajv2019({
    strict: false,
  }),
  "http://json-schema.org/draft-07/schema": new Ajv({
    strict: false,
  }),
};
let validateSchema: ValidateFunction;

// Reactive State
// =============================================================================
const launchForm = ref<HTMLFormElement | null>(null);

const formState = reactive<{
  formInput: FlatWorkflowParameters;
  validated: boolean;
  metaParameters: WorkflowMetaParameters;
  errorType?: string;
  viewMode: ParameterVisibility;
}>({
  formInput: {},
  validated: false,
  metaParameters: {
    logs_s3_path: undefined,
    debug_s3_path: undefined,
    provenance_s3_path: undefined,
    notes: undefined,
  },
  errorType: undefined,
  viewMode: ParameterVisibility.SIMPLE,
});

// Computed Properties
// =============================================================================
const schemaDefinitions = computed<Record<string, object>>(
  () => props.schema?.["definitions"] ?? props.schema?.["$defs"],
);

const defaultParameterVisibility = computed<ParameterVisibilityMapping>(() => {
  const mapping: ParameterVisibilityMapping = {};
  const groups: object = schemaDefinitions.value;
  for (const groupName of Object.keys(groups)) {
    /* eslint-disable @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    const group = groups[groupName];
    const anyOfDeps: string[] = (group["anyOf"] ?? [])
      .map((dep: Record<string, string[]>) => dep["required"])
      .flat();
    const oneOfDeps: string[] = (group["oneOf"] ?? [])
      .map((dep: Record<string, string[]>) => dep["required"])
      .flat();
    const requiredDeps: string[] = group["required"] ?? [];
    const temp: Record<string, string[]> =
      group["dependentRequired"] ?? group["dependencies"] ?? {};
    const parameterDeps: string[] = Object.keys(temp)
      .map((param) => temp[param])
      .flat();

    for (const paramName of Object.keys(group["properties"] ?? {})) {
      const param: { hidden?: boolean } = group["properties"][paramName];
      if (
        anyOfDeps.includes(paramName) ||
        oneOfDeps.includes(paramName) ||
        requiredDeps.includes(paramName) ||
        parameterDeps.includes(paramName)
      ) {
        mapping[paramName] = ParameterVisibility.SIMPLE;
      } else if (!(param.hidden ?? false)) {
        mapping[paramName] = ParameterVisibility.ADVANCED;
      } else {
        mapping[paramName] = ParameterVisibility.EXPERT;
      }
    }
  }
  for (const paramName of Object.keys(props.schema?.["properties"] ?? {})) {
    mapping[paramName] =
      (props.schema?.["properties"][paramName]["hidden"] ?? false)
        ? ParameterVisibility.EXPERT
        : ParameterVisibility.ADVANCED;
  }
  return mapping;
});

const currentParameterVisibility = computed<ParameterVisibilityMapping>(() =>
  props.parameterExtension?.parameter_visibility != undefined &&
  Object.keys(props.parameterExtension?.parameter_visibility).length > 0
    ? props.parameterExtension.parameter_visibility
    : defaultParameterVisibility.value,
);

/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
const parameterGroups = computed<Record<string, never>>(() => {
  if (Object.keys(props.schema?.["properties"] ?? {}).length > 0) {
    return {
      ...schemaDefinitions.value,
      ungrouped_parameters: {
        title: "Ungrouped Parameters",
        properties: props.schema?.["properties"],
        type: "object",
      },
    };
  }
  return schemaDefinitions.value;
});

// Watchers
// =============================================================================
watch(
  () => props.schema,
  (newValue) => {
    if (newValue) {
      updateSchema(newValue);
    }
  },
);

// Functions
// =============================================================================
/* eslint-disable @typescript-eslint/no-explicit-any */
function updateSchema(schema: Record<string, any>) {
  const schemaCompiler =
    /* eslint-disable @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    ajvVersions[
      schema["$schema"] ?? "https://json-schema.org/draft/2020-12/schema"
    ] ?? ajvVersions["https://json-schema.org/draft/2020-12/schema"];
  validateSchema = schemaCompiler.compile(schema);
  const groupedParameters = Object.keys(parameterGroups.value).map(
    (groupName) =>
      Object.fromEntries(
        Object.entries(parameterGroups.value[groupName]["properties"]).map(
          ([parameterName, parameter]) => [
            parameterName,
            // @ts-ignore
            parameter["default"],
          ],
        ),
      ),
  );
  formState.formInput = groupedParameters.reduce((acc, val) => {
    return { ...acc, ...val };
  });
  loadParameters({ params: props.parameterExtension?.defaults }, false);
  loadParameters(executionRepository.popTemporaryParameters());
}

function startWorkflow() {
  errorToast?.hide();
  formState.validated = true;
  formState.errorType = undefined;
  // delete parameters that are strings and have a length of 0
  for (const paramName of Object.keys(formState.formInput)) {
    const param = formState.formInput[paramName];
    if (
      param == undefined ||
      (typeof param === "string" && param?.length === 0)
    ) {
      delete formState.formInput[paramName];
    }
  }
  if (launchForm.value?.checkValidity()) {
    const schemaValid = validateSchema(formState.formInput);

    if (!schemaValid) {
      console.error(validateSchema.errors);
      errorToast?.show();
    } else {
      emit(
        "start-workflow",
        nestParameters(formState.formInput),
        formState.metaParameters,
      );
    }
  } else {
    formState.errorType = "form";
    errorToast?.show();
  }
}

function loadParameters(
  tempParams?: TemporaryParams,
  showSuccessToast: boolean = true,
) {
  if (tempParams?.params) {
    const flatTempParams = flattenParameters(tempParams.params);
    for (const param in flatTempParams) {
      if (param in formState.formInput) {
        formState.formInput[param] = flatTempParams[param];
      }
    }
  }
  if (tempParams?.metaParams) {
    formState.metaParameters = tempParams.metaParams;
  }
  if (
    showSuccessToast &&
    (Object.keys(tempParams?.params ?? {}).length > 0 ||
      Object.keys(tempParams?.metaParams ?? {}).length > 0)
  ) {
    parameterLoadToast?.show();
  }
}

// Lifecycle Events
// =============================================================================
onMounted(() => {
  if (props.schema) updateSchema(props.schema);
  bucketRepository.fetchOwnBuckets();
  bucketRepository.fetchOwnPermissions();
  keyRepository.fetchS3Keys();
  resourceRepository.fetchPublicResources();
  errorToast = new Toast("#workflowExecutionErrorToast");
  parameterLoadToast = new Toast("#workflowExecutionParameterLoadToast");
});
</script>

<template>
  <bootstrap-toast
    toast-id="workflowExecutionParameterLoadToast"
    color-class="success"
  >
    Successfully loaded parameters
  </bootstrap-toast>
  <bootstrap-toast toast-id="workflowExecutionErrorToast" color-class="danger">
    <template #default>Error starting workflow</template>
    <template #body>
      <template v-if="formState.errorType === 'form'">
        Some inputs are not valid.
      </template>
      <template v-else>
        There was an error with starting the workflow execution. Look in the
        console for more information.
      </template>
    </template>
  </bootstrap-toast>
  <div class="row align-items-start">
    <form
      v-if="props.schema"
      id="launchWorkflowForm"
      ref="launchForm"
      class="col-9"
      :class="{ 'was-validated': formState.validated }"
      novalidate
      @submit.prevent="startWorkflow"
    >
      <template v-for="(group, groupName) in parameterGroups" :key="groupName">
        <parameter-group-form
          v-if="formState.formInput"
          v-model="formState.formInput"
          :parameter-group-name="groupName"
          :schema-properties="parameterGroups"
          :resource-parameters="props.clowmInfo?.resourceParameters"
          :mapping="parameterExtension?.mapping"
          :parameter-visibility="currentParameterVisibility"
          :current-visibility="formState.viewMode"
        />
      </template>
      <div class="card mb-3">
        <h3 id="pipelineGeneralOptions" class="card-header">
          <font-awesome-icon icon="fa-solid fa-gear" class="me-2" />
          Pipeline Options
        </h3>
        <div class="card-body">
          <h5 class="card-title">
            General Options about the pipeline execution
          </h5>
          <div v-if="props.allowNotes">
            <code
              class="bg-secondary-subtle p-2 rounded-top border border-secondary"
              >--notes</code
            >
            <div class="input-group">
              <span
                id="pipelineNotes"
                class="input-group-text border border-secondary"
              >
                <font-awesome-icon icon="fa-solid fa-sticky-note" />
              </span>
              <textarea
                id="pipelineNotes"
                v-model="formState.metaParameters.notes"
                class="form-control border border-secondary"
                rows="2"
              />
            </div>
            <div class="mb-3 d-inline-block">
              Personal notes about the pipeline execution
            </div>
          </div>
          <div>
            <code
              class="bg-secondary-subtle p-2 rounded-top border border-secondary"
              >--logs_s3_path</code
            >
            <div class="input-group">
              <span class="input-group-text border border-secondary">
                <font-awesome-icon icon="fa-solid fa-folder" />
              </span>
              <parameter-input
                v-model="formState.metaParameters.logs_s3_path"
                :parameter="{
                  format: 'directory-path',
                  type: 'string',
                }"
                border="secondary"
              />
            </div>
            <div class="mb-3 d-inline-block">
              Directory in bucket where to save Nextflow log and reports
            </div>
          </div>
          <div v-if="props.nextflowVersion >= NextflowVersion['23_04_0']">
            <code
              class="bg-secondary-subtle p-2 rounded-top border border-secondary"
              >--provenance_s3_path</code
            >
            <div class="input-group">
              <span class="input-group-text border border-secondary">
                <font-awesome-icon icon="fa-solid fa-folder" />
              </span>
              <parameter-input
                v-model="formState.metaParameters.provenance_s3_path"
                :parameter="{
                  format: 'directory-path',
                  type: 'string',
                }"
                border="secondary"
              />
            </div>
            <div class="mb-3 d-inline-block">
              Directory in bucket where to save provenance information about the
              workflow execution
            </div>
          </div>
          <div :hidden="formState.viewMode < ParameterVisibility.EXPERT">
            <code
              class="bg-secondary-subtle p-2 rounded-top border border-secondary"
              >--debug_s3_path</code
            >
            <div class="input-group">
              <span class="input-group-text border border-secondary">
                <font-awesome-icon icon="fa-solid fa-folder" />
              </span>
              <parameter-input
                v-model="formState.metaParameters.debug_s3_path"
                :parameter="{
                  format: 'directory-path',
                  type: 'string',
                }"
                border="secondary"
              />
            </div>
            <div class="mb-3 d-inline-block">
              Directory in bucket where to save debug information about the
              workflow execution
            </div>
          </div>
        </div>
      </div>
    </form>
    <!-- Loading card -->
    <div v-else class="col-9">
      <div class="card mb-3">
        <h2 class="card-header placeholder-glow">
          <span class="placeholder col-6"></span>
        </h2>
        <div class="card-body">
          <h5 class="card-title placeholder-glow">
            <span class="placeholder col-5"> </span>
          </h5>
          <template v-for="n in 4" :key="n">
            <div class="placeholder-glow fs-5">
              <span class="placeholder w-100"> </span>
            </div>
            <div class="mb-3 placeholder-glow">
              <span class="placeholder col-3"> </span>
            </div>
          </template>
        </div>
      </div>
    </div>
    <group-nav
      v-if="props.schema != undefined"
      :clowm-info="props.clowmInfo"
      :parameter-groups="parameterGroups"
      :parameter-visibility="currentParameterVisibility"
      :workflow-id="workflowId"
      launchable
      form-id="launchWorkflowForm"
      @load-parameters="loadParameters"
      @change-view-mode="(viewMode) => (formState.viewMode = viewMode)"
    />
  </div>
</template>

<style>
.was-validated *:invalid {
  border-color: var(--bs-form-invalid-border-color) !important;
  background: var(--bs-danger-bg-subtle) !important;
}
</style>
