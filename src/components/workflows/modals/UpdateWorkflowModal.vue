<script setup lang="ts">
import { computed, onMounted, reactive, ref, watch } from "vue";
import { Collapse, Modal, Toast } from "bootstrap";
import type {
  WorkflowUpdate,
  WorkflowOut,
  WorkflowModeIn,
  WorkflowModeOut,
  WorkflowVersionOut,
} from "@/client/types.gen";
import { DocumentationEnum } from "@/client/types.gen";
import BootstrapModal from "@/components/modals/BootstrapModal.vue";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { WorkflowCredentialsService } from "@/client";
import { GitRepository, determineGitIcon } from "@/utils/GitRepository";
import { valid, lte, inc } from "semver";
import { latestVersion as calculateLatestVersion } from "@/utils/Workflow";
import WorkflowModeTransitionGroup from "@/components/transitions/WorkflowModeTransitionGroup.vue";
import { useWorkflowStore } from "@/stores/workflows";
import BootstrapToast from "@/components/BootstrapToast.vue";
import dayjs from "dayjs";
import { AxiosError } from "axios";

const workflowRepository = useWorkflowStore();
// Bootstrap Elements
// =============================================================================
let updateWorkflowModal: Modal | null = null;
let successToast: Toast | null = null;
let workflowModesCollapse: Collapse | null = null;

// Form Elements
// =============================================================================
const workflowUpdateForm = ref<HTMLFormElement | undefined>(undefined);
const workflowIconInputElement = ref<HTMLInputElement | undefined>(undefined);
const workflowVersionElement = ref<HTMLInputElement | undefined>(undefined);
const workflowGitCommitHashElement = ref<HTMLInputElement | undefined>(
  undefined,
);
const workflowIconElement = ref<HTMLImageElement | undefined>(undefined);

// Constants
// =============================================================================
const randomIDSuffix = Math.random().toString(16).substring(2, 8);

// Props
// =============================================================================
const props = defineProps<{
  modalId: string;
  workflow: WorkflowOut;
}>();

// Reactive State
// =============================================================================
const workflowUpdate = reactive<WorkflowUpdate>({
  version: "",
  git_commit_hash: "",
  delete_modes: [],
  append_modes: [],
});

const workflowModes = reactive<{
  deleteModes: string[];
  addModes: WorkflowModeOut[];
}>({
  deleteModes: [],
  addModes: [],
});

const formState = reactive<{
  validated: boolean;
  missingFiles: string[];
  loading: boolean;
  checkRepoLoading: boolean;
  allowUpload: boolean;
  loadCredentials: boolean;
  workflowToken?: string;
  modesEnabled: boolean;
  ratelimitReset: number;
}>({
  loading: false,
  checkRepoLoading: false,
  allowUpload: false,
  validated: false,
  missingFiles: [],
  loadCredentials: false,
  workflowToken: undefined,
  modesEnabled: false,
  ratelimitReset: 0,
});

watch(
  () => props.workflow,
  () => {
    resetForm();
    formState.modesEnabled = (latestVersion.value.modes ?? []).length > 0;
    if (props.workflow.private) {
      formState.loadCredentials = true;
      WorkflowCredentialsService.workflowCredentialsGetWorkflowCredentials({
        path: { wid: props.workflow.workflow_id },
      })
        .then((response) => response.data!)
        .then((credentials) => {
          formState.workflowToken = credentials.token ?? undefined;
        })
        .catch(() => {
          formState.workflowToken = undefined;
        })
        .finally(() => {
          formState.loadCredentials = false;
        });
    } else {
      formState.workflowToken = undefined;
    }
  },
);

watch(
  () => formState.modesEnabled,
  (show) => {
    if (show) {
      workflowModesCollapse?.show();
      if (
        (latestVersion.value.modes?.length ?? 0) +
          workflowModes.addModes.length ===
        0
      ) {
        addMode();
      }
    } else {
      workflowModesCollapse?.hide();
    }
  },
);

// Computed Properties
// =============================================================================
const latestVersion = computed<WorkflowVersionOut>(() => {
  return calculateLatestVersion(props.workflow.versions)!;
});
const gitIcon = computed<string>(() =>
  determineGitIcon(props.workflow.repository_url),
);

const showIcon = computed<boolean>(
  () => latestVersion.value.icon_url != undefined,
);

// Functions
// =============================================================================
function modalClosed() {
  formState.validated = false;
  formState.missingFiles = [];
  formState.allowUpload = false;
  workflowGitCommitHashElement.value?.setCustomValidity("");
}

function checkVersionValidity() {
  if (valid(workflowUpdate.version) == null) {
    workflowVersionElement.value?.setCustomValidity(
      "Please use semantic versioning",
    );
  } else if (lte(workflowUpdate.version, latestVersion.value.version)) {
    workflowVersionElement.value?.setCustomValidity(
      "The new version must be greater than previous version",
    );
  } else {
    workflowVersionElement.value?.setCustomValidity("");
  }
}

function checkRepository() {
  formState.validated = true;
  workflowGitCommitHashElement.value?.setCustomValidity("");
  if (workflowUpdateForm.value?.checkValidity() && !formState.allowUpload) {
    formState.missingFiles = [];
    const repo = GitRepository.buildRepository(
      props.workflow.repository_url,
      workflowUpdate.git_commit_hash,
      formState.workflowToken,
    );
    const oldModes =
      (latestVersion.value.modes ?? [])
        .filter((mode_id) => !workflowModes.deleteModes.includes(mode_id)) // filter all old modes that should be deleted
        .map((mode_id) => workflowRepository.modeMapping[mode_id]) // map mode id to mode object
        .filter((mode) => mode != undefined) ?? []; // filter all mode objects that are undefined
    const newModes = formState.modesEnabled ? workflowModes.addModes : [];
    repo
      .validateRepo([...oldModes, ...newModes])
      .then((checkRepoResult) => {
        const missingFiles = [];
        if (checkRepoResult.mainScriptMissing != undefined) {
          missingFiles.push(checkRepoResult.mainScriptMissing);
        }
        for (const doc of Object.values(DocumentationEnum)) {
          if (
            doc === DocumentationEnum.CLOWM_INFO_JSON ||
            doc === DocumentationEnum.CITATIONS_MD
          ) {
            continue;
          }
          missingFiles.push(...(checkRepoResult.docs[doc]?.missing ?? []));
        }
        if (missingFiles.length === 0) {
          formState.allowUpload = true;
        } else {
          workflowGitCommitHashElement.value?.setCustomValidity(
            "Files are missing in the repository",
          );
          formState.missingFiles = missingFiles;
        }
      })
      .catch((e) => {
        if (e instanceof AxiosError && e.response != undefined) {
          if (
            parseInt(e.response.headers["x-ratelimit-remaining"] ?? "1") == 0
          ) {
            formState.ratelimitReset = parseInt(
              e.response.headers["x-ratelimit-reset"] ?? "0",
            );
          } else if (e.response.status === 404) {
            workflowGitCommitHashElement.value?.setCustomValidity(
              "Can't find combination of repository and Git commit hash",
            );
          }
        }
      });
  }
}

function resetForm() {
  modalClosed();

  workflowIconElement.value!.src = latestVersion.value.icon_url ?? "";
  workflowUpdate.version = "";
  workflowUpdate.git_commit_hash = "";
  workflowUpdate.delete_modes = [];
  workflowUpdate.append_modes = [];
  workflowModes.addModes = [];
  workflowModes.deleteModes = [];
  if (workflowIconInputElement.value != undefined) {
    workflowIconInputElement.value.value = "";
  }
}

function updateWorkflow() {
  formState.validated = true;
  workflowUpdate.version = workflowUpdate.version.trim();
  if (workflowUpdateForm.value?.checkValidity() && formState.allowUpload) {
    formState.loading = true;
    workflowGitCommitHashElement.value?.setCustomValidity("");
    if (formState.modesEnabled) {
      workflowUpdate.append_modes = workflowModes.addModes.map((mode) => {
        return {
          schema_path: mode.schema_path,
          entrypoint: mode.entrypoint,
          name: mode.name,
        } as WorkflowModeIn;
      });
      workflowUpdate.delete_modes = workflowModes.deleteModes;
    }
    workflowRepository
      .updateWorkflow(props.workflow.workflow_id, workflowUpdate)
      .then(() => {
        successToast?.show();
        updateWorkflowModal?.hide();
        resetForm();
      })
      .catch((error) => {
        const errorText = error.response?.data?.["detail"];
        if (errorText.startsWith("Workflow with git_commit_hash")) {
          workflowGitCommitHashElement.value?.setCustomValidity(
            "Git commit is already used by a workflow",
          );
        }
        workflowUpdate.delete_modes = [];
        workflowUpdate.append_modes = [];
      })
      .finally(() => {
        formState.loading = false;
      });
  }
}

function addMode() {
  workflowModes.addModes.push({
    mode_id: crypto.randomUUID(),
    name: "",
    schema_path: "",
    entrypoint: "",
  });
  formState.allowUpload = false;
}

function addOldMode(mode_id: string) {
  if (
    (latestVersion.value.modes?.length ?? 0) +
      workflowModes.addModes.length -
      workflowModes.deleteModes.length <
    10
  ) {
    workflowModes.deleteModes = workflowModes.deleteModes.filter(
      (mode) => mode != mode_id,
    );
    formState.allowUpload = false;
  }
}

function removeNewMode(mode_id: string) {
  if (
    (latestVersion.value.modes?.length ?? 0) + workflowModes.addModes.length >
    1
  ) {
    workflowModes.addModes = workflowModes.addModes.filter(
      (mode) => mode.mode_id != mode_id,
    );
    formState.allowUpload = false;
  }
}

function removeOldMode(mode_id: string) {
  workflowModes.deleteModes.push(mode_id);
  formState.allowUpload = false;
}

// Lifecycle Events
// =============================================================================
onMounted(() => {
  updateWorkflowModal = new Modal("#" + props.modalId);
  successToast = new Toast("#successToast-" + randomIDSuffix);
  workflowModesCollapse = new Collapse("#updateWorkflowModesCollapse", {
    toggle: false,
  });
});
</script>

<template>
  <bootstrap-toast :toast-id="'successToast-' + randomIDSuffix">
    Successfully updated Workflow
  </bootstrap-toast>
  <bootstrap-modal
    :modal-id="modalId"
    :static-backdrop="true"
    modal-label="Update Workflow Modal"
    size-modifier-modal="lg"
    v-on="{ 'hidden.bs.modal': modalClosed }"
  >
    <template #header>
      Update Workflow
      <span class="fw-bold">{{ props.workflow.name }}</span>
    </template>
    <template #body>
      <form
        id="workflowUpdateForm"
        ref="workflowUpdateForm"
        :class="{ 'was-validated': formState.validated }"
      >
        <div class="mb-3">
          <span class="me-3">Git Repository URL:</span>
          <font-awesome-icon :icon="gitIcon" />
          <a
            class="ms-2"
            :href="props.workflow.repository_url"
            target="_blank"
            >{{ props.workflow.repository_url }}</a
          >
          <font-awesome-icon
            v-if="props.workflow.private"
            class="ms-2"
            icon="fa-solid fa-lock"
          />
          <img
            ref="workflowIconElement"
            :src="latestVersion.icon_url ?? undefined"
            class="float-end"
            :hidden="!showIcon"
            alt="Workflow Icon"
          />
        </div>
        <div class="row mb-3">
          <div class="col-8">
            <label for="updateWorkflowGitCommitInput" class="form-label"
              >Git Commit Hash</label
            >
            <div class="input-group">
              <div class="input-group-text">
                <font-awesome-icon icon="fa-solid fa-code-commit" />
              </div>
              <input
                id="updateWorkflowGitCommitInput"
                ref="workflowGitCommitHashElement"
                v-model="workflowUpdate.git_commit_hash"
                type="text"
                class="form-control text-lowercase"
                placeholder="ba8bcd9..."
                required
                maxlength="40"
                minlength="40"
                pattern="^[0-9a-f]+$"
                @change="formState.allowUpload = false"
              />
            </div>
            <div v-if="formState.ratelimitReset > 0" class="text-danger">
              Can't check GitHub repository because the default
              <a
                href="https://docs.github.com/en/rest/using-the-rest-api/rate-limits-for-the-rest-api?apiVersion=2022-11-28#primary-rate-limit-for-unauthenticated-users"
                target="_blank"
                >rate-limit</a
              >
              for your IP address was exhausted <br />
              Rate-limit resets
              {{ dayjs.unix(formState.ratelimitReset).fromNow() }}
            </div>
            <div
              v-else-if="formState.missingFiles.length > 0"
              class="text-danger"
            >
              The following files are missing in the repository
              <ul>
                <li v-for="file in formState.missingFiles" :key="file">
                  {{ file }}
                </li>
              </ul>
            </div>
          </div>
          <div class="col-4">
            <label for="updateWorkflowVersionInput" class="form-label"
              >Version</label
            >
            <div class="input-group">
              <div class="input-group-text">
                <font-awesome-icon icon="fa-solid fa-tag" />
              </div>
              <input
                id="updateWorkflowVersionInput"
                ref="workflowVersionElement"
                v-model="workflowUpdate.version"
                type="text"
                class="form-control"
                :placeholder="inc(latestVersion.version, 'patch') ?? undefined"
                maxlength="10"
                required
                aria-describedby="updateVersionHelp"
                @change="checkVersionValidity"
              />
            </div>
            <div id="updateVersionHelp" class="form-text">
              Previous Version: {{ latestVersion.version }}
            </div>
          </div>
        </div>
        <div class="mb-3">
          <div class="form-check fs-5">
            <input
              id="updateWorkflowModesCheckbox"
              v-model="formState.modesEnabled"
              class="form-check-input"
              type="checkbox"
              aria-controls="#updateWorkflowModesCollapse"
              :disabled="latestVersion.modes && latestVersion.modes.length > 0"
              @change="formState.allowUpload = false"
            />
            <label class="form-check-label" for="updateWorkflowModesCheckbox">
              Enable Workflow Modes
            </label>
            <button
              v-if="formState.modesEnabled"
              class="btn btn-primary float-end"
              :disabled="
                (latestVersion.modes?.length ?? 0) +
                  workflowModes.addModes.length -
                  workflowModes.deleteModes.length >=
                10
              "
              @click="addMode"
            >
              Add Mode
            </button>
          </div>
        </div>
        <div id="updateWorkflowModesCollapse" class="collapse">
          <WorkflowModeTransitionGroup>
            <div
              v-for="(mode_id, index) in latestVersion.modes"
              :key="mode_id"
              class="row mb-3"
            >
              <template v-if="workflowRepository.modeMapping[mode_id]">
                <h6>
                  <font-awesome-icon
                    v-if="workflowModes.deleteModes.includes(mode_id)"
                    icon="fa-solid fa-plus"
                    class="text-success me-1 fs-6 cursor-pointer"
                    @click="addOldMode(mode_id)"
                  />
                  <font-awesome-icon
                    v-else
                    icon="fa-solid fa-minus"
                    class="text-danger me-1 fs-6 cursor-pointer"
                    @click="removeOldMode(mode_id)"
                  />
                  <span
                    :class="{
                      'opacity-75': workflowModes.deleteModes.includes(mode_id),
                    }"
                    >Previous Mode {{ index + 1 }}</span
                  >
                </h6>
                <div class="col-6">
                  <label
                    :for="'modeNameInput-' + mode_id"
                    class="form-label"
                    :class="{
                      'opacity-75': workflowModes.deleteModes.includes(mode_id),
                    }"
                    >Name</label
                  >
                  <div class="input-group">
                    <div class="input-group-text">
                      <font-awesome-icon icon="fa-solid fa-tag" />
                    </div>
                    <input
                      :id="'modeNameInput-' + mode_id"
                      type="text"
                      class="form-control"
                      maxlength="128"
                      readonly
                      :disabled="workflowModes.deleteModes.includes(mode_id)"
                      :value="workflowRepository.modeMapping[mode_id].name"
                      :required="
                        formState.modesEnabled &&
                        !workflowModes.deleteModes.includes(mode_id)
                      "
                    />
                  </div>
                </div>
                <div class="col-6 mb-2">
                  <label
                    :for="'modeEntryInput-' + mode_id"
                    class="form-label"
                    :class="{
                      'opacity-75': workflowModes.deleteModes.includes(mode_id),
                    }"
                    >Entrypoint</label
                  >
                  <div class="input-group">
                    <div class="input-group-text">
                      <font-awesome-icon icon="fa-solid fa-turn-down" />
                    </div>
                    <input
                      :id="'modeEntryInput-' + mode_id"
                      type="text"
                      class="form-control"
                      readonly
                      :disabled="workflowModes.deleteModes.includes(mode_id)"
                      :value="
                        workflowRepository.modeMapping[mode_id].entrypoint
                      "
                      :required="
                        formState.modesEnabled &&
                        !workflowModes.deleteModes.includes(mode_id)
                      "
                    />
                  </div>
                </div>
                <label
                  :for="'modeSchemaInput-' + index"
                  class="form-label"
                  :class="{
                    'opacity-75': workflowModes.deleteModes.includes(mode_id),
                  }"
                  >Schema File</label
                >
                <div class="input-group">
                  <div class="input-group-text">
                    <font-awesome-icon icon="fa-solid fa-file-code" />
                  </div>
                  <input
                    :id="'modeSchemaInput-' + mode_id"
                    type="text"
                    class="form-control"
                    readonly
                    :disabled="workflowModes.deleteModes.includes(mode_id)"
                    :value="workflowRepository.modeMapping[mode_id].schema_path"
                    :required="
                      formState.modesEnabled &&
                      !workflowModes.deleteModes.includes(mode_id)
                    "
                  />
                </div>
              </template>
            </div>

            <div
              v-for="(mode, index) in workflowModes.addModes"
              :key="mode.mode_id"
              class="row mb-3"
            >
              <h6>
                <font-awesome-icon
                  icon="fa-solid fa-minus"
                  class="text-danger me-1 fs-6 cursor-pointer"
                  @click="removeNewMode(mode.mode_id)"
                />
                New Mode {{ (latestVersion.modes?.length ?? 0) + index + 1 }}
              </h6>
              <div class="col-6">
                <label :for="'modeNameInput-' + index" class="form-label"
                  >Name</label
                >
                <div class="input-group">
                  <div class="input-group-text">
                    <font-awesome-icon icon="fa-solid fa-tag" />
                  </div>
                  <input
                    :id="'modeNameInput-' + index"
                    v-model="mode.name"
                    type="text"
                    class="form-control"
                    maxlength="128"
                    :required="formState.modesEnabled"
                  />
                </div>
              </div>
              <div class="col-6 mb-2">
                <label :for="'modeEntryInput-' + index" class="form-label"
                  >Entrypoint</label
                >
                <div class="input-group">
                  <div class="input-group-text">
                    <font-awesome-icon icon="fa-solid fa-turn-down" />
                  </div>
                  <input
                    :id="'modeEntryInput-' + index"
                    v-model="mode.entrypoint"
                    type="text"
                    class="form-control"
                    maxlength="128"
                    :required="formState.modesEnabled"
                  />
                </div>
              </div>
              <label :for="'modeSchemaInput-' + index" class="form-label"
                >Schema File</label
              >
              <div class="input-group">
                <div class="input-group-text">
                  <font-awesome-icon icon="fa-solid fa-file-code" />
                </div>
                <input
                  :id="'modeSchemaInput-' + index"
                  v-model="mode.schema_path"
                  type="text"
                  class="form-control"
                  maxlength="128"
                  pattern=".*\.json$"
                  :required="formState.modesEnabled"
                  @change="formState.allowUpload = false"
                />
              </div>
            </div>
          </WorkflowModeTransitionGroup>
        </div>
      </form>
    </template>
    <template #footer>
      <button
        type="button"
        class="btn btn-info me-auto"
        :disabled="formState.allowUpload || formState.loadCredentials"
        @click="checkRepository"
      >
        <span
          v-if="formState.checkRepoLoading"
          class="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
        Check Repository
      </button>
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
        Close
      </button>
      <button
        type="submit"
        form="workflowUpdateForm"
        class="btn btn-primary"
        :disabled="formState.loading || !formState.allowUpload"
        @click.prevent="updateWorkflow"
      >
        <span
          v-if="formState.loading"
          class="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
        Save
      </button>
    </template>
  </bootstrap-modal>
</template>

<style scoped>
img {
  max-height: 32px;
  max-width: 32px;
}
</style>
