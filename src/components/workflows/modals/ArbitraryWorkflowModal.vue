<script setup lang="ts">
import BootstrapModal from "@/components/modals/BootstrapModal.vue";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { computed, onMounted, reactive, ref, watch } from "vue";
import { useRouter } from "vue-router";
import { GitRepository, determineGitIcon } from "@/utils/GitRepository";
import { Collapse, Modal } from "bootstrap";
import { type DocLocations, useWorkflowStore } from "@/stores/workflows";
import { DocumentationEnum, NextflowVersion } from "@/client/types.gen";
import type { WorkflowModeOut } from "@/client/types.gen";
import { environment } from "@/environment";
import { useS3KeyStore } from "@/stores/s3keys";
import { useResourceStore } from "@/stores/resources";
import { createDownloadUrl } from "@/utils/DownloadJson";
import dayjs from "dayjs";
import { AxiosError } from "axios";

const props = defineProps<{
  modalId: string;
}>();

let createWorkflowModal: Modal | null = null;
let privateRepositoryCollapse: Collapse | null = null;
let tokenHelpCollapse: Collapse | null = null;
let workflowModeCollapse: Collapse | null = null;
const arbitraryWorkflowForm = ref<HTMLFormElement | undefined>(undefined);
const workflowRepositoryElement = ref<HTMLInputElement | undefined>(undefined);

type Tabs = "clowm" | "cluster" | "local";

const router = useRouter();
const workflowStore = useWorkflowStore();
const s3KeyStore = useS3KeyStore();
const resourceStore = useResourceStore();

const workflow = reactive<{
  repository_url: string;
  git_commit_hash: string;
  nextflow_version: NextflowVersion;
}>({
  repository_url: "",
  git_commit_hash: "",
  nextflow_version:
    Object.values(NextflowVersion)[Object.values(NextflowVersion).length - 1],
});

const repositoryCredentials = reactive<{
  token: string;
  privateRepo: boolean;
}>({
  token: "",
  privateRepo: false,
});

const workflowMode = reactive<{
  mode: WorkflowModeOut;
  modeEnabled: boolean;
}>({
  mode: {
    entrypoint: "",
    schema_path: "",
    name: "",
    mode_id: crypto.randomUUID(),
  },
  modeEnabled: false,
});
const docsLocations = reactive<DocLocations>({
  "changelog.md": "",
  "clowm_info.json": "",
  "input.md": "",
  "output.md": "",
  "parameter_schema.json": "",
  "usage.md": "",
  "CITATIONS.md": "",
});

const activeTab = ref<Tabs>("clowm");

const formState = reactive<{
  loading: boolean;
  checkRepoLoading: boolean;
  validated: boolean;
  allowUpload: boolean;
  missingFiles: string[];
  unsupportedRepository: boolean;
  ratelimit_reset: number;
}>({
  validated: false,
  allowUpload: false,
  loading: false,
  checkRepoLoading: false,
  missingFiles: [],
  unsupportedRepository: false,
  ratelimit_reset: 0,
});

watch(
  () => repositoryCredentials.privateRepo,
  (show) => {
    if (show) {
      privateRepositoryCollapse?.show();
    } else {
      privateRepositoryCollapse?.hide();
      tokenHelpCollapse?.hide();
    }
  },
);

watch(
  () => workflowMode.modeEnabled,
  (show) => {
    if (show) {
      workflowModeCollapse?.show();
    } else {
      workflowModeCollapse?.hide();
    }
  },
);

function modalClosed() {
  formState.validated = false;
  tokenHelpCollapse?.hide();
}

function viewWorkflow() {
  createWorkflowModal?.hide();
  workflowStore
    .setArbitraryWorkflow(
      {
        ...workflow,
        name: "",
        short_description: "",
        modes: workflowMode.modeEnabled ? [{ ...workflowMode.mode }] : [],
        token:
          repositoryCredentials.token.length > 0
            ? repositoryCredentials.token
            : undefined,
        initial_version: "",
      },
      { ...docsLocations },
    )
    .then((wid) => {
      router.push({
        name: "arbitrary-workflow",
        query: {
          wid: wid,
        },
      });
    });
}

const resourcePathPrefix = computed<string>(() => {
  let resourcePath: string | null = null;
  for (const resource of resourceStore.resources) {
    for (const version of resource.versions) {
      if (version.cluster_path.length > 0) {
        resourcePath = version.cluster_path;
      }
    }
  }
  if (resourcePath == null) {
    return "";
  }
  const ind = resourcePath.search(/CLDB-[\da-f]{32}/);
  if (ind == -1) {
    return "";
  }
  return resourcePath.slice(0, ind - 1);
});

const nextflowConfig = computed<string>(() => {
  if (activeTab.value === "cluster") {
    return `// Nextflow config to emulate CloWM native execution
aws {
    client {
       endpoint = '${environment.S3_URL}'
       s3PathStyleAccess = true
    }
    // Your personal S3 keys
    accessKey = '${s3KeyStore.keys[0]?.access_key ?? ""}'
    secretKey = '${s3KeyStore.keys[0]?.secret_key ?? ""}'
}

// Docker container options
docker {
    enabled = true
    runOptions = "-u \\$(id -u):\\$(id -g) -q \\
                  -v ${resourcePathPrefix.value}:${resourcePathPrefix.value}:ro \\
                  -v /var/lib/lxcfs/proc/cpuinfo:/proc/cpuinfo:rw \\
                  -v /var/lib/lxcfs/proc/diskstats:/proc/diskstats:rw \\
                  -v /var/lib/lxcfs/proc/meminfo:/proc/meminfo:rw  \\
                  -v /var/lib/lxcfs/proc/stat:/proc/stat:rw \\
                  -v /var/lib/lxcfs/proc/swaps:/proc/swaps:rw \\
                  -v /var/lib/lxcfs/proc/uptime:/proc/uptime:rw"
}

// Disable unwanted features
conda.enabled = false
weblog.enabled = false
shifter.enabled = false
singularity.enabled = false
podman.enabled = false
charliecloud.enabled = false
dag.enabled = false
trace.enabled = false

// Nextflow provenance tracking plugin
plugins {
  id 'nf-prov'
}

prov {
  enabled = true
  formats {
    legacy {
      file = "./nf-prov_manifest.json"
      overwrite = true
    }
    bco {
      file = "./nf-prov_bco.json"
      overwrite = true
    }
    dag {
      file = "./nf-prov_dag.html"
      overwrite = true
    }
  }
}

executor {
    submitRateLimit = '5 sec'
}

// Process resource labels currently used by CloWM
process {
    executor = 'slurm'
    container = 'ubuntu:22.04'
    cpus   = { 2    }
    memory = { 4000.MB }
    maxErrors     = '-1'

    // Process-specific resource requirements
    withLabel:process_single {
        cpus   = { 1 }
        memory = { 6000.MB }
    }
    withLabel:process_low {
        cpus   = { 2     }
        memory = { 12000.MB }
    }
    withLabel:process_medium {
        cpus   = { 6     }
        memory = { 36000.MB }
    }
    withLabel:process_high {
        cpus   = { 12    }
        memory = { 72000.MB }
    }
    withLabel:process_high_memory {
        memory = { 200000.MB }
    }
    withLabel:error_ignore {
        errorStrategy = 'ignore'
    }
    withLabel: highmemXLarge {
        cpus = { 28 }
        memory = {470000.MB}
    }
    withLabel: computeXLarge {
        cpus = {28}
        memory = {470000.MB}
    }
    withLabel: highmemLarge {
        cpus = {28}
        memory = {240000.MB}
    }
    withLabel: highmemMedium {
        cpus = {14}
        memory = {120000.MB}
    }
    withLabel: large {
        cpus = {28}
        memory = {64000.MB}
    }
    withLabel: medium {
        cpus = {14}
        memory = {32000.MB}
    }
    withLabel: small {
        cpus = {8}
        memory = {16000.MB}
    }
    withLabel: mini {
        cpus = {4}
        memory = {8000.MB}
    }
    withLabel: tiny {
        cpus = {1}
        memory = {2000.MB}
    }
}`;
  } else if (activeTab.value === "local") {
    return `// Basic Nextflow config to enable docker and S3 access
aws {
    client {
        endpoint = '${environment.S3_URL}'
        s3PathStyleAccess = true
    }
    // Your personal S3 keys
    accessKey = '${s3KeyStore.keys[0]?.access_key ?? ""}'
    secretKey = '${s3KeyStore.keys[0]?.secret_key ?? ""}'
}

docker {
    enabled = true
    runOptions = "-u \\$(id -u):\\$(id -g) -q"
}

// Disable unwanted features
conda.enabled = false
tower.enabled = false
weblog.enabled = false
shifter.enabled = false
singularity.enabled = false
podman.enabled = false
charliecloud.enabled = false
dag.enabled = true
trace.enabled = true

process {
    executor = 'local'
    container = 'ubuntu:22.04'
    cpus   = { 2 }
    memory = { 4000.MB }
}
`;
  }
  return "";
});

const configDownloadUrl = computed<string>(() =>
  createDownloadUrl(nextflowConfig.value, "text/plain"),
);

function checkRepository() {
  formState.validated = true;
  // remove trailing slash (/)
  workflow.repository_url = workflow.repository_url
    .trim()
    .replace(/(^\/+|\/+$)/g, "");
  formState.ratelimit_reset = 0;
  workflowRepositoryElement.value?.setCustomValidity("");
  if (arbitraryWorkflowForm.value?.checkValidity() && !formState.allowUpload) {
    formState.unsupportedRepository = false;
    formState.missingFiles = [];
    try {
      const repo = GitRepository.buildRepository(
        workflow.repository_url,
        workflow.git_commit_hash,
        repositoryCredentials.privateRepo
          ? repositoryCredentials.token
          : undefined,
      );
      repo
        .validateRepo(workflowMode.modeEnabled ? [workflowMode.mode] : [])
        .then((checkRepoResult) => {
          const missingFiles = [];
          if (checkRepoResult.mainScriptMissing != undefined) {
            missingFiles.push(checkRepoResult.mainScriptMissing);
          }
          for (const doc of Object.values(DocumentationEnum)) {
            docsLocations[doc] = checkRepoResult.docs[doc].found ?? "";
            if (
              doc === DocumentationEnum.CLOWM_INFO_JSON ||
              doc === DocumentationEnum.CITATIONS_MD
            ) {
              continue;
            }
            missingFiles.push(...(checkRepoResult.docs[doc]?.missing ?? []));
          }
          if (missingFiles.length === 0) {
            formState.allowUpload = true;
          } else {
            formState.missingFiles = missingFiles;
          }
        })
        .catch((e) => {
          if (e instanceof AxiosError && e.response != undefined) {
            if (
              parseInt(e.response.headers["x-ratelimit-remaining"] ?? "1") == 0
            ) {
              formState.ratelimit_reset = parseInt(
                e.response.headers["x-ratelimit-reset"] ?? "0",
              );
            } else if (e.response.status === 404) {
              workflowRepositoryElement.value?.setCustomValidity(
                "Can't find combination of repository and Git commit hash",
              );
            }
          }
        });
    } catch {
      formState.unsupportedRepository = true;
      workflowRepositoryElement.value?.setCustomValidity(
        "Repository is not supported",
      );
    }
  }
}

const gitIcon = computed<string>(() =>
  determineGitIcon(workflow.repository_url),
);

onMounted(() => {
  createWorkflowModal = new Modal("#" + props.modalId);
  privateRepositoryCollapse = new Collapse("#privateRepositoryCollapse", {
    toggle: false,
  });
  workflowModeCollapse = new Collapse("#workflowModeCollapse", {
    toggle: false,
  });
  tokenHelpCollapse = new Collapse("#tokenHelpCollapse", {
    toggle: false,
  });
});
</script>

<template>
  <bootstrap-modal
    :modal-id="modalId"
    :static-backdrop="false"
    modal-label="Test My Workflow Modal"
    size-modifier-modal="lg"
    v-on="{ 'hidden.bs.modal': modalClosed }"
  >
    <template #header>Test workflow prior registration</template>
    <template #body>
      <h5>Choose the environment for workflow execution</h5>
      <div class="btn-group justify-content-center w-100 mb-2" role="group">
        <input
          id="clowm-outlined"
          v-model="activeTab"
          type="radio"
          class="btn-check"
          name="execution-environment-choice"
          autocomplete="off"
          checked
          value="clowm"
        />
        <label class="btn btn-outline-primary" for="clowm-outlined">
          <font-awesome-icon icon="fa-solid fa-cloud" class="me-1" />
          CloWM native execution</label
        >
        <input
          id="cluster-outlined"
          v-model="activeTab"
          type="radio"
          class="btn-check"
          name="execution-environment-choice"
          autocomplete="off"
          value="cluster"
        />
        <label class="btn btn-outline-primary" for="cluster-outlined">
          <font-awesome-icon icon="fa-solid fa-server" class="me-1" />
          Emulated CloWM environment
        </label>
        <input
          id="cli-outlined"
          v-model="activeTab"
          type="radio"
          class="btn-check"
          name="execution-environment-choice"
          autocomplete="off"
          value="local"
        />
        <label class="btn btn-outline-primary" for="cli-outlined">
          <font-awesome-icon icon="fa-solid fa-laptop" class="me-1" />
          Local Execution</label
        >
      </div>
      <div v-if="activeTab !== 'clowm'">
        <p v-if="activeTab === 'cluster'">
          To emulate the CloWM native execution environment (e.g. docker
          configuration, S3 access, resource labels etc.) on the workflow
          development system, please download and use the Nextflow configuration
          file (<code>clowm-nextflow.config</code>) as described below.
        </p>
        <p v-else>
          To emulate a basic CloWM native execution environment (e.g. docker
          configuration, S3 access etc.) on your personal computer, please
          download and use the Nextflow configuration file
          (<code>clowm-nextflow.config</code>) as described below.
        </p>
        <h6>Start Workflow:</h6>
        <pre
          class="rounded-1 w-100"
        ><code>nextflow run /path/to/main.nf -c clowm-nextflow.config ...</code></pre>
        <h6>clowm-nextflow.config:</h6>
        <pre class="w-100 rounded-1"><code>{{ nextflowConfig }}</code></pre>
      </div>
      <form
        id="arbitraryWorkflowForm"
        ref="arbitraryWorkflowForm"
        :hidden="activeTab !== 'clowm'"
        :class="{ 'was-validated': formState.validated }"
      >
        <p>
          Fill out the form to run an execution of your workflow on the CloWM
          infrastructure directly from your git repository.
        </p>
        <div class="mb-3">
          <label for="arbitraryWorkflowRepositoryInput" class="form-label"
            >Git Repository URL</label
          >
          <div class="input-group">
            <div class="input-group-text">
              <font-awesome-icon :icon="gitIcon" />
            </div>
            <input
              id="arbitraryWorkflowRepositoryInput"
              ref="workflowRepositoryElement"
              v-model="workflow.repository_url"
              type="url"
              class="form-control"
              placeholder="https://..."
              required
              aria-describedby="gitRepoProviderHelp"
              @change="formState.allowUpload = false"
            />
          </div>
          <div id="gitRepoProviderHelp" class="form-text">
            We support Github and (self-hosted) GitLab Repositories
          </div>
          <div class="text-danger">
            <div v-if="formState.unsupportedRepository">
              Repository is not supported
            </div>
          </div>
        </div>
        <div class="mb-3">
          <label for="workflowGitCommitInput" class="form-label"
            >Git Commit Hash</label
          >
          <div class="input-group">
            <div class="input-group-text">
              <font-awesome-icon icon="fa-solid fa-code-commit" />
            </div>
            <input
              id="workflowGitCommitInput"
              ref="workflowGitCommitHashElement"
              v-model="workflow.git_commit_hash"
              type="text"
              class="form-control text-lowercase"
              placeholder="ba8bcd9..."
              required
              maxlength="40"
              minlength="40"
              pattern="^[0-9a-f]+$"
              @change="formState.allowUpload = false"
            />
          </div>
        </div>
        <div v-if="formState.ratelimit_reset > 0" class="text-danger">
          Can't check GitHub repository because the default
          <a
            href="https://docs.github.com/en/rest/using-the-rest-api/rate-limits-for-the-rest-api?apiVersion=2022-11-28#primary-rate-limit-for-unauthenticated-users"
            target="_blank"
            >rate-limit</a
          >
          for your IP address was exhausted. You could mark this repository as
          private repository to bypass this constraint. <br />
          Rate-limit resets
          {{ dayjs.unix(formState.ratelimit_reset).fromNow() }}
        </div>
        <div v-else-if="formState.missingFiles.length > 0" class="text-danger">
          The following files are missing in the repository
          <ul>
            <li v-for="file in formState.missingFiles" :key="file">
              {{ file }}
            </li>
          </ul>
        </div>
        <div class="mb-3">
          <label for="workflowNextflowVersionInput" class="form-label"
            >Nextflow version</label
          >
          <select
            id="workflowNextflowVersionInput"
            v-model="workflow.nextflow_version"
            class="form-select"
            required
          >
            <option
              v-for="version in Object.values(NextflowVersion).reverse()"
              :key="version"
              :value="version"
            >
              {{ version }}
            </option>
          </select>
        </div>
        <div class="mb-3">
          <div class="form-check fs-5">
            <input
              id="privateRepositoryCheckbox"
              v-model="repositoryCredentials.privateRepo"
              class="form-check-input"
              type="checkbox"
              aria-controls="#privateRepositoryCollapse"
              @change="formState.allowUpload = false"
            />
            <label class="form-check-label" for="privateRepositoryCheckbox">
              Enable Private Git Repository
            </label>
          </div>
          <div id="privateRepositoryCollapse" class="collapse">
            <label for="arbitraryRepositoryTokenInput" class="form-label"
              >Token</label
            >
            <div class="input-group">
              <div class="input-group-text">
                <font-awesome-icon icon="fa-solid fa-key" />
              </div>
              <input
                id="arbitraryRepositoryTokenInput"
                v-model="repositoryCredentials.token"
                type="password"
                class="form-control"
                :required="repositoryCredentials.privateRepo"
                aria-controls="#tokenHelpCollapse"
                @change="formState.allowUpload = false"
              />
              <div
                class="input-group-text cursor-pointer hover-info"
                @click="tokenHelpCollapse?.toggle()"
              >
                <font-awesome-icon icon="fa-solid fa-circle-question" />
              </div>
            </div>
            <div id="tokenHelpCollapse" class="collapse">
              <div class="card card-body mt-3">
                <h5>GitHub</h5>
                <p>
                  For private GitHub repositories, CloWM needs a Personal Access
                  Token (classic) with the scope <code>repo</code>.<br />
                  Read this
                  <a
                    target="_blank"
                    href="https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#creating-a-personal-access-token-classic"
                    >Tutorial</a
                  >
                  on how to create such a token.
                </p>
                <h5>GitLab</h5>
                <p>
                  For private GitLab repositories, CloWM needs a Project Access
                  Token with the <code>read_api</code> scope and at least
                  <code>Reporter</code> role.<br />
                  Read this
                  <a
                    target="_blank"
                    href="https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token"
                    >Tutorial</a
                  >
                  on how to create such a token.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-3">
          <div class="form-check fs-5">
            <input
              id="workflowModeCheckbox"
              v-model="workflowMode.modeEnabled"
              class="form-check-input"
              type="checkbox"
              aria-controls="#workflowModeCollapse"
              @change="formState.allowUpload = false"
            />
            <label class="form-check-label" for="workflowModeCheckbox">
              Enable Workflow Mode
            </label>
          </div>
          <div id="workflowModeCollapse" class="collapse">
            <div class="row">
              <div class="col-6 mb-2">
                <label for="arbitraryModeEntryInput" class="form-label"
                  >Entrypoint</label
                >
                <div class="input-group">
                  <div class="input-group-text">
                    <font-awesome-icon icon="fa-solid fa-tag" />
                  </div>
                  <input
                    id="arbitraryModeEntryInput"
                    v-model="workflowMode.mode.entrypoint"
                    type="text"
                    class="form-control"
                    maxlength="128"
                    :required="workflowMode.modeEnabled"
                    @change="formState.allowUpload = false"
                  />
                </div>
              </div>
              <div class="col-6">
                <label for="modeSchemaInput-" class="form-label"
                  >Schema File</label
                >
                <div class="input-group">
                  <div class="input-group-text">
                    <font-awesome-icon icon="fa-solid fa-file-code" />
                  </div>
                  <input
                    id="modeSchemaInput-"
                    v-model="workflowMode.mode.schema_path"
                    type="text"
                    class="form-control"
                    maxlength="128"
                    pattern=".*\.json$"
                    :required="workflowMode.modeEnabled"
                    @change="formState.allowUpload = false"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </template>
    <template #footer>
      <button
        v-if="activeTab === 'clowm'"
        type="button"
        class="btn btn-info me-auto"
        :disabled="formState.allowUpload"
        @click="checkRepository"
      >
        <span
          v-if="formState.checkRepoLoading"
          class="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
        Check Repository
      </button>
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
        Close
      </button>
      <button
        v-if="activeTab === 'clowm'"
        type="submit"
        form="workflowCreateForm"
        class="btn btn-primary"
        :disabled="formState.loading || !formState.allowUpload"
        @click.prevent="viewWorkflow"
      >
        <span
          v-if="formState.loading"
          class="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
        Run
      </button>
      <a
        v-else
        class="btn btn-primary"
        role="button"
        :href="configDownloadUrl"
        download="clowm-nextflow.config"
      >
        Download config
        <font-awesome-icon icon="fa-solid fa-download" class="ms-1" />
      </a>
    </template>
  </bootstrap-modal>
</template>

<style scoped></style>
