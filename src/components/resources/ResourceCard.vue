<script setup lang="ts">
import type {
  FileTree,
  ResourceOut,
  ResourceVersionOut,
} from "@/client/types.gen";
import { ResourceVersionStatus } from "@/client/types.gen";
import { computed, onMounted, ref } from "vue";
import dayjs from "dayjs";
import CopyToClipboardIcon from "@/components/CopyToClipboardIcon.vue";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { useS3ObjectStore } from "@/stores/s3objects";
import { useResourceStore } from "@/stores/resources";
import { Tooltip } from "bootstrap";
import { useNameStore } from "@/stores/names";
import RequestReviewButton from "@/components/resources/RequestReviewButton.vue";
import { useUserStore } from "@/stores/users";
import { createDownloadUrl } from "@/utils/DownloadJson";
import { filesize } from "filesize";
import { useOTRStore } from "@/stores/otrs";

const randomIDSuffix: string = Math.random().toString(16).substring(2, 8);
const objectRepository = useS3ObjectStore();
const resourceRepository = useResourceStore();
const nameRepository = useNameStore();
const userRepository = useUserStore();
const otrRepository = useOTRStore();

const props = defineProps<{
  resource: ResourceOut;
  loading: boolean;
  extended?: boolean;
}>();
let refreshTimeout: NodeJS.Timeout | undefined = undefined;

const stateToUIMapping: Record<ResourceVersionStatus, string> = {
  APPROVED: "Resource approved",
  WAIT_FOR_REVIEW: "Wait for review",
  CLUSTER_DELETE_ERROR: "Error deleting resource on cluster",
  CLUSTER_DELETING: "Resource deletion on cluster in progress",
  S3_DELETE_ERROR: "Error deleting tarball in S3",
  S3_DELETING: "Tarball deletion in S3 in progress",
  SETTING_LATEST: "Resource available",
  SYNC_ERROR: "Error syncing to cluster",
  DENIED: "Resource creation rejected by reviewer",
  RESOURCE_REQUESTED: "Resource creation requested",
  S3_DELETED: "Tarball deleted in S3",
  SYNCHRONIZED: "Resource available",
  SYNCHRONIZING: "Synchronizing to cluster in progress",
  SYNC_REQUESTED: "Synchronization to cluster requested",
  LATEST: "Resource available (latest)",
};

const emit = defineEmits<{
  (e: "click-info", resourceVersion: ResourceVersionOut): void;
  (e: "click-update", resource: ResourceOut): void;
  (e: "click-request-sync", resourceVersion: ResourceVersionOut): void;
  (e: "click-show-otr", resourceId: string): void;
  (e: "click-create-otr", resource: ResourceOut): void;
  (e: "click-delete", resource: ResourceOut): void;
}>();

const resourceVersionS3Ready = ref<Record<string, boolean>>({});

const resourceVersions = computed<ResourceVersionOut[]>(() =>
  [...props.resource.versions].sort((a, b) =>
    a.created_at < b.created_at ? 1 : -1,
  ),
);

function checkS3Resource(resourceVersion: ResourceVersionOut) {
  const bucket = resourceVersion.s3_path.slice(5).split("/")[0];
  const key = resourceVersion.s3_path.split(bucket)[1].slice(1);
  objectRepository
    .fetchS3ObjectMeta(bucket, key)
    .then(() => {
      resourceVersionS3Ready.value[resourceVersion.resource_version_id] = true;
    })
    .catch(() => {
      resourceVersionS3Ready.value[resourceVersion.resource_version_id] = false;
    });
}

function prepareFileTreeForDownload(
  parent: string,
  tree?: FileTree | null,
): string[] {
  if (tree == undefined || tree.contents == undefined) {
    return [];
  }
  let files: string[] = [];
  const currentName = parent.length > 0 ? parent + tree.name + "/" : "/";
  for (const dir of tree.contents) {
    if (dir.type === "directory") {
      files = files.concat(prepareFileTreeForDownload(currentName, dir));
    } else {
      files.push(currentName + dir.name + "\t" + filesize(dir.size));
    }
  }
  return files;
}

const fileTreeFiles = computed<(string | undefined)[]>(() => {
  return props.resource.versions.map((version) => {
    if (resourceRepository.resourceTree[version.resource_version_id]) {
      return createDownloadUrl(
        prepareFileTreeForDownload(
          "",
          resourceRepository.resourceTree[version.resource_version_id],
        ).join("\n"),
        "text/plain",
      );
    }
    return undefined;
  });
});

function clickCheckS3Resource(resourceVersion: ResourceVersionOut) {
  clearTimeout(refreshTimeout);
  refreshTimeout = setTimeout(() => {
    checkS3Resource(resourceVersion);
  }, 500);
}

function requestReview(resourceVersion: ResourceVersionOut) {
  resourceRepository.requestReview(resourceVersion);
}

onMounted(() => {
  if (!props.loading) {
    new Tooltip("#resource-name-" + props.resource.resource_id);
    for (const r of props.resource.versions) {
      if (
        userRepository.resourceMaintainer ||
        userRepository.workflowDev ||
        userRepository.admin
      ) {
        resourceRepository.fetchResourceTree(
          props.resource.resource_id,
          r.resource_version_id,
        );
      }
      if (r.status == ResourceVersionStatus.RESOURCE_REQUESTED) {
        checkS3Resource(r);
      }
    }
    [
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      ...(document
        .querySelector("#resource-card-" + randomIDSuffix)
        ?.querySelectorAll("[data-bs-toggle='tooltip']") ?? []),
    ].forEach((el) => new Tooltip(el));
  }
});
</script>

<template>
  <div
    :id="'resource-card-' + randomIDSuffix"
    class="card-hover border border-secondary card m-2"
  >
    <div class="card-body">
      <div
        class="card-title fs-3 d-flex justify-content-between align-items-center"
      >
        <div v-if="props.loading" class="placeholder-glow w-100">
          <span class="placeholder col-6"></span>
        </div>
        <div v-else class="d-inline-flex align-items-center text-truncate">
          <span
            :id="'resource-name-' + props.resource.resource_id"
            data-bs-toggle="tooltip"
            :data-bs-title="props.resource.name"
            >{{ props.resource.name }}</span
          >
          <font-awesome-icon
            v-if="props.resource.private"
            :id="'resource-private-icon-' + props.resource.resource_id"
            icon="fa-solid fa-lock"
            class="fs-5 ms-2 tooltip-private-repository"
            tooltip="Private resource"
          />
          <font-awesome-icon
            v-if="extended && otrRepository.otrMapping[resource.resource_id]"
            icon="fa-solid fa-people-arrows"
            class="fs-5 ms-2 cursor-pointer hover-info"
            tooltip="Ownership transfer requested"
            @click="emit('click-show-otr', resource.resource_id)"
          />
        </div>
        <div v-if="props.extended" class="btn-group">
          <button
            type="button"
            class="btn btn-outline-success"
            :disabled="props.loading"
            data-bs-toggle="modal"
            data-bs-target="#updateResourceModal"
            @click="emit('click-update', props.resource)"
          >
            Update
          </button>
          <button
            type="button"
            class="btn btn-outline-success dropdown-toggle dropdown-toggle-split"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            <span class="visually-hidden">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu z-3">
            <li
              v-if="otrRepository.otrMapping[resource.resource_id] == undefined"
            >
              <button
                type="button"
                class="dropdown-item"
                @click="emit('click-create-otr', resource)"
              >
                Transfer ownership
              </button>
            </li>
            <li>
              <button
                type="button"
                class="dropdown-item text-danger"
                @click="emit('click-delete', resource)"
              >
                <font-awesome-icon icon="fa-solid fa-trash" class="=me-2" />
                Delete
              </button>
            </li>
          </ul>
        </div>
      </div>
      <p class="card-text">
        <span v-if="props.loading" class="placeholder-glow"
          ><span class="placeholder col-12"></span
        ></span>
        <span v-else>{{ props.resource.description }}</span>
        <br />
        Source:
        <span v-if="props.loading" class="placeholder-glow"
          ><span class="placeholder col-2"></span
        ></span>
        <span v-else>{{ props.resource.source }}</span>
      </p>
      <div v-if="!props.loading">
        <div :id="'accordion-' + props.resource.resource_id" class="accordion">
          <div
            v-for="(resourceVersion, index) in resourceVersions"
            :key="resourceVersion.resource_version_id"
            class="accordion-item"
          >
            <h2 class="accordion-header">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                :data-bs-target="
                  '#collapseResourceVersion-' +
                  resourceVersion.resource_version_id
                "
                aria-expanded="false"
                :aria-controls="
                  '#collapseResourceVersion-' +
                  resourceVersion.resource_version_id
                "
              >
                {{ resourceVersion.release }} -
                {{ stateToUIMapping[resourceVersion.status] }}
              </button>
            </h2>
            <div
              :id="
                'collapseResourceVersion-' + resourceVersion.resource_version_id
              "
              class="accordion-collapse collapse"
              :data-bs-parent="'#accordion-' + props.resource.resource_id"
            >
              <div class="accordion-body d-flex flex-column">
                <div>
                  Registered at:
                  {{
                    dayjs.unix(resourceVersion.created_at).format("DD MMM YYYY")
                  }}
                </div>
                <div
                  v-if="
                    resourceVersion.status == ResourceVersionStatus.APPROVED
                  "
                  class="d-grid gap-2"
                >
                  <button
                    type="button"
                    class="btn btn-primary"
                    data-bs-toggle="modal"
                    data-bs-target="#request-synchronization-modal"
                    @click="emit('click-request-sync', resourceVersion)"
                  >
                    Request synchronization
                  </button>
                </div>
                <div
                  v-if="
                    props.extended &&
                    resourceVersion.status ==
                      ResourceVersionStatus.RESOURCE_REQUESTED
                  "
                  class="d-flex justify-content-between align-items-center"
                >
                  <request-review-button
                    :disabled="
                      !resourceVersionS3Ready[
                        resourceVersion.resource_version_id
                      ]
                    "
                    @click-refresh="clickCheckS3Resource(resourceVersion)"
                    @click-review="requestReview(resourceVersion)"
                  />
                  <button
                    type="button"
                    class="btn btn-info btn-sm"
                    data-bs-toggle="modal"
                    data-bs-target="#uploadResourceInfoModal"
                    @click="emit('click-info', resourceVersion)"
                  >
                    What to do next?
                  </button>
                </div>
                <div
                  v-if="
                    resourceVersion.status ===
                      ResourceVersionStatus.SYNCHRONIZED ||
                    resourceVersion.status === ResourceVersionStatus.LATEST
                  "
                >
                  <label
                    :for="
                      'nextflow-access-path-' +
                      resourceVersion.resource_version_id
                    "
                    class="form-label"
                    >Nextflow Access Path:</label
                  >
                  <div class="input-group fs-4">
                    <div
                      :id="
                        'tooltip-cluster-path-' +
                        resourceVersion.resource_version_id
                      "
                      class="input-group-text hover-info"
                      data-bs-toggle="tooltip"
                      data-bs-title="Physical path to access the resource from your workflow"
                    >
                      <font-awesome-icon icon="fa-solid fa-circle-question" />
                    </div>
                    <input
                      :id="
                        'nextflow-access-path-' +
                        resourceVersion.resource_version_id
                      "
                      class="form-control"
                      type="text"
                      :value="resourceVersion.cluster_path"
                      aria-label="Nextflow Access Path"
                      readonly
                    />
                    <span class="input-group-text"
                      ><copy-to-clipboard-icon
                        :text="resourceVersion.cluster_path ?? ''"
                    /></span>
                  </div>
                </div>
                <div
                  v-if="
                    props.extended &&
                    resourceVersion.status ===
                      ResourceVersionStatus.RESOURCE_REQUESTED
                  "
                >
                  <label
                    :for="
                      's3-access-path-' + resourceVersion.resource_version_id
                    "
                    class="form-label"
                    >S3 Upload Path:</label
                  >
                  <div class="input-group fs-4 mb-3">
                    <div
                      :id="
                        'tooltip-s3-path-' + resourceVersion.resource_version_id
                      "
                      class="input-group-text hover-info"
                      data-bs-toggle="tooltip"
                      data-bs-title="S3 Path under which the resource should be uploaded"
                    >
                      <font-awesome-icon icon="fa-solid fa-circle-question" />
                    </div>
                    <input
                      :id="
                        's3-access-path-' + resourceVersion.resource_version_id
                      "
                      class="form-control"
                      type="text"
                      :value="resourceVersion.s3_path"
                      aria-label="S3 Access Path"
                      readonly
                    />
                    <span class="input-group-text"
                      ><copy-to-clipboard-icon :text="resourceVersion.s3_path"
                    /></span>
                  </div>
                </div>
                <a
                  v-if="fileTreeFiles[index] != undefined"
                  role="button"
                  :href="fileTreeFiles[index]"
                  :download="`file-list-${resourceVersion.resource_version_id}.txt`"
                  class="btn btn-primary"
                >
                  Download resource file list
                  <font-awesome-icon
                    icon="fa-solid fa-file-arrow-down"
                    class="ms-2"
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mt-2">
        Maintainer:
        <span
          v-if="
            props.loading || !nameRepository.getName(resource.maintainer_id)
          "
          class="placeholder-glow"
          ><span class="placeholder col-2"></span
        ></span>
        <span v-else>{{ nameRepository.getName(resource.maintainer_id) }}</span>
      </div>
    </div>
  </div>
</template>

<style scoped>
.card-hover {
  transition: transform 0.3s ease-out;
}

.card-hover:hover {
  transform: translate(0, -5px);
  box-shadow: var(--bs-box-shadow) !important;
}

.accordion-body > div:not(:last-child) {
  margin-bottom: 0.5rem !important;
}
</style>
