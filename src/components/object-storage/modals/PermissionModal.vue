<script setup lang="ts">
import { onMounted, reactive, watch, ref, computed } from "vue";
import BootstrapModal from "@/components/modals/BootstrapModal.vue";
import SearchUserModal from "@/components/modals/SearchUserModal.vue";
import { Modal } from "bootstrap";
import dayjs from "dayjs";
import type {
  BucketPermissionOut,
  BucketPermissionIn,
  BucketPermissionParameters,
  UserOut,
} from "@/client/types.gen";
import type { FolderTree } from "@/types/PseudoFolder";
import { Toast } from "bootstrap";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { useBucketStore } from "@/stores/buckets";
import BootstrapToast from "@/components/BootstrapToast.vue";
import { useNameStore } from "@/stores/names";
import DeletePermissionModal from "@/components/object-storage/modals/DeletePermissionModal.vue";

// Props
// -----------------------------------------------------------------------------
const props = defineProps<{
  modalId: string;
  bucketName: string;
  subFolders: FolderTree;
  editUserPermission?: BucketPermissionOut;
  readonly: boolean;
  editable: boolean;
  deletable: boolean;
  backModalId?: string;
}>();

const bucketRepository = useBucketStore();
const nameRepository = useNameStore();

const emit = defineEmits<{ (e: "permission-deleted"): void }>();
// Variables
// -----------------------------------------------------------------------------
const randomIDSuffix = Math.random().toString(16).substring(2, 8);
let permissionModal: Modal | null = null;
let successToast: Toast | null = null;

// Reactive State
// -----------------------------------------------------------------------------

const formState = reactive<{
  loading: boolean;
  error: boolean;
  duplicate: boolean;
  readonly: boolean;
}>({
  loading: false,
  error: false,
  duplicate: false,
  readonly: props.readonly,
});

const permission = reactive<BucketPermissionIn>({
  from_timestamp: undefined,
  to_timestamp: undefined,
  file_prefix: undefined,
  scopes: [],
  uid: "",
  bucket_name: props.bucketName,
});

const permissionDeleted = ref<boolean>(false);
const permissionForm = ref<HTMLFormElement | undefined>(undefined);
const permissionReadScope = ref<HTMLInputElement | undefined>(undefined);
const permissionWriteScope = ref<HTMLInputElement | undefined>(undefined);

// Computes Properties
// -----------------------------------------------------------------------------
const editPermission = computed<boolean>(
  () => props.editUserPermission != undefined,
);

const possibleSubFolders = computed<string[]>(() =>
  findSubFolders(props.subFolders, []),
);

const permissionUserReadonly = computed<boolean>(() => {
  return formState.readonly || editPermission.value;
});

// Watchers
// -----------------------------------------------------------------------------
watch(
  () => props.bucketName,
  (newBucketName) => {
    updateLocalPermission();
    permission.bucket_name = newBucketName;
  },
);

watch(
  () => props.editUserPermission,
  () => updateLocalPermission(),
);

// Functions
// -----------------------------------------------------------------------------
/**
 * Reset the form. Triggered when the modal is closed.
 */
function modalClosed() {
  formState.readonly = props.readonly;
  formState.error = false;
  if (editPermission.value) {
    updateLocalPermission();
  }
}

/**
 * Callback when the toast is hidden again.
 */
function toastHidden() {
  permissionDeleted.value = false;
}

/**
 * Check if an input should be visible based on its state
 * @param input Input which visibility should be determined.
 */
function inputVisible(input?: number | string | null): boolean {
  return !formState.readonly || input != undefined;
}

/**
 * Update the form content
 */
function updateLocalPermission() {
  if (props.editUserPermission != undefined) {
    permission.bucket_name = props.editUserPermission.bucket_name;
    permission.file_prefix = props.editUserPermission.file_prefix;
    permission.uid = props.editUserPermission.uid;
    permission.from_timestamp = props.editUserPermission.from_timestamp;
    permission.to_timestamp = props.editUserPermission.to_timestamp;
    permission.scopes = props.editUserPermission.scopes;
  } else {
    permission.file_prefix = undefined;
    permission.uid = "";
    permission.from_timestamp = undefined;
    permission.to_timestamp = undefined;
    permission.scopes = [];
  }
}

/**
 * Find recursively all sub folders based on the folder structure
 * @param currentFolder Current Folder
 * @param parentFolders All parent folders
 */
function findSubFolders(
  currentFolder: FolderTree,
  parentFolders: string[],
): string[] {
  const arr: string[] = [];
  for (const subFolder of Object.keys(currentFolder.subFolders)) {
    const subFolderString =
      (parentFolders.length > 0 ? parentFolders.join("/") + "/" : "") +
      subFolder +
      (subFolder.endsWith("/") ? "" : "/");
    arr.push(
      subFolderString,
      ...findSubFolders(
        currentFolder.subFolders[subFolder],
        subFolderString.slice(0, subFolderString.length - 1).split("/"),
      ),
    );
  }
  return arr;
}

function formCheckValidity(): boolean {
  permissionWriteScope.value?.setCustomValidity("");
  permissionReadScope.value?.setCustomValidity("");
  if (permissionForm.value?.checkValidity()) {
    if (permission.scopes.length === 0) {
      permissionWriteScope.value?.setCustomValidity(
        "At least one scope must be set",
      );
      permissionReadScope.value?.setCustomValidity(
        "At least one scope must be set",
      );
      return false;
    }
    return true;
  }
  return false;
}

/**
 * Submit the form
 */
function formSubmit() {
  formState.error = false;
  if (formCheckValidity()) {
    formState.loading = true;
    const serverAnswerPromise = editPermission.value
      ? bucketRepository.updateBucketPermission(
          permission.bucket_name,
          permission.uid,
          {
            to_timestamp: permission.to_timestamp,
            from_timestamp: permission.from_timestamp,
            scopes: permission.scopes,
            file_prefix: permission.file_prefix,
          } as BucketPermissionParameters,
        )
      : bucketRepository.createBucketPermission(permission);
    serverAnswerPromise
      .then(() => {
        permissionModal?.hide();
        successToast?.show();
        updateLocalPermission();
      })
      .catch((e) => {
        formState.error = true;
        formState.duplicate =
          e.response?.data?.["detail"]?.includes("already exists");
      })
      .finally(() => {
        formState.loading = false;
      });
  }
}

/**
 * Delete a permission for a bucket user combination
 * @param permission Bucket permission to delete
 */
function confirmedDeletePermission(permission: BucketPermissionOut) {
  if (!formState.loading) {
    formState.loading = true;
    bucketRepository
      .deleteBucketPermission(permission.bucket_name, permission.uid)
      .then(() => {
        permissionDeleted.value = true;
        emit("permission-deleted");
        permissionModal?.hide();
        successToast?.show();
      })
      .catch(() => {
        formState.error = true;
      })
      .finally(() => {
        formState.loading = false;
      });
  }
}

function updateUser(user: UserOut) {
  permission.uid = user.uid;
}

// Lifecycle Hooks
// -----------------------------------------------------------------------------
onMounted(() => {
  permissionModal = new Modal("#" + props.modalId);
  successToast = new Toast("#" + "toast-" + randomIDSuffix);
  updateLocalPermission();
});

function fromTimestampChanged(target?: HTMLInputElement | null) {
  permission.from_timestamp = target?.value
    ? dayjs(target?.value).unix()
    : undefined;
}

function toTimestampChanged(target?: HTMLInputElement | null) {
  permission.to_timestamp = target?.value
    ? dayjs(target?.value).unix()
    : undefined;
}
</script>

<template>
  <delete-permission-modal
    :modal-id="'delete-permission-modal' + randomIDSuffix"
    :permission="editUserPermission"
    :back-modal-id="modalId"
    @confirm-delete="confirmedDeletePermission"
  />
  <search-user-modal
    :modal-id="'search-user-modal' + randomIDSuffix"
    :back-modal-id="modalId"
    filter-user-self
    @user-found="updateUser"
  />
  <bootstrap-toast
    :toast-id="'toast-' + randomIDSuffix"
    v-on="{ 'hidden.bs.toast': toastHidden }"
  >
    Successfully
    <template v-if="permissionDeleted">deleted</template>
    <template v-else-if="editPermission">edited</template>
    <template v-else>created</template>
    Permission
  </bootstrap-toast>
  <bootstrap-modal
    :modal-id="modalId"
    static-backdrop
    modal-label="Permission Modal"
    v-on="{ 'hidden.bs.modal': modalClosed }"
  >
    <template v-if="formState.readonly" #header>View Permission</template>
    <template v-else-if="props.editUserPermission !== undefined" #header
      >Edit Permission
    </template>
    <template v-else #header>Create new Bucket Permission</template>
    <template #extra-button>
      <font-awesome-icon
        v-if="props.deletable"
        icon="fa-solid fa-trash"
        class="me-2 cursor-pointer"
        :class="{ 'hover-danger': !formState.loading }"
        data-bs-toggle="modal"
        :data-bs-target="'#delete-permission-modal' + randomIDSuffix"
      />
      <font-awesome-icon
        v-if="formState.readonly && props.editable"
        icon="fa-solid fa-pen"
        class="pseudo-link cursor-pointer"
        @click="formState.readonly = false"
      />
    </template>
    <template #body>
      <form
        :id="'permissionCreateEditForm' + randomIDSuffix"
        ref="permissionForm"
        @submit.prevent="formSubmit"
      >
        <div class="mb-3 row">
          <label for="bucketNameInput" class="col-2 col-form-label"
            >Bucket<span v-if="!formState.readonly">*</span></label
          >
          <div class="col-10">
            <input
              id="bucketNameInput"
              type="text"
              readonly
              class="form-control-plaintext"
              required
              :value="permission.bucket_name"
            />
          </div>
        </div>
        <div class="mb-3 row align-items-center d-flex">
          <label for="permissionGranteeInput" class="col-2 col-form-label">
            User<span v-if="!formState.readonly">*</span>
          </label>
          <div class="col-10">
            <input
              id="permissionGranteeInput"
              type="text"
              class="form-control"
              required
              placeholder="Search for a user"
              :value="nameRepository.getName(permission.uid)"
              readonly
              :data-bs-toggle="permissionUserReadonly ? null : 'modal'"
              :data-bs-target="
                permissionUserReadonly
                  ? null
                  : '#search-user-modal' + randomIDSuffix
              "
            />
          </div>
        </div>
        <div class="mb-3 row">
          <div class="mb-1">
            Scopes<span v-if="!formState.readonly">*</span>
          </div>
          <div class="form-check form-check-inline w-fit ms-3">
            <input
              :id="`scope-permission-read-${randomIDSuffix}`"
              ref="permissionReadScope"
              v-model="permission.scopes"
              class="form-check-input"
              type="checkbox"
              :disabled="formState.readonly"
              value="read"
            />
            <label
              class="form-check-label text-uppercase"
              :for="`scope-permission-read-${randomIDSuffix}`"
            >
              read
            </label>
          </div>
          <div class="form-check form-check-inline w-fit">
            <input
              :id="`scope-permission-write-${randomIDSuffix}`"
              ref="permissionWriteScope"
              v-model="permission.scopes"
              class="form-check-input"
              type="checkbox"
              :disabled="formState.readonly"
              value="write"
            />
            <label
              class="form-check-label text-uppercase"
              :for="`scope-permission-write-${randomIDSuffix}`"
            >
              write
            </label>
          </div>
        </div>
        <div class="mb-3 row">
          <label
            v-if="inputVisible(permission.from_timestamp)"
            for="permissionDateFromInput"
            class="col-2 col-form-label"
          >
            From
          </label>
          <div v-if="inputVisible(permission.from_timestamp)" class="col-4">
            <input
              id="permissionDateFromInput"
              type="date"
              class="form-control"
              :readonly="formState.readonly"
              :min="dayjs().format('YYYY-MM-DD')"
              :value="
                permission.from_timestamp
                  ? dayjs.unix(permission.from_timestamp).format('YYYY-MM-DD')
                  : undefined
              "
              @input="
                (event) =>
                  fromTimestampChanged(event.target as HTMLInputElement)
              "
            />
          </div>
          <label
            v-if="inputVisible(permission.to_timestamp)"
            for="permissionDateToInput"
            class="col-2 col-form-label"
          >
            To
          </label>
          <div v-if="inputVisible(permission.to_timestamp)" class="col-4">
            <input
              id="permissionDateToInput"
              type="date"
              class="form-control"
              :readonly="formState.readonly"
              :min="
                permission.from_timestamp != null
                  ? dayjs
                      .unix(permission.from_timestamp)
                      .add(1, 'day')
                      .format('YYYY-MM-DD')
                  : dayjs().add(1, 'day').format('YYYY-MM-DD')
              "
              :value="
                permission.to_timestamp
                  ? dayjs.unix(permission.to_timestamp).format('YYYY-MM-DD')
                  : undefined
              "
              @input="
                (event) => toTimestampChanged(event.target as HTMLInputElement)
              "
            />
          </div>
        </div>
        <div
          v-if="
            inputVisible(permission.file_prefix) &&
            possibleSubFolders.length > 0
          "
          class="mb-3 row align-items-center d-flex"
        >
          <label for="permissionSubFolderInput" class="col-2 col-form-label">
            Subfolder
          </label>
          <div
            :class="{
              'col-10': formState.readonly,
              'col-9': !formState.readonly,
            }"
          >
            <select
              id="permissionSubFolderInput"
              v-model="permission.file_prefix"
              class="form-select"
              :disabled="formState.readonly"
            >
              <option disabled selected>Select one folder...</option>
              <option
                v-for="folder in possibleSubFolders"
                :key="folder"
                :value="folder"
              >
                {{ folder }}
              </option>
            </select>
          </div>
          <div v-if="!formState.readonly" class="col-1">
            <button
              type="button"
              class="btn btn-outline-danger btn-sm float-end"
              :hidden="permission.file_prefix == undefined"
              @click="permission.file_prefix = undefined"
            >
              <font-awesome-icon icon="fa-solid fa-x" />
            </button>
          </div>
        </div>
      </form>
      <div v-if="formState.error" class="text-danger">
        <span v-if="formState.duplicate"
          >Permission for the user
          {{ nameRepository.getName(permission.uid) }} already exists</span
        ><span v-else>There was some kind of error<br />Try again later</span>
      </div>
    </template>
    <template #footer>
      <button
        v-if="backModalId !== undefined"
        type="button"
        class="btn btn-secondary"
        :data-bs-target="'#' + props.backModalId"
        data-bs-toggle="modal"
      >
        Back
      </button>
      <button
        v-else
        type="button"
        class="btn btn-secondary"
        data-bs-dismiss="modal"
      >
        Close
      </button>
      <button
        v-if="!formState.readonly"
        type="submit"
        :form="'permissionCreateEditForm' + randomIDSuffix"
        class="btn btn-primary"
        :disabled="
          formState.loading ||
          permission.uid.length === 0 ||
          permission.scopes.length === 0
        "
      >
        <span
          v-if="formState.loading"
          class="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
        Save
      </button>
    </template>
  </bootstrap-modal>
</template>

<style scoped>
.pseudo-link {
  color: var(--bs-secondary);
}

.pseudo-link:hover {
  color: var(--bs-link-hover-color);
}
</style>
