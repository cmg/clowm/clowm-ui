/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
const windowEnv: Record<string, string> = window["env"];
export const environment: env = {
  S3_URL: windowEnv["s3Url"],
  API_BASE_URL: windowEnv["apiUrl"],
  MATOMO_SITE_ID: parseInt(windowEnv["matomoSiteId"]),
  MATOMO_HOST: windowEnv["matomoHost"],
  DEV_SYSTEM: windowEnv["devSystem"].toLowerCase() === "true",
};

type env = {
  S3_URL: string;
  API_BASE_URL: string;
  MATOMO_SITE_ID: number;
  MATOMO_HOST: string;
  DEV_SYSTEM: boolean;
};
