export type SizeModifierType = "sm" | "lg";

export type Colors =
  | "primary"
  | "secondary"
  | "success"
  | "warning"
  | "danger"
  | "info"
  | "black"
  | "white";

export type ExtendedColors =
  | Colors
  | "primary-subtle"
  | "secondary-subtle"
  | "success-subtle"
  | "warning-subtle"
  | "danger-subtle"
  | "info-subtle";
