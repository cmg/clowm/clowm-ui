export type FlatWorkflowParameters = Record<
  string,
  string | number | boolean | undefined
>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type NestedWorkflowParameters = Record<string, any>;

export type WorkflowMetaParameters = {
  logs_s3_path?: string | null;
  debug_s3_path?: string | null;
  provenance_s3_path?: string | null;
  notes?: string | null;
};

export type TemporaryParams = {
  params?: NestedWorkflowParameters;
  metaParams?: WorkflowMetaParameters;
};

// helper interface for anyOf or oneOf relations in json schema
export interface ParameterCombinationDependencies {
  group: string[]; // AND group in OR combination
  dependencies: string[][]; // (X)OR combination to fulfill
}
