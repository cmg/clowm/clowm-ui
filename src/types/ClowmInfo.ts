export type ClowmInfo = {
  inputParameters: string[];
  outputParameters: string[];
  resourceParameters?: string[];
  exampleParameters?: Record<string, string | boolean | number>;
  dois?: string[];
};
