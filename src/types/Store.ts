export type IdMapping<Type> = {
  [key: string]: Type;
};
