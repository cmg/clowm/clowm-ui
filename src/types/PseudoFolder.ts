import type { _Object as S3Object } from "@aws-sdk/client-s3";

export interface S3ObjectWithFolder extends S3Object {
  folder: string[];
  pseudoFileName: string;
}

export type S3PseudoFolder = {
  Size: number;
  quantity: number;
  parentFolder: string[];
  LastModified: Date;
  name: string;
  Key: string;
};

export type FolderTree = {
  subFolders: Record<string, FolderTree>;
  files: S3ObjectWithFolder[];
};
