import type { WorkflowVersionOut } from "@/client/types.gen.ts";
import type {
  NestedWorkflowParameters,
  FlatWorkflowParameters,
} from "@/types/WorkflowParameters";

export function sortedVersions(
  versions: WorkflowVersionOut[],
  desc = true,
): WorkflowVersionOut[] {
  const vs = [...versions];
  if (desc) {
    vs.sort((a, b) => (a.created_at < b.created_at ? 1 : -1));
  } else {
    vs.sort((a, b) => (a.created_at < b.created_at ? -1 : 1));
  }
  return vs;
}

export function latestVersion(
  versions: WorkflowVersionOut[],
): WorkflowVersionOut | undefined {
  if (versions == undefined || versions.length == 0) {
    return undefined;
  }
  const vs = sortedVersions(versions);
  return vs[0];
}

export function flattenParameters(
  params: NestedWorkflowParameters,
  parents: string[] = [],
): FlatWorkflowParameters {
  let newParams: FlatWorkflowParameters = {};
  for (const paramName of Object.keys(params)) {
    if (typeof params[paramName] === "object" && params[paramName] !== null) {
      newParams = {
        ...newParams,
        ...flattenParameters(params[paramName], [...parents, paramName]),
      };
      continue;
    }
    newParams[[...parents, paramName].join(".")] = params[paramName];
  }
  return newParams;
}

export function nestParameters(
  params: FlatWorkflowParameters,
): NestedWorkflowParameters {
  const newParams: NestedWorkflowParameters = {};
  for (const paramName of Object.keys(params)) {
    const paramParts = paramName.split(".");
    if (paramParts.length === 1) {
      newParams[paramParts[0]] = params[paramParts[0]];
      continue;
    }
    let local = newParams;
    let lastKnownPartNumber = 0;
    for (const partNumber in paramParts) {
      if (local[paramParts[partNumber]] != undefined) {
        local = local[paramParts[partNumber]];
        lastKnownPartNumber++;
        continue;
      }
      break;
    }
    local[paramParts[lastKnownPartNumber]] = createNestedObjectFromArray(
      paramParts.slice(lastKnownPartNumber + 1),
      params[paramName],
    );
  }
  return newParams;
}

function createNestedObjectFromArray(
  arr: string[],
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  key: any,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Record<string, any> | any {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const f: Record<string, any> = {};
  switch (arr.length) {
    case 0:
      return key;
    case 1:
      f[arr[0]] = key;
      break;
    default:
      f[arr[0]] = createNestedObjectFromArray(arr.slice(1), key);
      break;
  }
  return f;
}
