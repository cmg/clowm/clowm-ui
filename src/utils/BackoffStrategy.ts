abstract class BackoffStrategy {
  protected currentVal: number;
  protected iteration: number;
  protected reachedMax: boolean;
  protected maxValue: number;

  constructor(maxValue?: number) {
    this.currentVal = 0;
    this.iteration = 0;
    this.reachedMax = false;
    this.maxValue = maxValue ?? 300;
  }

  protected abstract computeNextValue(): number;

  public reset() {
    this.iteration = 0;
    this.currentVal = 0;
    this.reachedMax = false;
  }

  public *generator(): Generator<number> {
    while (true) {
      this.iteration++;
      if (this.reachedMax) {
        yield this.maxValue;
      } else {
        this.currentVal = this.computeNextValue();
        if (0 < this.maxValue && this.maxValue < this.currentVal) {
          this.reachedMax = true;
          yield this.maxValue;
        } else {
          yield this.currentVal;
        }
      }
    }
  }
}

export class ExponentialBackoff extends BackoffStrategy {
  protected computeNextValue(): number {
    return 2 << (this.iteration - 1);
  }
}

export class NoBackoff extends BackoffStrategy {
  private readonly constantValue: number;

  constructor(constantValue?: number) {
    super();
    this.constantValue = constantValue ?? 30;
  }

  protected computeNextValue(): number {
    return this.constantValue;
  }
}

export class LinearBackoff extends BackoffStrategy {
  private readonly backoff: number;

  constructor(backoff?: number) {
    super();
    this.backoff = backoff ?? 5;
  }

  protected computeNextValue(): number {
    return this.currentVal + this.backoff;
  }
}
