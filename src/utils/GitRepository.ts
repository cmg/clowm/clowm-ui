import axios from "axios";
import type { AxiosInstance, AxiosBasicCredentials } from "axios";
import type { WorkflowModeOut, WorkflowModeIn } from "@/client/types.gen";
import { DocumentationEnum } from "@/client/types.gen";

interface CheckDocResult {
  found?: string;
  missing?: string[];
}

interface CheckRepoResult {
  docs: Record<DocumentationEnum, CheckDocResult>;
  mainScriptMissing?: string;
}

function repositoryPaths(document: DocumentationEnum): string[] {
  switch (document) {
    case DocumentationEnum.USAGE_MD:
      return ["clowm/README.md", "README.md"];
    case DocumentationEnum.INPUT_MD:
      return ["clowm/usage.md", "docs/usage.md"];
    case DocumentationEnum.OUTPUT_MD:
      return ["clowm/output.md", "docs/output.md"];
    case DocumentationEnum.PARAMETER_SCHEMA_JSON:
      return ["clowm/nextflow_schema.json", "nextflow_schema.json"];
    case DocumentationEnum.CHANGELOG_MD:
      return ["clowm/CHANGELOG.md", "CHANGELOG.md"];
    case DocumentationEnum.CLOWM_INFO_JSON:
      return ["clowm/clowm_info.json", "clowm_info_json"];
    case DocumentationEnum.CITATIONS_MD:
      return ["clowm/CITATIONS.md", "CITATIONS.md"];
  }
}

export function determineGitIcon(repo_url?: string): string {
  let gitProvider = "git-alt";
  if (repo_url != null) {
    if (repo_url.includes("github")) {
      gitProvider = "github";
    } else if (repo_url.includes("gitlab")) {
      gitProvider = "gitlab";
    } else if (repo_url.includes("bitbucket")) {
      gitProvider = "bitbucket";
    }
  }
  return "fa-brands fa-".concat(gitProvider);
}

const capturingMainScriptRegex =
  /manifest\s+{[\s\S]+mainScript\s+=\s+["'](?<scriptname>\S+)["']/;

export abstract class GitRepository {
  protected repo: URL;
  protected gitCommitHash: string;
  protected token?: string;
  protected httpClient: AxiosInstance;
  protected abstract readonly repoAvailabilityUrl: string;

  protected constructor(
    repoUrl: string,
    gitCommitHash: string,
    token?: string,
  ) {
    this.repo = new URL(repoUrl);
    this.gitCommitHash = encodeURIComponent(gitCommitHash);
    if (token != undefined && token.length > 0) {
      this.token = token;
    }
    this.httpClient = axios.create({
      maxRedirects: 2,
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async downloadFile(filepath: string): Promise<any> {
    return await this.httpClient.get(await this.downloadFileUrl(filepath));
  }

  async checkRepoAvailability(): Promise<boolean> {
    try {
      await this.httpClient.get(this.repoAvailabilityUrl);
      return true;
    } catch {
      return false;
    }
  }

  async validateRepo(
    modes: WorkflowModeIn[] | WorkflowModeOut[],
  ): Promise<CheckRepoResult> {
    const files = await this.getFileList();
    const filteredFiles = files.filter(
      // files in base directory and docs directory
      (file) =>
        !file.includes("/") ||
        file.startsWith("docs/") ||
        file.startsWith("clowm/"),
    );
    const checkRepoResult: CheckRepoResult = {
      docs: {
        "changelog.md": {},
        "usage.md": {},
        "parameter_schema.json": {},
        "clowm_info.json": {},
        "input.md": {},
        "output.md": {},
        "CITATIONS.md": {},
      },
      mainScriptMissing: undefined,
    };
    // Check if main.nf is there and if not, check for an alternative name in the nextflow.config
    if (filteredFiles.findIndex((file) => file === "main.nf") === -1) {
      try {
        const nextflowConfig = (await this.downloadFile("nextflow.config"))
          .data;
        const entryScriptName = nextflowConfig.match(capturingMainScriptRegex)
          .groups?.["scriptname"];
        if (entryScriptName == undefined) {
          checkRepoResult.mainScriptMissing = "main.nf";
        } else if (files.findIndex((file) => file === entryScriptName) === -1) {
          checkRepoResult.mainScriptMissing = entryScriptName;
        }
      } catch {
        checkRepoResult.mainScriptMissing = "main.nf";
      }
    }

    // Check all documentation files
    for (const doc of Object.values(DocumentationEnum)) {
      // if parameter schema and modes are present, use check modes schema instead of default one
      if (doc === DocumentationEnum.PARAMETER_SCHEMA_JSON && modes.length > 0) {
        continue;
      }
      // loop over possible locations
      for (const repositoryPath of repositoryPaths(doc)) {
        // if a file is found, stop the search and set the found location
        if (filteredFiles.findIndex((file) => file === repositoryPath) > -1) {
          checkRepoResult.docs[doc].found = repositoryPath;
          break;
        }
      }
      // if at no location the file is found, set the missing file string
      if (checkRepoResult.docs[doc].found == undefined) {
        checkRepoResult.docs[doc].missing = [repositoryPaths(doc).join(" or ")];
      }
    }
    // check schema path of each mode
    for (const schemaPath of modes.map((mode) => mode.schema_path)) {
      if (files.findIndex((file) => file === schemaPath) === -1) {
        if (
          checkRepoResult.docs[DocumentationEnum.PARAMETER_SCHEMA_JSON]
            .missing == undefined
        ) {
          checkRepoResult.docs[
            DocumentationEnum.PARAMETER_SCHEMA_JSON
          ].missing = [schemaPath];
        } else {
          checkRepoResult.docs[
            DocumentationEnum.PARAMETER_SCHEMA_JSON
          ].missing.push(schemaPath);
        }
      } else {
        if (
          checkRepoResult.docs[DocumentationEnum.PARAMETER_SCHEMA_JSON].found ==
          undefined
        ) {
          checkRepoResult.docs[DocumentationEnum.PARAMETER_SCHEMA_JSON].found =
            schemaPath;
        }
      }
    }
    return checkRepoResult;
  }

  protected abstract downloadFileUrl(filepath: string): Promise<string>;

  abstract getFileList(): Promise<string[]>;

  static buildRepository(
    repoUrl: string,
    gitCommitHash: string,
    token?: string,
  ): GitRepository {
    if (repoUrl.includes("github")) {
      return new GithubRepository(repoUrl, gitCommitHash, token);
    } else if (repoUrl.includes("gitlab")) {
      return new GitlabRepository(repoUrl, gitCommitHash, token);
    }
    throw new Error("Repository is not supported.");
  }
}

class GithubRepository extends GitRepository {
  private readonly account: string;
  private readonly repoName: string;
  protected readonly repoAvailabilityUrl: string;

  constructor(repoUrl: string, gitCommitHash: string, token?: string) {
    super(repoUrl, gitCommitHash, token);
    const pathParts = this.repo.pathname.slice(1).split("/");
    this.account = pathParts[0];
    this.repoName = pathParts[1];
    this.repoAvailabilityUrl = `https://api.github.com/repos/${this.account}/${this.repoName}/git/commits/${this.gitCommitHash}`;
    if (token) {
      this.httpClient.interceptors.request.use((req) => {
        if (!req.url?.includes("raw")) {
          req.auth = {
            password: this.token,
            username: this.account,
          } as AxiosBasicCredentials;
        }
        return req;
      });
    }
    this.httpClient.interceptors.request.use((req) => {
      if (!req.url?.includes("raw")) {
        req.headers["X-GitHub-Api-Version"] = "2022-11-28";
        req.headers.setAccept("application/vnd.github.object+json");
      }
      return req;
    });
  }

  fileUrl(filepath: string): string {
    return `https://api.github.com/repos/${this.account}/${
      this.repoName
    }/contents/${encodeURIComponent(filepath)}?ref=${this.gitCommitHash}`;
  }

  protected async downloadFileUrl(filepath: string): Promise<string> {
    if (this.token == undefined) {
      return Promise.resolve(
        `https://raw.githubusercontent.com/${this.account}/${this.repoName}/${this.gitCommitHash}/${filepath}`,
      );
    }
    return (
      await this.httpClient.get(
        this.fileUrl(filepath) + `&time=${new Date().getTime()}`,
      )
    ).data["download_url"];
  }

  async getFileList(): Promise<string[]> {
    const response = await this.httpClient.get(
      `https://api.github.com/repos/${this.account}/${
        this.repoName
      }/git/trees/${this.gitCommitHash}?recursive=true`,
    );
    return response.data["tree"].map(
      (file: Record<string, string>): string => file["path"],
    );
  }
}

class GitlabRepository extends GitRepository {
  private readonly host: string;
  private readonly project: string;
  protected readonly repoAvailabilityUrl: string;

  constructor(repoUrl: string, gitCommitHash: string, token?: string) {
    super(repoUrl, gitCommitHash, token);
    const url = new URL(repoUrl);
    this.host = url.host;
    this.project = encodeURIComponent(url.pathname.slice(1));
    this.repoAvailabilityUrl = `https://${this.host}/api/v4/projects/${this.project}/repository/commits/${this.gitCommitHash}?stats=false`;
    if (token != undefined) {
      this.httpClient.interceptors.request.use((req) => {
        req.headers.setAuthorization(`Bearer ${token}`);
        return req;
      });
    }
  }

  protected downloadFileUrl(filepath: string): Promise<string> {
    return Promise.resolve(
      `https://${this.host}/api/v4/projects/${
        this.project
      }/repository/files/${encodeURIComponent(
        filepath,
      )}/raw?ref=${this.gitCommitHash}`,
    );
  }

  async getFileList(): Promise<string[]> {
    const fileList: string[] = [];
    let response = await this.httpClient.get(
      `https://${this.host}/api/v4/projects/${
        this.project
      }/repository/tree?ref=${this.gitCommitHash}&recursive=true&per_page=99&pagination=keyset&order_by=id&sort=asc`,
    );
    fileList.push(
      ...response.data.map(
        (file: Record<string, string>): string => file["path"],
      ),
    );
    while (response.headers?.["link"] != undefined) {
      const linkHeader: string = response.headers?.["link"];

      response = await this.httpClient.get(
        linkHeader.split(";")[0].slice(1, -1),
      );
      fileList.push(
        ...response.data.map(
          (file: Record<string, string>): string => file["path"],
        ),
      );
    }
    return fileList;
  }
}
