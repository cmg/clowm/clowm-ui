/**
 * Calculate MD5 hash of a string
 *
 * @param {string} d The string to hash.
 * @returns {number} The hexdigest.
 */
export declare function md5(d: string): string;
