export function createDownloadUrl(content: string, mimeType?: string): string {
  const blob = new Blob([content], {
    type: mimeType,
  });
  return URL.createObjectURL(blob);
}
