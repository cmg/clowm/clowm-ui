<script setup lang="ts">
import { computed, onMounted, reactive } from "vue";
import { useResourceStore } from "@/stores/resources";
import CardTransitionGroup from "@/components/transitions/CardTransitionGroup.vue";
import ResourceCard from "@/components/resources/ResourceCard.vue";
import CreateResourceModal from "@/components/resources/modals/CreateResourceModal.vue";
import UploadResourceInfoModal from "@/components/resources/modals/UploadResourceInfoModal.vue";
import { useS3KeyStore } from "@/stores/s3keys";
import type {
  OwnershipTransferRequestOut,
  ResourceOut,
  ResourceVersionOut,
} from "@/client/types.gen";
import { OwnershipTypeEnum } from "@/client/types.gen";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import UpdateResourceModal from "@/components/resources/modals/UpdateResourceModal.vue";
import ReasonModal from "@/components/modals/ReasonModal.vue";
import BootstrapToast from "@/components/BootstrapToast.vue";
import { Modal, Toast, Tooltip } from "bootstrap";
import OtrModal from "@/components/modals/ShowOtrModal.vue";
import CreateOtrModal from "@/components/modals/CreateOtrModal.vue";
import ListOtrsModal from "@/components/modals/ListOtrsModal.vue";
import { useOTRStore } from "@/stores/otrs";
import { useUserStore } from "@/stores/users";
import { useSettingsStore } from "@/stores/settings";
import DeleteResourceModal from "@/components/resources/modals/DeleteResourceModal.vue";

const resourceRepository = useResourceStore();
const s3KeyRepository = useS3KeyStore();
const otrRepository = useOTRStore();
const userRepository = useUserStore();
const settingsStore = useSettingsStore();

let showOtrModalInstance: Modal | null = null;
let createOtrModalInstance: Modal | null = null;
let requestReasonModal: Modal | null = null;
let deleteModal: Modal | null = null;
let syncRequestSuccessToast: Toast | null = null;

const resourceState = reactive<{
  loading: boolean;
  resourceVersionInfo?: ResourceVersionOut;
  updateResource: ResourceOut;
  syncResourceVersion?: ResourceVersionOut;
  deleteResource: string;
  showOtrTarget: string;
  createOtrTarget: ResourceOut;
}>({
  loading: true,
  resourceVersionInfo: undefined,
  syncResourceVersion: undefined,
  deleteResource: "",
  showOtrTarget: "",
  createOtrTarget: {
    name: "",
    description: "",
    source: "",
    resource_id: "",
    versions: [],
    maintainer_id: "",
  },
  updateResource: {
    name: "",
    description: "",
    source: "",
    resource_id: "",
    versions: [],
    maintainer_id: "",
  },
});

const otrForNewResources = computed<OwnershipTransferRequestOut[]>(() =>
  otrRepository.resourceOtrs.filter(
    (otr) => otr.new_owner_uid === userRepository.currentUID,
  ),
);

function setResourceVersionInfo(resourceVersionInfo?: ResourceVersionOut) {
  resourceState.resourceVersionInfo = resourceVersionInfo;
}

function setResourceUpdate(resource: ResourceOut) {
  resourceState.updateResource = resource;
}

function setResourceSync(version: ResourceVersionOut) {
  resourceState.syncResourceVersion = version;
}

function showOtrModal(resourceId: string) {
  resourceState.showOtrTarget = resourceId;
  showOtrModalInstance?.show();
}

function showCreateOtrModal(resource: ResourceOut) {
  resourceState.createOtrTarget = resource;
  createOtrModalInstance?.show();
}

function showDeleteResourceModal(resource: ResourceOut) {
  resourceState.deleteResource = resource.resource_id;
  deleteModal?.show();
}

function deleteResource(resource_id: string) {
  resourceRepository.delete(resource_id);
}

function requestResourceSync(
  reason: string,
  resourceVersion?: ResourceVersionOut,
) {
  if (resourceVersion != undefined) {
    resourceRepository
      .requestSynchronization(resourceVersion, {
        reason: reason,
      })
      .then(() => {
        requestReasonModal?.hide();
        syncRequestSuccessToast?.show();
      });
  }
}

onMounted(() => {
  new Tooltip("#showResourceOtrsButtons");
  let fetchedResources = false;
  requestReasonModal = Modal.getOrCreateInstance(
    "#request-synchronization-modal",
  );
  syncRequestSuccessToast = Toast.getOrCreateInstance("#request-sync-toast");
  showOtrModalInstance = Modal.getOrCreateInstance("#view-resource-otr-modal");
  createOtrModalInstance = Modal.getOrCreateInstance(
    "#create-resource-otr-modal",
  );
  deleteModal = Modal.getOrCreateInstance("#delete-resource-modal");
  otrRepository.fetchOwnOtrs(OwnershipTypeEnum.RESOURCE);
  s3KeyRepository.fetchS3Keys(() => {
    if (!fetchedResources) {
      fetchedResources = true;
      resourceRepository.fetchOwnResources(() => {
        resourceState.loading = false;
      });
    }
  });
});
</script>

<template>
  <delete-resource-modal
    modal-id="delete-resource-modal"
    :resource="
      resourceRepository.ownResourceMapping[resourceState.deleteResource]
    "
    @confirm-delete="deleteResource"
  />
  <otr-modal
    :otr-target-id="resourceState.showOtrTarget"
    modal-id="view-resource-otr-modal"
  />
  <list-otrs-modal
    :otrs="otrForNewResources"
    :otr-type="OwnershipTypeEnum.RESOURCE"
    modal-id="list-resource-otrs-modal"
  />
  <create-otr-modal
    modal-id="create-resource-otr-modal"
    :target="resourceState.createOtrTarget"
  />
  <bootstrap-toast toast-id="request-sync-toast" color-class="success">
    Requested resource synchronization
  </bootstrap-toast>
  <reason-modal
    modal-id="request-synchronization-modal"
    modal-label=""
    :loading="false"
    purpose="request"
    @save="
      (reason) => requestResourceSync(reason, resourceState.syncResourceVersion)
    "
  >
    <template #header> Request resource synchronization</template>
  </reason-modal>
  <create-resource-modal modal-id="createResourceModal" />
  <upload-resource-info-modal
    modal-id="uploadResourceInfoModal"
    :resource-version="resourceState.resourceVersionInfo"
  />
  <update-resource-modal
    :resource="resourceState.updateResource"
    modal-id="updateResourceModal"
  />
  <div
    class="row border-bottom mb-4 justify-content-between align-items-center pb-2"
  >
    <h2 class="w-fit">My Resources</h2>
    <div class="w-fit">
      <button
        id="showResourceOtrsButtons"
        :hidden="otrForNewResources.length === 0"
        class="btn border btn-lg shadow-sm position-relative me-3"
        :class="{
          'btn-light': settingsStore.lightThemeActive,
          'btn-secondary': settingsStore.darkThemeActive,
        }"
        data-bs-title="Ownership transfer requests"
        data-bs-target="#list-resource-otrs-modal"
        data-bs-toggle="modal"
      >
        Requests
        <span
          class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger"
          style="font-size: 0.8em"
        >
          {{ otrForNewResources.length }}
          <span class="visually-hidden"
            >open resource ownership transfer requests</span
          >
        </span>
      </button>
      <button
        type="button"
        class="btn btn-info btn-lg w-fit me-3"
        data-bs-toggle="modal"
        data-bs-target="#uploadResourceInfoModal"
        @click="setResourceVersionInfo(undefined)"
      >
        Tutorial
      </button>
      <button
        class="btn btn-lg btn-primary w-fit"
        data-bs-toggle="modal"
        data-bs-target="#createResourceModal"
      >
        Create Resource
      </button>
    </div>
  </div>
  <div v-if="!resourceState.loading">
    <div
      v-if="resourceRepository.ownResources.length === 0"
      class="text-center fs-2 mt-5"
    >
      <font-awesome-icon
        icon="fa-solid fa-x"
        class="my-5 fs-0"
        style="color: var(--bs-secondary)"
      />
      <p>You don't have any resources registered in the system.</p>
    </div>
    <CardTransitionGroup
      v-else
      class="d-flex flex-wrap align-items-center justify-content-between"
    >
      <resource-card
        v-for="resource in resourceRepository.ownResources"
        :key="resource.resource_id"
        :resource="resource"
        :loading="false"
        style="width: 48%"
        extended
        @click-info="setResourceVersionInfo"
        @click-update="setResourceUpdate"
        @click-request-sync="setResourceSync"
        @click-show-otr="showOtrModal"
        @click-create-otr="showCreateOtrModal"
        @click-delete="showDeleteResourceModal"
      />
    </CardTransitionGroup>
  </div>
  <div
    v-else
    class="d-flex flex-wrap align-items-center justify-content-between"
  >
    <resource-card
      v-for="resource in 4"
      :key="resource"
      :resource="{
        name: '',
        description: '',
        source: '',
        resource_id: '',
        versions: [],
        maintainer_id: '',
      }"
      style="min-width: 48%"
      loading
      extended
    />
  </div>
</template>

<style scoped></style>
