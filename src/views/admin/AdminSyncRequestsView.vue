<script setup lang="ts">
import BootstrapToast from "@/components/BootstrapToast.vue";
import ReasonModal from "@/components/modals/ReasonModal.vue";
import ResourceVersionInfoModal from "@/components/resources/modals/ResourceVersionInfoModal.vue";
import { onMounted, reactive } from "vue";
import type {
  ResourceOut,
  ResourceVersionOut,
  UserSynchronizationRequestOut,
} from "@/client/types.gen";
import { ResourceVersionStatus } from "@/client/types.gen";
import { useResourceStore } from "@/stores/resources";
import { useNameStore } from "@/stores/names";
import { useUserStore } from "@/stores/users";
import { Modal, Toast } from "bootstrap";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { useSettingsStore } from "@/stores/settings";

const resourceState = reactive<{
  sendingRequest: boolean;
  loading: boolean;
  resources: Record<string, ResourceOut>;
  inspectResource?: ResourceOut;
  rejectResource?: ResourceVersionOut;
  inspectVersionIndex: number;
}>({
  sendingRequest: false,
  loading: true,
  resources: {},
  inspectResource: undefined,
  rejectResource: undefined,
  inspectVersionIndex: 0,
});

const resourceRepository = useResourceStore();
const nameRepository = useNameStore();
const userRepository = useUserStore();
const settingsStore = useSettingsStore();

let rejectReasonModal: Modal | null = null;
let successToast: Toast | null = null;
let rejectToast: Toast | null = null;
let refreshTimeout: NodeJS.Timeout | undefined = undefined;

function rejectSyncRequest(
  reason: string,
  resourceVersion?: ResourceVersionOut,
) {
  if (resourceVersion != undefined) {
    resourceState.sendingRequest = true;
    resourceRepository
      .syncResource(resourceVersion, { deny: true, reason })
      .then(() => {
        rejectReasonModal?.hide();
        rejectToast?.show();
      })
      .finally(() => {
        resourceState.sendingRequest = false;
      });
  }
}

function syncVersion(resourceId: string, resourceVersionId: string) {
  resourceState.sendingRequest = true;
  resourceRepository
    .syncResource(
      {
        resource_version_id: resourceVersionId,
        resource_id: resourceId,
        release: "",
        created_at: 0,
        s3_path: "",
        cluster_path: "",
        status: ResourceVersionStatus.SYNC_REQUESTED,
        compressed_size: 0,
      },
      { deny: false },
    )
    .then(() => {
      successToast?.show();
      delete resourceState.resources[resourceId];
    })
    .finally(() => {
      resourceState.sendingRequest = false;
    });
}

function fetchResources(
  syncRequests: UserSynchronizationRequestOut[],
): Promise<ResourceOut[]> {
  return Promise.all(
    syncRequests.map((request) =>
      resourceRepository.fetchResource(request.resource_id, [
        ResourceVersionStatus.SYNC_REQUESTED,
      ]),
    ),
  );
}

function fetchUserNames(
  syncRequests: UserSynchronizationRequestOut[],
): UserSynchronizationRequestOut[] {
  userRepository.fetchUsernames(
    syncRequests.map((request) => request.requester_id),
  );
  return syncRequests;
}

function fetchRequests() {
  resourceState.loading = true;
  resourceRepository
    .fetchSyncRequests(() => {
      resourceState.loading = false;
    })
    .then(fetchUserNames)
    .then(fetchResources)
    .then((resources) => {
      const newMapping: Record<string, ResourceOut> = {};
      for (const resource of resources) {
        newMapping[resource.resource_id] = resource;
      }
      resourceState.resources = newMapping;
    });
}

function clickRefreshRequests() {
  clearTimeout(refreshTimeout);
  refreshTimeout = setTimeout(() => {
    fetchRequests();
  }, 500);
}

onMounted(() => {
  fetchRequests();

  rejectReasonModal = new Modal("#sync-request-reject-modal");
  successToast = new Toast("#accept-sync-request-toast");
  rejectToast = new Toast("#reject-sync-request-toast");
});
</script>

<template>
  <bootstrap-toast toast-id="accept-sync-request-toast" color-class="success">
    Syncing resource to the cluster
  </bootstrap-toast>
  <bootstrap-toast toast-id="reject-sync-request-toast" color-class="danger">
    Rejected resource synchronization request
  </bootstrap-toast>
  <resource-version-info-modal
    modal-id="sync-request-resource-version-info-modal"
    :resource-version-index="resourceState.inspectVersionIndex"
    :resource="resourceState.inspectResource"
  />
  <reason-modal
    modal-id="sync-request-reject-modal"
    modal-label="Resource Synchronization Request Reject Modal"
    :loading="resourceState.sendingRequest"
    purpose="rejection"
    @save="(reason) => rejectSyncRequest(reason, resourceState.rejectResource)"
  >
    <template #header>
      Reject Resource Synchronization Request
      <b>{{ resourceState.rejectResource?.release }}</b>
    </template>
  </reason-modal>
  <div
    class="d-flex border-bottom mb-4 justify-content-between align-items-center flex-wrap gap-2"
  >
    <h2 class="w-fit">Review resource synchronization requests</h2>
    <span
      id="refreshReviewableResourcesButton"
      class="w-fit mb-2"
      tabindex="0"
      data-bs-title="Refresh Reviewable Resources"
      data-bs-toggle="tooltip"
    >
      <button
        type="button"
        class="btn shadow-sm border w-fit"
        :class="{
          'btn-light': settingsStore.lightThemeActive,
          'btn-secondary': settingsStore.darkThemeActive,
        }"
        :disabled="resourceState.loading || resourceState.sendingRequest"
        @click="clickRefreshRequests"
      >
        <font-awesome-icon icon="fa-solid fa-arrow-rotate-right" />
        <span class="visually-hidden">Refresh Reviewable Resources</span>
      </button>
    </span>
  </div>
  <div v-if="resourceState.loading" class="text-center mt-5">
    <div class="spinner-border" style="width: 3rem; height: 3rem" role="status">
      <span class="visually-hidden">Loading...</span>
    </div>
  </div>
  <div
    v-else-if="resourceRepository.syncRequests.length === 0"
    class="text-center mt-5 fs-4"
  >
    There are currently no resource synchronization requests
  </div>
  <div v-else class="d-flex flex-column">
    <div
      v-for="request in resourceRepository.syncRequests"
      :key="request.resource_version_id"
      class="border p-2 pb-0 rounded mb-2 d-flex hover-card"
    >
      <div class="flex-grow-1">
        <h6>
          {{ nameRepository.getName(request.resource_id) }}@{{
            nameRepository.getName(request.resource_version_id)
          }}
        </h6>
        <div>
          <b>Requester</b>: {{ nameRepository.getName(request.requester_id) }}
        </div>
        <div><b>Reason</b>:</div>
        <p>{{ request.reason }}</p>
      </div>
      <div class="d-flex flex-column justify-content-evenly align-items-center">
        <button
          type="button"
          class="btn btn-secondary btn-sm"
          data-bs-toggle="modal"
          data-bs-target="#sync-request-resource-version-info-modal"
          @click="
            resourceState.inspectResource =
              resourceState.resources[request.resource_id];
            resourceState.inspectVersionIndex = resourceState.resources[
              request.resource_id
            ].versions.findIndex(
              (version) =>
                version.resource_version_id === request.resource_version_id,
            );
          "
        >
          Inspect Resource
        </button>
        <div class="btn-group">
          <button
            type="button"
            class="btn btn-success btn-sm"
            :disabled="resourceState.sendingRequest"
            @click="
              syncVersion(request.resource_id, request.resource_version_id)
            "
          >
            Accept
          </button>
          <button
            type="button"
            class="btn btn-danger btn-sm"
            data-bs-toggle="modal"
            data-bs-target="#sync-request-reject-modal"
            :disabled="resourceState.sendingRequest"
            @click="
              resourceState.rejectResource = resourceState.resources[
                request.resource_id
              ].versions.find(
                (version) =>
                  version.resource_version_id === request.resource_version_id,
              )
            "
          >
            Reject
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<style scoped>
.hover-card:hover {
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
}
</style>
