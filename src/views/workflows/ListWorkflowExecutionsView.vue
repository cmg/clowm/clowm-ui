<script setup lang="ts">
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { onMounted, reactive, computed, onUnmounted } from "vue";
import type { WorkflowExecutionOut } from "@/client/types.gen";
import dayjs from "dayjs";
import { Tooltip } from "bootstrap";
import { useWorkflowStore } from "@/stores/workflows";
import { useWorkflowExecutionStore } from "@/stores/workflowExecutions";
import ParameterModal from "@/components/workflows/modals/ParameterModal.vue";
import { ExponentialBackoff } from "@/utils/BackoffStrategy";
import { useNameStore } from "@/stores/names";
import MarkdownRenderer from "@/components/MarkdownRenderer.vue";
import { useSettingsStore } from "@/stores/settings";
import DeleteExecutionModal from "@/components/workflows/modals/DeleteExecutionModal.vue";

const workflowRepository = useWorkflowStore();
const nameRepository = useNameStore();
const executionRepository = useWorkflowExecutionStore();
const settingsStore = useSettingsStore();
const backoff = new ExponentialBackoff();

let refreshTimeout: NodeJS.Timeout | undefined = undefined;
let pollingTimeout: NodeJS.Timeout | undefined = undefined;

const executionsState = reactive<{
  loading: boolean;
  executionToDelete?: WorkflowExecutionOut;
  executionParameters?: string;
}>({
  loading: true,
  executionToDelete: undefined,
  executionParameters: undefined,
});

const statusToColorMapping = {
  PENDING: "bg-warning",
  SCHEDULED: "bg-warning",
  RUNNING: "bg-info",
  CANCELED: "bg-danger",
  SUCCESS: "bg-success",
  ERROR: "bg-danger",
};

const statusToIconMapping = {
  PENDING: "fa-solid fa-circle-pause",
  SCHEDULED: "fa-solid fa-circle-pause",
  RUNNING: "fa-solid fa-circle-play",
  CANCELED: "fa-solid fa-circle-xmark",
  SUCCESS: "fa-solid fa-circle-check",
  ERROR: "fa-solid fa-circle-xmark",
};

const sortedExecutions = computed<WorkflowExecutionOut[]>(() => {
  const tempList = [...executionRepository.executions];
  tempList.sort((a, b) => {
    // sort by start time descending
    return dayjs.unix(a.start_time).isBefore(dayjs.unix(b.start_time)) ? 1 : -1;
  });
  return tempList;
});

// Functions
// -----------------------------------------------------------------------------
function updateExecutions() {
  backoff.reset();
  clearTimeout(pollingTimeout);
  executionRepository
    .fetchExecutions(() => {
      executionsState.loading = false;
    })
    .then(() => {
      if (runningExecutions.value.length > 0) {
        refreshRunningWorkflowExecutionTimer();
      }
    });
}

function refreshExecutions() {
  clearTimeout(refreshTimeout);
  refreshTimeout = setTimeout(() => {
    updateExecutions();
  }, 500);
}

function workflowExecutionCancelable(execution: WorkflowExecutionOut): boolean {
  return execution.end_time == undefined;
}

function deleteWorkflowExecution(executionId?: string) {
  if (executionId) {
    executionRepository.deleteExecution(executionId);
  }
}

function cancelWorkflowExecution(executionId: string) {
  executionRepository.cancelExecution(executionId);
}

const runningExecutions = computed<WorkflowExecutionOut[]>(() =>
  executionRepository.executions.filter((execution) =>
    workflowExecutionCancelable(execution),
  ),
);

function refreshRunningWorkflowExecution() {
  Promise.all(
    runningExecutions.value.map((execution) =>
      executionRepository.fetchExecution(execution.execution_id),
    ),
  );
}

async function refreshRunningWorkflowExecutionTimer() {
  for (const sleep of backoff.generator()) {
    await new Promise((resolve) => {
      pollingTimeout = setTimeout(resolve, sleep * 1000);
    });
    refreshRunningWorkflowExecution();
  }
}

onMounted(() => {
  updateExecutions();
  workflowRepository.fetchWorkflows();
  new Tooltip("#refreshExecutionsButton");
});

onUnmounted(() => {
  clearTimeout(pollingTimeout);
});
</script>

<template>
  <delete-execution-modal
    modal-id="deleteWorkflowExecutionModal"
    :execution="executionsState.executionToDelete"
    @confirm-delete="deleteWorkflowExecution"
  />
  <parameter-modal
    modal-id="workflowExecutionParameterModal"
    :execution-id="executionsState.executionParameters"
  />
  <div
    class="row border-bottom mb-4 justify-content-between align-items-center"
  >
    <h2 class="mb-2 w-fit">My Workflow Executions</h2>
    <div class="w-fit">
      <button
        id="refreshExecutionsButton"
        class="btn me-2 shadow-sm border"
        :class="{
          'btn-light': settingsStore.lightThemeActive,
          'btn-secondary': settingsStore.darkThemeActive,
        }"
        data-bs-title="Refresh Workflow Executions"
        data-bs-toggle="tooltip"
        @click="refreshExecutions"
      >
        <font-awesome-icon icon="fa-solid fa-arrow-rotate-right" />
        <span class="visually-hidden">Refresh Data Buckets</span>
      </button>
      <router-link
        :to="{ name: 'workflows' }"
        class="btn btn-primary shadow-sm border"
        >Start Workflow Execution
      </router-link>
    </div>
  </div>
  <div class="overflow-x-auto" style="max-width: 100vw">
    <table class="table table-hover caption-top align-middle">
      <caption>
        Displaying
        {{
          executionRepository.executions.length
        }}
        workflow execution(s) worth
        {{
          executionRepository.executions
            .reduce(
              (acc, cur) => acc.add(dayjs.duration(cur.cpu_time)),
              dayjs.duration(0, "day"),
            )
            .asHours()
            .toFixed(2)
        }}
        CPU hours
      </caption>
      <thead>
        <tr>
          <th scope="col">Workflow</th>
          <th scope="col">Status</th>
          <th scope="col">Started</th>
          <th scope="col">Duration</th>
          <th scope="col">Ended</th>
          <th scope="col">CPU hours [HH:mm:ss]</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <template v-if="executionsState.loading">
          <tr v-for="n in 5" :key="n">
            <td class="placeholder-glow w-25">
              <span class="placeholder col-6"></span>
            </td>
            <td class="placeholder-glow" style="width: 15%">
              <span class="placeholder col-4"></span>
            </td>
            <td class="placeholder-glow" style="width: 15%">
              <span class="placeholder col-6"></span>
            </td>
            <td class="placeholder-glow" style="width: 15%">
              <span class="placeholder col-6"></span>
            </td>
            <td class="text-end">
              <div
                class="btn-group btn-group-sm dropdown-center dropdown-menu-start"
              >
                <button type="button" class="btn btn-secondary border" disabled>
                  Details
                </button>
                <button
                  type="button"
                  class="btn btn-secondary dropdown-toggle dropdown-toggle-split"
                  disabled
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
              </div>
            </td>
          </tr>
        </template>
        <template v-else-if="executionRepository.executions.length > 0">
          <tr
            v-for="execution in sortedExecutions"
            :key="execution.execution_id"
          >
            <td>
              <router-link
                v-if="execution.workflow_id && execution.workflow_version_id"
                :to="{
                  name: 'workflow-version',
                  params: {
                    versionId: execution.workflow_version_id,
                    workflowId: execution.workflow_id,
                  },
                }"
              >
                {{ nameRepository.getName(execution.workflow_id) }}@{{
                  nameRepository.getName(execution.workflow_version_id)
                }}
              </router-link>
              <markdown-renderer
                v-else-if="execution.notes"
                class="execution-notes"
                :markdown="execution.notes"
              />
              <span v-else>Deleted Workflow</span>
            </td>
            <td>
              <span
                class="rounded-pill py-1 px-2 text-light text-nowrap"
                :class="statusToColorMapping[execution.status]"
              >
                <font-awesome-icon
                  class="me-2"
                  :icon="statusToIconMapping[execution.status]"
                />{{ execution.status }}</span
              >
            </td>
            <td>
              {{
                dayjs.unix(execution.start_time).format("MMM DD, YYYY HH:mm")
              }}
            </td>
            <td>
              <template v-if="execution.end_time">
                {{
                  dayjs
                    .duration(
                      execution.end_time - execution.start_time,
                      "seconds",
                    )
                    .humanize()
                }}
              </template>
              <template v-else
                >{{ dayjs.unix(execution.start_time).toNow(true) }}
              </template>
            </td>
            <td>
              <template v-if="execution.end_time">
                {{
                  dayjs.unix(execution.end_time).format("MMM DD, YYYY HH:mm")
                }}
              </template>
              <template v-else> -</template>
            </td>
            <td class="text-center">
              {{ Math.floor(dayjs.duration(execution.cpu_time).asHours()) }}:{{
                dayjs.duration(execution.cpu_time).format("mm:ss")
              }}
            </td>
            <td class="text-end">
              <div
                class="btn-group btn-group-sm dropdown-center dropdown-menu-start"
              >
                <button type="button" class="btn btn-secondary" disabled>
                  Details
                </button>
                <button
                  type="button"
                  class="btn btn-secondary dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu">
                  <li>
                    <button
                      class="dropdown-item align-middle"
                      type="button"
                      data-bs-toggle="modal"
                      data-bs-target="#workflowExecutionParameterModal"
                      @click="
                        executionsState.executionParameters =
                          execution.execution_id
                      "
                    >
                      <span class="ms-1">Parameters</span>
                    </button>
                  </li>
                  <li v-if="workflowExecutionCancelable(execution)">
                    <button
                      class="dropdown-item text-danger align-middle"
                      type="button"
                      @click="cancelWorkflowExecution(execution.execution_id)"
                    >
                      <font-awesome-icon icon="fa-solid fa-ban" />
                      <span class="ms-1">Cancel</span>
                    </button>
                  </li>
                  <li v-if="!workflowExecutionCancelable(execution)">
                    <button
                      class="dropdown-item text-danger align-middle"
                      type="button"
                      data-bs-toggle="modal"
                      data-bs-target="#deleteWorkflowExecutionModal"
                      @click="executionsState.executionToDelete = execution"
                    >
                      <font-awesome-icon
                        icon="fa-solid fa-trash"
                        class="me-1"
                      />
                      <span>Delete</span>
                    </button>
                  </li>
                </ul>
              </div>
            </td>
          </tr>
        </template>
        <tr v-else>
          <td colspan="6" class="text-center"><i>No workflow executions</i></td>
        </tr>
      </tbody>
    </table>
  </div>
</template>

<style scoped></style>
