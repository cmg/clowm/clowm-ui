<script setup lang="ts">
import { computed, onMounted, reactive, watch } from "vue";
import { WorkflowVersionStatus } from "@/client/types.gen";
import type {
  WorkflowOut,
  WorkflowStatistic,
  WorkflowVersionOut,
} from "@/client/types.gen";
import { WorkflowService } from "@/client/sdk.gen";
import WorkflowStatisticsChart from "@/components/workflows/WorkflowStatisticsChart.vue";
import { useRoute, useRouter } from "vue-router";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import {
  latestVersion as calculateLatestVersion,
  sortedVersions,
} from "@/utils/Workflow";
import { determineGitIcon } from "@/utils/GitRepository";
import { useUserStore } from "@/stores/users";
import { useWorkflowStore } from "@/stores/workflows";
import { useNameStore } from "@/stores/names";

const workflowRepository = useWorkflowStore();
const userRepository = useUserStore();
const nameRepository = useNameStore();

// Props
// =============================================================================
const props = defineProps<{
  workflowId: string;
  versionId?: string;
  workflowModeId?: string;
  developerView: boolean;
}>();

// Constants
// =============================================================================
const router = useRouter();
const route = useRoute();

// Reactive State
// =============================================================================
const workflowState = reactive<{
  loading: boolean;
  initialOpen: boolean;
  stats: WorkflowStatistic[];
}>({
  loading: true,
  initialOpen: true,
  stats: [],
});

// Watchers
// =============================================================================
watch(
  () => props.workflowId,
  (newWorkflowId, oldWorkflowId) => {
    if (newWorkflowId !== oldWorkflowId) {
      updateWorkflow(newWorkflowId);
    }
  },
);

// Computed Properties
// =============================================================================
const workflow = computed<WorkflowOut | undefined>(() =>
  props.developerView
    ? workflowRepository.comprehensiveWorkflowMapping[props.workflowId]
    : workflowRepository.workflowMapping[props.workflowId],
);

const latestVersion = computed<WorkflowVersionOut | undefined>(() =>
  calculateLatestVersion(
    workflow.value?.versions?.filter(
      (version) => version.status == WorkflowVersionStatus.PUBLISHED,
    ) || [],
  ),
);
const activeVersion = computed<WorkflowVersionOut | undefined>(() =>
  workflow.value?.versions.find(
    (w) => w.workflow_version_id === props.versionId,
  ),
);

const activeVersionModeIds = computed<string[]>(
  () => activeVersion.value?.modes ?? [],
);

const activeVersionString = computed<string>(
  () => activeVersion.value?.version ?? "",
);

const activeVersionIcon = computed<string | undefined>(
  () => activeVersion.value?.icon_url ?? undefined,
);

const versionLaunchable = computed<boolean>(
  () => activeVersion.value?.status == WorkflowVersionStatus.PUBLISHED,
);

const gitIcon = computed<string>(() =>
  determineGitIcon(workflow.value?.repository_url),
);

const allowVersionDeprecation = computed<boolean>(() => {
  if (activeVersion.value?.status === WorkflowVersionStatus.PUBLISHED) {
    if (userRepository.reviewer || userRepository.admin) {
      return true;
    } else if (
      userRepository.workflowDev &&
      workflow.value?.developer_id === userRepository.currentUID
    ) {
      return true;
    }
  }
  return false;
});

const repositoryUrl = computed<string>(() => {
  if (workflow.value == undefined) {
    return "";
  }
  const url = workflow.value?.repository_url;
  if (activeVersion.value != undefined) {
    return url + `/tree/${activeVersion.value?.workflow_version_id}`;
  }
  return url;
});

// Functions
// =============================================================================
function updateWorkflow(workflowId: string) {
  workflowState.loading = true;
  workflowRepository
    .fetchWorkflow(props.workflowId, props.developerView, () => {
      workflowState.loading = false;
      workflowState.initialOpen = false;
    })
    .then((workflow) => {
      if (workflow.developer_id) {
        userRepository.fetchUsernames([workflow.developer_id]);
      }
      return workflow;
    })
    .then((workflow) => {
      document.title = workflow.name + " - CloWM";
      if (props.versionId == undefined) {
        updateVersion(
          calculateLatestVersion(
            workflow.versions.filter(
              (version) => version.status == WorkflowVersionStatus.PUBLISHED,
            ) || [],
          )?.workflow_version_id,
        );
      }
    });

  WorkflowService.workflowGetWorkflowStatistics({
    path: { wid: workflowId },
  }).then((response) => {
    workflowState.stats = response.data!;
  });
}

function updateVersion(workflowVersionId?: string) {
  if (workflowVersionId != undefined) {
    const possibleModes = getModesForVersion(workflowVersionId);
    let modeId: string | undefined = undefined;
    if (
      (props.workflowModeId == undefined && possibleModes.length > 0) || // next version needs a mode
      (props.workflowId != undefined && // next version has not the current mode
        possibleModes.length > 0 &&
        !possibleModes.includes(props.workflowId))
    ) {
      modeId = possibleModes[0];
    } else if (
      // next version has the same mode as the current mode
      props.workflowId != undefined &&
      possibleModes.length > 0 &&
      possibleModes.includes(props.workflowId)
    ) {
      modeId = props.workflowModeId;
    }
    router.replace({
      name: "workflow-version",
      params: {
        ...route.params,
        versionId: workflowVersionId,
      },
      query: { ...route.query, workflowModeId: modeId },
    });
  }
}

function updateMode(modeId?: string) {
  router.replace({
    params: {
      ...route.params,
    },
    query: { ...route.query, workflowModeId: modeId },
  });
}

function getModesForVersion(workflowVersionId: string): string[] {
  return (
    workflow.value?.versions.find(
      (w) => w.workflow_version_id === workflowVersionId,
    )?.modes ?? []
  );
}

function deprecateCurrentWorkflowVersion() {
  if (props.versionId) {
    workflowRepository.deprecateWorkflowVersion(
      props.workflowId,
      props.versionId,
    );
  }
}

// Lifecycle Events
// =============================================================================
onMounted(() => {
  updateWorkflow(props.workflowId);
});
</script>

<template>
  <div v-if="workflowState.loading">
    <div
      class="d-flex mt-5 justify-content-between align-items-center placeholder-glow"
    >
      <span class="fs-0 placeholder col-6"></span>
      <span class="fs-0 placeholder col-1"></span>
    </div>
    <div class="fs-4 mb-5 mt-4 placeholder-glow">
      <span class="placeholder col-10"></span>
    </div>
    <div class="row align-items-center placeholder-glow my-1">
      <span class="mx-auto col-2 placeholder bg-success fs-0"></span>
      <span class="position-absolute end-0 col-1 placeholder fs-2"></span>
    </div>
    <div class="row w-100 mb-4 mt-3 mx-0 placeholder-glow">
      <span class="placeholder col-3 mx-auto"></span>
    </div>
  </div>
  <div v-else-if="workflow">
    <div class="d-flex justify-content-between align-items-center">
      <h3 class="w-fit">
        {{ workflow.name }}
        <span v-if="activeVersionString">@{{ activeVersionString }}</span>
        <span v-if="nameRepository.getName(workflow.developer_id)" class="fs-4">
          registered by
          {{ nameRepository.getName(workflow.developer_id) }}</span
        >
      </h3>
      <img
        v-if="activeVersionIcon != null"
        :src="activeVersionIcon"
        class="img-fluid icon"
        alt="Workflow icon"
      />
    </div>
    <p class="fs-5 mt-3">{{ workflow?.short_description }}</p>
    <div
      v-if="activeVersionModeIds.length > 0"
      class="row align-items-center mb-3 fs-5"
    >
      <label class="col-sm-1 col-form-label" for="workflowModeSelect"
        ><b>Mode:</b></label
      >
      <div class="col-sm-11">
        <select
          id="workflowModeSelect"
          class="form-select w-fit"
          @change="updateMode(($event?.target as HTMLSelectElement)?.value)"
        >
          <option
            v-for="modeId of activeVersionModeIds"
            :key="modeId"
            :value="modeId"
          >
            {{ workflowRepository.modeMapping[modeId]?.name }}
          </option>
        </select>
      </div>
    </div>
    <template v-if="route.name !== 'workflow-start'">
      <div
        v-if="!versionLaunchable"
        class="alert alert-warning w-fit mx-auto"
        role="alert"
      >
        This version can not be used.
        <router-link
          v-if="latestVersion"
          class="alert-link"
          :to="{
            name: 'workflow-version',
            params: {
              versionId: latestVersion.workflow_version_id,
            },
            query: { tab: route.query.tab },
          }"
          >Try the latest version {{ latestVersion.version }}.
        </router-link>
      </div>
      <div v-if="props.versionId" class="row align-items-center">
        <div class="w-fit position-absolute start-0">
          <button
            v-if="allowVersionDeprecation"
            type="button"
            class="btn btn-warning"
            @click="deprecateCurrentWorkflowVersion"
          >
            Deprecate version
          </button>
        </div>
        <router-link
          role="button"
          class="btn btn-success btn-lg w-fit mx-auto"
          :class="{ disabled: !versionLaunchable }"
          :to="{
            name: 'workflow-start',
            params: {
              versionId: props.versionId,
              workflowId: props.workflowId,
            },
            query: {
              workflowModeId: props.workflowModeId,
              viewMode: 'simple',
            },
          }"
        >
          <font-awesome-icon icon="fa-solid fa-play" class="me-2" />
          <span class="align-middle">Launch {{ activeVersionString }}</span>
        </router-link>
        <div
          v-if="latestVersion"
          class="input-group w-fit position-absolute end-0"
        >
          <span id="workflow-version-wrapping" class="input-group-text px-2"
            ><font-awesome-icon icon="fa-solid fa-tags" class="text-secondary"
          /></span>
          <select
            id="workflowVersionSelect"
            class="form-select form-select-sm"
            aria-label="Workflow version selection"
            aria-describedby="workflow-version-wrapping"
            @change="
              updateVersion(($event?.target as HTMLSelectElement)?.value)
            "
          >
            <option
              v-for="version in sortedVersions(workflow.versions)"
              :key="version.workflow_version_id"
              :value="version.workflow_version_id"
              :selected="version.workflow_version_id === props.versionId"
            >
              {{ version.version }}
            </option>
          </select>
        </div>
      </div>
      <div class="row w-100 mb-4 mt-2 mx-0 border-bottom pb-2">
        <a
          :href="repositoryUrl"
          target="_blank"
          class="text-secondary text-decoration-none mx-auto w-fit p-0"
        >
          <font-awesome-icon :icon="gitIcon" class="me-1" />
          <span class="align-middle"> {{ workflow?.repository_url }}</span>
          <font-awesome-icon
            v-if="workflow?.private"
            icon="fa-solid fa-lock"
            class="ms-1"
          />
        </a>
      </div>
      <workflow-statistics-chart
        v-if="workflowState.stats"
        :stats="workflowState.stats"
      />
    </template>
  </div>
  <router-view v-if="workflowState.loading || workflow" />
  <div v-else class="text-center fs-1 mt-5">
    <font-awesome-icon
      icon="fa-solid fa-magnifying-glass"
      class="my-5 fs-0"
      style="color: var(--bs-secondary)"
    />
    <p class="my-5">
      Could not find any Workflow with ID <br />'{{ workflowId }}'
    </p>
    <router-link :to="{ name: 'workflows' }" class="mt-5">Back</router-link>
  </div>
</template>

<style scoped>
.icon {
  max-width: 64px;
  max-height: 64px;
}
</style>
