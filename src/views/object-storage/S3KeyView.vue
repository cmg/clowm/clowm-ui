<script setup lang="ts">
import type { S3Key } from "@/client";
import { ref, watch } from "vue";
import FontAwesomeIcon from "@/components/FontAwesomeIcon.vue";
import { environment } from "@/environment";
import CopyToClipboardIcon from "@/components/CopyToClipboardIcon.vue";
import DeleteS3KeyModal from "@/components/object-storage/modals/DeleteS3KeyModal.vue";

const props = defineProps<{
  s3key: S3Key;
  deletable: boolean;
  loading: boolean;
}>();

const emit = defineEmits<{
  (e: "delete-key", accessKey: string): void;
}>();

watch(
  () => props.s3key.access_key,
  () => {
    visibleSecretKey.value = false;
  },
);
const visibleSecretKey = ref<boolean>(false);

function deleteKeyTrigger(keyId: string) {
  if (props.deletable) {
    emit("delete-key", keyId);
  }
}
</script>

<template>
  <delete-s3-key-modal
    modal-id="delete-key-modal"
    :access-key="props.s3key.access_key"
    @confirm-delete="deleteKeyTrigger"
  />
  <h3>S3 Endpoint:</h3>
  <div class="input-group mb-2">
    <input
      id="s3-endpoint"
      class="form-control"
      type="text"
      :value="environment.S3_URL"
      aria-label="S3 Endpoint"
      readonly
    />
    <span id="s3-endpoint-copy" class="input-group-text"
      ><copy-to-clipboard-icon :text="environment.S3_URL"
    /></span>
  </div>
  <h4>Access Key:</h4>
  <div v-if="props.loading" class="placeholder-glow">
    <span class="placeholder col-5 mt-3 mb-2 fs-4"></span><br />
  </div>
  <div v-else class="input-group mb-2">
    <input
      id="s3-access-key"
      class="form-control"
      type="text"
      :value="props.s3key.access_key"
      aria-label="S3 Access Key"
      readonly
    />
    <span id="s3-secret-key-copy" class="input-group-text"
      ><copy-to-clipboard-icon :text="props.s3key.access_key"
    /></span>
  </div>
  <h4>Secret Key:</h4>
  <div v-if="props.loading" class="placeholder-glow">
    <span class="placeholder col-7 mt-3 mb-4 fs-4"></span><br />
  </div>
  <div v-else class="input-group fs-4 mb-3">
    <span
      class="input-group-text cursor-pointer"
      @click="visibleSecretKey = !visibleSecretKey"
    >
      <font-awesome-icon
        :icon="visibleSecretKey ? 'fa-solid fa-eye' : 'fa-solid fa-eye-slash'"
      />
    </span>
    <input
      id="s3-secret-key"
      class="form-control"
      :type="visibleSecretKey ? 'text' : 'password'"
      :value="props.s3key.secret_key"
      aria-label="S3 Access Key"
      readonly
    />
    <span id="s3-secret-key-copy" class="input-group-text"
      ><copy-to-clipboard-icon :text="props.s3key.secret_key"
    /></span>
  </div>
  <button
    type="button"
    class="btn btn-danger fs-5 mb-3"
    :disabled="!props.deletable || props.loading"
    data-bs-toggle="modal"
    data-bs-target="#delete-key-modal"
  >
    Delete
  </button>
  <h4>Suggested CLI tools</h4>
  <p>
    While this website supports file uploads to S3 buckets, some users may
    benefit from using CLI tools for enhanced performance, greater configuration
    flexibility, and advanced features. The following list includes CLI tools
    verified to work with CloWM's S3 buckets. <br />
    <b>Note</b>: this list is not exhaustive and doesn't indicate that other S3
    clients won't work.
  </p>
  <ul>
    <li>
      <a
        target="_blank"
        href="https://min.io/docs/minio/linux/reference/minio-mc.html"
        >MinIO client</a
      >
    </li>
    <li>
      <a
        target="_blank"
        href="https://github.com/peak/s5cmd?tab=readme-ov-file#google-cloud-storage-support"
        >s5cmd</a
      >
    </li>
    <li><a target="_blank" href="https://rclone.org/s3/#ceph">Rclone</a></li>
  </ul>
  <div class="alert alert-warning" role="alert">
    <h5 class="alert-heading">Incompatible S3 SDKs</h5>
    <p>
      Since January 16, 2025 the AWS S3 SDKs adopts new
      <a
        href="https://docs.aws.amazon.com/sdkref/latest/guide/feature-dataintegrity.html"
        >default integrity protections</a
      >
      that are not compatible with our Ceph version yet. If you are using an AWS
      S3 SDK to communicate with our storage you have to pin the version of your
      SDK to be compatible.
    </p>
    <div>
      Here is a list of programming languages and the supported SDK version:
    </div>
    <ul>
      <li>
        <a href="https://aws.amazon.com/sdk-for-cpp/">C++</a>
        <code>&lt;v1.11.486</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-go/">Go</a>
        <code>&lt;v1.73.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-java/">Java</a>
        <code>&lt;v2.30.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-javascript/"
          >Javascript/Typescript</a
        >
        <code>&lt;v3.729.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-kotlin/">Kotlin</a>
        <code>&lt;v1.4.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-net/">.NET</a>
        <code>&lt;v3.7.412.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-php/">PHP</a>
        <code>&lt;v3.337.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-python/">Python</a>
        <code>&lt;v1.36.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-ruby/">Ruby</a>
        <code>&lt;v1.178.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-rust/">Rust</a>
        <code>&lt;v1.69.0</code>
      </li>
      <li>
        <a href="https://aws.amazon.com/sdk-for-swift/">Swift</a>
        <code>&lt;v1.1.0</code>
      </li>
    </ul>
  </div>
</template>

<style scoped></style>
