<script setup lang="ts">
import { computed, onBeforeMount, onBeforeUnmount, onMounted } from "vue";
import { useRouter } from "vue-router";
import { useUserStore, useWorkflowStore } from "@/stores";
import BootstrapCard from "@/components/BootstrapCard.vue";
import dayjs from "dayjs";
import type { PublicWorkflowOut } from "@/client";

const router = useRouter();
const authRepo = useUserStore();
const workflowRepository = useWorkflowStore();

onBeforeMount(() => {
  if (authRepo.authenticated) {
    // If user is authenticated redirect him to the dashboard
    router.push({ name: "dashboard" });
  } else {
    const linkTag = document.createElement("link");
    linkTag.setAttribute("rel", "canonical");
    linkTag.href = window.location.origin + window.location.pathname;
    document.head.appendChild(linkTag);
  }
});

const publicWorkflows = computed(() => {
  const tmpArr = Object.values(workflowRepository.publicWorkflowMapping);
  tmpArr.sort((a, b) =>
    a.latest_version_timestamp < b.latest_version_timestamp ? 1 : -1,
  );
  return tmpArr;
});

function repositoryCommitUrl(workflow: PublicWorkflowOut) {
  return workflow.repository_url + `/tree/${workflow.latest_git_commit_hash}`;
}

onMounted(() => {
  workflowRepository.fetchPublicWorkflows();
});

onBeforeUnmount(() => {
  document.head.querySelector("[rel=canonical]")?.remove();
});
</script>

<template>
  <div class="row align-items-center my-lg-5 pb-5">
    <div class="col-md text-center">
      <div class="mb-3 text-center">
        <div class="fs-1 fw-bold">CloWM</div>
        <img
          src="/src/assets/images/clowm.svg"
          class="mb-3 img-fluid"
          width="300"
          height="300"
          alt="CloWM Logo"
        />
        <div class="fs-2 fw-bold">
          The <span class="clowm-blue fw-semibold">Clo</span>ud-based
          <span class="clowm-red fw-semibold">W</span>orkflow
          <span class="clowm-green fw-semibold">M</span>anager
        </div>
      </div>
    </div>
    <div class="col-md">
      <h1>An integrated platform for highly scalable workflow execution</h1>
      <div class="fs-4 text-secondary">
        The <span class="clowm-blue fw-semibold">Clo</span>ud-based
        <span class="clowm-red fw-semibold">W</span>orkflow
        <span class="clowm-green fw-semibold">M</span>anager (<span
          class="clowm-blue fw-bold"
          >Clo</span
        >
        <span class="clowm-red fw-bold">W</span>
        <span class="clowm-green fw-bold">M </span>
        <span title="Pronounced like clown with a 'm'" style="cursor: help"
          >/kla&#650;m/</span
        >) offers the seamless integration of (1) curated, scientific
        <a href="#available-workflows">workflows</a> written in the Nextflow
        DSL, (2) robust data storage, (3) a highly scalable compute layer for
        data-intensive analysis tasks and a (4) user-friendly interface. The
        CloWM platform is completely open-source and can be used free-of-charge.
      </div>
    </div>
  </div>

  <div class="row mt-4 align-items-center gap-2">
    <div class="col-md">
      <h2>Curated, best-practice workflows</h2>
      <div class="fs-5 text-secondary order-md-last">
        CloWM transforms command-line driven workflows into user-friendly web
        services, making them easily accessible and usable. Workflows undergo a
        rigorous review process before being published on the platform, ensuring
        they adhere to best practices. The majority of open-source workflows
        from well known repositories like
        <a href="https://nf-co.re/">nf-core</a> and
        <a href="https://github.com/epi2me-labs">EPI2ME Labs</a> are either
        directly runnable on the CloWM platform or require only a minimal amount
        of adaptation.
      </div>
    </div>
    <div class="col-md text-center order-md-first">
      <img
        src="/src/assets/images/code.jpg"
        class="img-fluid rounded"
        style="max-height: 300px"
        alt="code photo"
      />
    </div>
  </div>
  <div class="row mt-4 pt-4 align-items-center gap-2">
    <div class="col-md">
      <h2>Personalized S3 Storage</h2>
      <div class="fs-5 text-secondary">
        For efficient data storage and retrieval, CloWM provisions personalized
        S3 Buckets with the software-defined storage platform
        <a href="https://ceph.io">Ceph</a>. Such data buckets can be easily
        accessed and also shared with other users through CloWM's integrated
        bucket browser. Advanced users have direct access to the S3 endpoint and
        can use third-party tools for more complex tasks.
      </div>
    </div>
    <div class="col-md text-center">
      <img
        src="/src/assets/images/storage.jpg"
        class="img-fluid rounded"
        style="max-height: 300px"
        alt="hard drive photo"
      />
    </div>
  </div>
  <div class="row mt-4 align-items-center gap-2">
    <div class="col-md">
      <h2>Build on a highly-scalable infrastructure</h2>
      <div class="fs-5 text-secondary order-md-last">
        Workflow executions are managed by a
        <a href="https://slurm.schedmd.com/overview.html">Slurm</a> cluster with
        dynamic resource provisioning. This ensures efficient resource usage by
        dynamically spinning up temporary virtual machines (VMs) with optional
        GPU support in response to increased system load. The Slurm cluster,
        Ceph platform, and CloWM itself are hosted on the
        <a href="https://cloud.denbi.de">de.NBI cloud</a> infrastructure,
        providing a powerful and scalable environment for the execution of
        data-intense and computational demanding workflows.
      </div>
    </div>
    <div class="col-md text-center order-md-first">
      <img
        src="/src/assets/images/server.jpg"
        class="img-fluid rounded"
        style="max-height: 300px"
        alt="server photo"
      />
    </div>
  </div>

  <div class="row mt-4 pt-4 align-items-center gap-2">
    <div class="col-md">
      <h2>User-friendly Interface + REST API</h2>
      <div class="fs-5 text-secondary">
        Comprehensive workflow documentation is readily available and even
        complex parameterized workflows can be directly executed through an
        intuitive, automatically generated, graphical interface. CloWM's
        user-friendly interface makes it trivial to manage your workflows. The
        intuitive bucket browser allows the easy navigation and management of
        objects within a user's personalized S3 storage, including the setting
        of bucket permissions. For programmatic access and integration with
        other systems, CloWM provides a powerful REST API.
      </div>
    </div>
    <div class="col-md text-center position-relative">
      <img
        src="/src/assets/images/screenshot-buckets.png"
        class="img-fluid rounded"
        style="max-height: 400px"
        alt="screenshot of CloWM"
      />
    </div>
  </div>
  <div class="row my-4 align-items-center gap-2">
    <div class="col-md">
      <h2>Secure and personalized access</h2>
      <div class="fs-5 text-secondary order-md-last">
        CloWM employs
        <a href="https://lifescience-ri.eu/ls-login/">LifeScience AAI</a> for
        single-sign on and secure authentication. This ensures that only
        authorized users can access the platform.
        <a href="#signup-button">New users</a> are automatically registered
        within the system upon their initial login, streamlining the onboarding
        process. Authorization is based on Role-based Access Control (RBAC),
        granting users access to specific resources and functionalities based on
        their assigned roles. The CloWM REST API is secured by personal access
        tokens.
      </div>
    </div>
    <div class="col-md text-center order-md-first">
      <img
        src="/src/assets/images/security.jpg"
        class="img-fluid rounded"
        style="max-height: 300px"
        alt="security photo"
      />
    </div>
  </div>
  <div class="row mt-4 pt-4 align-items-center gap-2">
    <div class="col-md">
      <h2>Benefits for workflow developers</h2>
      <div class="fs-5 text-secondary">
        CloWM greatly broadens the accessibility of your workflows and connects
        them with a powerful compute and storage infrastructure. Moreover, it
        allows the automatic collection of key performance indicators for
        reporting purposes. A separate platform for workflow development and
        testing is available on invitation.
      </div>
    </div>
    <div class="col-md text-center position-relative">
      <img
        src="/src/assets/images/developer.jpg"
        class="img-fluid rounded"
        style="max-height: 300px"
        alt="man in front of a laptop"
      />
    </div>
  </div>
  <div class="row my-4 align-items-center gap-2">
    <div class="col-md">
      <h2>Available workflows</h2>
      <div class="fs-5 text-secondary order-md-last">
        The CloWM platform already contains several curated, best-practice
        workflows that cover a broad range of research fields/tasks, such as
        metagenomics (WGS and 16S), human-variation-analysis, infectious
        diseases (SARS-CoV2, Mpox, Influenza A+B), meta-barcoding, genome
        assembly, transcriptomics, basecalling, phylogenomics, … <br />
        and many more hopefully coming soon! Some of these workflows are
        exclusively available on CloWM.
      </div>
    </div>
    <div class="col-md text-center order-md-first">
      <img
        src="/src/assets/images/screenshot-workflows.png"
        class="img-fluid rounded"
        style="max-height: 400px"
        alt="code photo"
      />
    </div>
  </div>

  <h3 id="available-workflows" class="mt-5">
    The following {{ publicWorkflows.length }} workflows are currently available
    on this CloWM instance:
  </h3>
  <div
    class="d-flex flex-wrap gap-3 align-items-center card-container overflow-y-auto p-1 border rounded justify-content-between"
  >
    <bootstrap-card
      v-for="workflow in publicWorkflows"
      :key="workflow.workflow_id"
      class="workflow-card"
    >
      <template #title>
        <div class="d-flex align-items-center justify-content-between">
          <router-link
            class="fs-4"
            :to="{
              name: 'workflow-version',
              params: {
                workflowId: workflow.workflow_id,
                versionId: workflow.latest_git_commit_hash,
              },
            }"
            >{{ workflow.name }}@{{ workflow.latest_version_tag }}
          </router-link>
          <img
            v-if="workflow.icon_url"
            class="float-end icon"
            :src="workflow.icon_url"
            :alt="`Icon for workflow ${workflow.name}`"
          />
        </div>
      </template>
      <template #body>
        <div>{{ workflow.short_description }}</div>
        <div>
          <a :href="repositoryCommitUrl(workflow)">{{
            workflow.repository_url
          }}</a>
        </div>
      </template>
      <template #footer>
        <div class="d-flex justify-content-between row-gap-2 mt-2">
          <div>
            Last Update:
            {{ dayjs.unix(workflow.latest_version_timestamp).fromNow() }}
          </div>

          <div class="w-fit">by {{ workflow.developer }}</div>
        </div>
      </template>
    </bootstrap-card>
  </div>
  <div id="signup-button" class="text-center mt-5">
    <h2>Register now and unlock the full potential of your workflows!</h2>

    <div class="d-grid col-md-6 mx-auto my-4">
      <router-link
        class="btn btn-info btn-lg fs-2"
        :to="{ name: 'login' }"
        role="button"
      >
        Signup
      </router-link>
    </div>
    <div class="fs-3">
      Get in touch - It will give you a smile
      <img
        src="/src/assets/images/clowm.svg"
        alt="CloWM Logo"
        style="max-height: 2rem"
      />
    </div>
  </div>
  <h3 class="my-3">Funding and Support</h3>
  <div
    class="d-flex flex-row justify-content-evenly align-items-center gap-2 flex-wrap w-100 mb-md-5"
  >
    <div class="border rounded p-4 institute text-center text-bg-light">
      <h4 class="mb-4">A Service By</h4>
      <a href="https://nfdi4microbiota.de/">
        <img
          src="/src/assets/images/nfdi.svg"
          alt="NFDI4Microbiota Logo"
          height="50"
        />
      </a>
    </div>
    <div class="border rounded p-4 institute text-center text-bg-light">
      <h4 class="mb-4">Powered By</h4>
      <a href="https://www.denbi.de/">
        <img src="/src/assets/images/denbi.svg" alt="de.NBI Logo" height="50" />
      </a>
    </div>
    <div class="border rounded p-4 institute text-center text-bg-light">
      <h4 class="mb-4">Hosted By</h4>
      <a href="https://bibi.uni-bielefeld.de/">
        <img src="/src/assets/images/bibi.png" alt="BiBi Logo" height="50" />
      </a>
    </div>
    <div class="border rounded p-4 institute text-center text-bg-light">
      <h4 class="mb-4">Funded By</h4>
      <img src="/src/assets/images/dfg.png" alt="DFG Logo" height="50" />
    </div>
    <div class="border rounded p-4 institute text-center text-bg-light">
      <img
        src="/src/assets/images/unibi.svg"
        alt="Bielefeld University Logo"
        height="50"
      />
    </div>
  </div>
</template>

<style scoped>
@media (min-width: 900px) {
  .workflow-card {
    max-width: 32%;
  }
}

a > img:hover {
  filter: brightness(0.9);
}

@media (max-width: 900px) {
  .workflow-card {
    max-width: 48%;
  }
}

@media (max-width: 650px) {
  .workflow-card {
    max-width: 100%;
  }
}

@media (max-height: 900px) {
  .card-container {
    max-height: 50vh;
  }
}

@media (min-height: 901px) {
  .card-container {
    max-height: 70vh;
  }
}

.icon {
  max-height: 48px;
  max-width: 48px;
}

.institute {
  transition: transform 0.3s ease-out;
}

.institute:hover {
  transform: translate(0, -5px);
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
}
</style>
