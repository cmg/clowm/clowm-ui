import type { RouteRecordRaw } from "vue-router";

export const adminRoutes: RouteRecordRaw[] = [
  {
    path: "/admin/resources",
    name: "admin-resources",
    component: () => import("../views/admin/AdminResourcesView.vue"),
    meta: {
      requiresAdminRole: true,
      title: "Manage Resource",
    },
  },
  {
    path: "/admin/users",
    name: "admin-users",
    component: () => import("../views/admin/AdminUsersView.vue"),
    meta: {
      requiresAdminRole: true,
      title: "Manage Users",
    },
  },
  {
    path: "/admin/buckets",
    name: "admin-buckets",
    component: () => import("../views/admin/AdminBucketsView.vue"),
    meta: {
      requiresAdminRole: true,
      title: "Manage Buckets",
    },
  },
  {
    path: "/admin/tokens",
    name: "admin-tokens",
    component: () => import("../views/admin/AdminApiTokensView.vue"),
    meta: {
      requiresAdminRole: true,
      title: "Manage Api tokens",
    },
  },
  {
    path: "/admin/executions",
    name: "admin-executions",
    component: () => import("../views/admin/AdminWorkflowExecutionView.vue"),
    meta: {
      requiresAdminRole: true,
      title: "Manage Executions",
    },
  },
  {
    path: "/admin/sync-requests",
    name: "admin-sync-requests",
    component: () => import("../views/admin/AdminSyncRequestsView.vue"),
    meta: {
      requiresAdminRole: true,
      title: "Sync Requests",
    },
  },
];
