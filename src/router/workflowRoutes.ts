import type { RouteRecordRaw } from "vue-router";

export const workflowRoutes: RouteRecordRaw[] = [
  {
    path: "/workflow-executions",
    name: "workflow-executions",
    meta: {
      title: "My Workflow Executions",
    },
    component: () =>
      import("../views/workflows/ListWorkflowExecutionsView.vue"),
  },
  {
    path: "/workflows",
    name: "workflows",
    meta: {
      title: "Workflows",
    },
    component: () => import("../views/workflows/ListWorkflowsView.vue"),
  },
  {
    path: "/developer/workflows",
    name: "workflows-developer",
    component: () => import("../views/workflows/MyWorkflowsView.vue"),
    meta: {
      requiresDeveloperRole: true,
      title: "My Workflows",
    },
  },
  {
    path: "/developer/workflows/clowminfo",
    name: "workflows-clowminfo",
    component: () => import("../views/workflows/CreateClowmInfoView.vue"),
    props: (route) => ({
      workflowVersionId: route.query.workflow_version_id ?? undefined,
      workflowId: route.query.workflow_id ?? undefined,
    }),
    meta: {
      requiresDeveloperRole: true,
      title: "clowm_info.json",
    },
  },
  {
    path: "/reviewer/workflows",
    name: "workflows-reviewer",
    component: () => import("../views/workflows/ReviewWorkflowsView.vue"),
    meta: {
      requiresReviewerRole: true,
      title: "Review Workflows",
    },
  },
  {
    path: "/workflows/arbitrary",
    name: "arbitrary-workflow",
    component: () => import("../views/workflows/ArbitraryWorkflowView.vue"),
    meta: {
      requiresDeveloperRole: true,
      title: "Arbitrary Workflow",
    },
    props: (route) => ({
      wid: route.query.wid,
      viewMode: route.query.viewMode ?? undefined,
    }),
  },
  {
    path: "/workflows/:workflowId",
    name: "workflow",
    component: () => import("../views/workflows/WorkflowView.vue"),
    props: (route) => ({
      versionId: route.params.versionId ?? undefined,
      workflowId: route.params.workflowId,
      workflowModeId: route.query.workflowModeId ?? undefined,
      developerView: route.query.developerView == "true",
    }),
    children: [
      {
        path: "version/:versionId",
        name: "workflow-version",
        component: () => import("../views/workflows/WorkflowVersionView.vue"),
        props: (route) => ({
          versionId: route.params.versionId,
          workflowId: route.params.workflowId,
          workflowModeId: route.query.workflowModeId ?? undefined,
        }),
      },
      {
        path: "version/:versionId/start",
        name: "workflow-start",
        component: () => import("../views/workflows/StartWorkflowView.vue"),
        props: (route) => ({
          versionId: route.params.versionId,
          workflowId: route.params.workflowId,
          workflowModeId: route.query.workflowModeId ?? undefined,
        }),
      },
    ],
  },
  {
    path: "/workflows/:workflowId/version/:versionId/parameters",
    name: "workflow-parameter-translation",
    component: () =>
      import("../views/workflows/CreateParameterTranslationView.vue"),
    props: (route) => ({
      versionId: route.params.versionId,
      workflowId: route.params.workflowId,
    }),
  },
  {
    path: "/workflows/:workflowId/version/:versionId/metadata",
    name: "workflow-metadata",
    component: () =>
      import("../views/workflows/UpdateWorkflowVersionMetadata.vue"),
    props: (route) => ({
      workflowVersionId: route.params.versionId,
      workflowId: route.params.workflowId,
    }),
    meta: {
      requiresDeveloperRole: true,
      title: "Metadata",
    },
  },
  {
    path: "/workflows/:workflowId/version/:versionId/param-visibility",
    name: "workflow-parameter-visibility",
    component: () =>
      import("../views/workflows/EditParameterVisibilityView.vue"),
    props: (route) => ({
      versionId: route.params.versionId,
      workflowId: route.params.workflowId,
      modeId: route.query.modeId ?? undefined,
    }),
    meta: {
      requiresDeveloperRole: true,
      title: "Edit Parameter Visibility",
    },
  },
];
