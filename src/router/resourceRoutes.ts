import type { RouteRecordRaw } from "vue-router";

export const resourceRoutes: RouteRecordRaw[] = [
  {
    path: "/resources",
    name: "resources",
    component: () => import("../views/resources/ListResourcesView.vue"),
    meta: {
      title: "Resource",
    },
  },
  {
    path: "/maintainer/resources",
    name: "resource-maintainer",
    component: () => import("../views/resources/MyResourcesView.vue"),
    meta: {
      requiresMaintainerRole: true,
      title: "My Resources",
    },
  },
  {
    path: "/reviewer/resources",
    name: "resource-review",
    component: () => import("../views/resources/ReviewResourceView.vue"),
    meta: {
      requiresReviewerRole: true,
      title: "Review Resources",
    },
  },
];
