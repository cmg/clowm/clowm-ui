import { createRouter, createWebHistory } from "vue-router";
import LoginView from "../views/LoginView.vue";
import { workflowRoutes } from "@/router/workflowRoutes";
import { s3Routes } from "@/router/s3Routes";
import { resourceRoutes } from "@/router/resourceRoutes";
import { adminRoutes } from "@/router/adminRoutes";
import { userRoutes } from "@/router/userRoutes";
import { newsRoutes } from "@/router/newsRoutes.ts";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    ...workflowRoutes,
    ...s3Routes,
    ...resourceRoutes,
    ...adminRoutes,
    ...userRoutes,
    ...newsRoutes,
    {
      path: "/dashboard",
      name: "dashboard",
      component: () => import("../views/DashboardView.vue"),
      meta: {
        title: "Dashboard",
      },
    },
    {
      path: "/login",
      name: "login",
      component: LoginView,
      meta: {
        title: "Login",
        public: true,
      },
      props: (route) => ({
        returnPath: route.query.next ?? undefined,
        loginError: route.query.login_error ?? undefined,
        invitationToken: route.query.invitation_token ?? undefined,
      }),
    },
    {
      path: "/signup",
      name: "signup",
      component: () => import("../views/SignupView.vue"),
      meta: {
        title: "Signup",
        public: true,
      },
      props: (route) => ({
        returnPath: route.query.next ?? undefined,
      }),
    },
    {
      path: "/",
      name: "home",
      component: () => import("../views/HomeView.vue"),
      meta: {
        public: true,
      },
    },
    {
      path: "/privacy",
      name: "privacy",
      meta: {
        title: "Privacy Policy",
        public: true,
      },
      component: () => import("../views/PrivacyPolicyView.vue"),
    },
    {
      path: "/terms",
      name: "terms",
      meta: {
        title: "Terms of Usage",
        public: true,
      },
      component: () => import("../views/TermsOfUsageView.vue"),
    },
    {
      path: "/imprint",
      name: "imprint",
      meta: {
        title: "Imprint",
        public: true,
      },
      component: () => import("../views/ImprintView.vue"),
    },
    {
      path: "/:pathMatch(.*)",
      meta: {
        title: "Error",
        public: true,
      },
      component: () => import("../views/NotFoundView.vue"),
    },
  ],
});

router.afterEach((to) => {
  if (to.meta?.title) {
    document.title = to.meta.title + " - CloWM";
  }
});

export default router;
