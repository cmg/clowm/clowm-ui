import type { RouteRecordRaw } from "vue-router";

export const newsRoutes: RouteRecordRaw[] = [
  {
    path: "/news/create",
    name: "create-news-event",
    component: () => import("../views/news/CreateNewsEvent.vue"),
  },
  {
    path: "/news/:newsId",
    name: "news-event",
    component: () => import("../views/news/NewsEventView.vue"),
    props: (route) => ({
      newsId: route.params.newsId,
    }),
  },
];
