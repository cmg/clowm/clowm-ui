import type { RouteRecordRaw } from "vue-router";

export const userRoutes: RouteRecordRaw[] = [
  {
    path: "/api-tokens",
    name: "api-tokens",
    component: () => import("../views/user/ListApiTokenView.vue"),
    meta: {
      title: "API Tokens",
    },
  },
  {
    path: "/profile",
    name: "profile",
    component: () => import("../views/user/ProfileView.vue"),
    meta: {
      title: "Profile",
    },
    props: (route) => ({
      linkSuccess: route.query["link_success"],
      linkError: route.query["link_error"],
    }),
  },
];
