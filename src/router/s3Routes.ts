import type { RouteRecordRaw } from "vue-router";

export const s3Routes: RouteRecordRaw[] = [
  {
    path: "/object-storage/buckets",
    name: "buckets",
    component: () => import("../views/object-storage/BucketsView.vue"),
    props: (route) => ({
      bucketName: route.params.bucketName ?? undefined,
    }),
    meta: {
      title: "Buckets",
    },
    children: [
      {
        path: ":bucketName/:subFolders*",
        name: "bucket",
        component: () => import("../views/object-storage/BucketView.vue"),
        props: true,
      },
    ],
  },
  {
    path: "/object-storage/s3-keys",
    name: "s3_keys",
    meta: {
      title: "S3 Keys",
    },
    component: () => import("../views/object-storage/S3KeysView.vue"),
  },
  {
    path: "/object-storage/multipart-uploads",
    name: "s3_multipart-uploads",
    meta: {
      title: "Multipart Uploads",
    },
    component: () => import("../views/object-storage/MultiPartUploadsView.vue"),
  },
];
