import { defineStore } from "pinia";

export type PossibleThemes = "light" | "dark";

export const useSettingsStore = defineStore("settings", {
  state: () =>
    ({
      theme: "light",
    }) as {
      theme: PossibleThemes;
    },
  getters: {
    lightThemeActive(): boolean {
      return this.theme === "light";
    },
    darkThemeActive(): boolean {
      return this.theme === "dark";
    },
  },
  actions: {
    setTheme(theme: PossibleThemes) {
      if (this.theme !== theme) {
        this.theme = theme;
        document.querySelector("html")?.setAttribute("data-bs-theme", theme);
        localStorage.setItem("theme", theme);
      }
    },
  },
});
