import { defineStore } from "pinia";
import type {
  UserOutExtended,
  UserOut,
  UserIn,
  ApiTokenOut,
  ApiTokenIn,
  ApiTokenPrivateOut,
} from "@/client/types.gen";
import { RoleEnum } from "@/client/types.gen";
import { UserService, ApiTokenService } from "@/client/sdk.gen";
import { useWorkflowExecutionStore } from "@/stores/workflowExecutions";
import { useBucketStore } from "@/stores/buckets";
import { useWorkflowStore } from "@/stores/workflows";
import { useS3KeyStore } from "@/stores/s3keys";
import { useS3ObjectStore } from "@/stores/s3objects";
import { clear as dbclear } from "idb-keyval";
import { useNameStore } from "@/stores/names";
import type { IdMapping } from "@/types/Store.ts";

type DecodedToken = {
  exp: number;
  iss: string;
  sub: string;
};

function parseJwt(token: string): DecodedToken {
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join(""),
  );

  return JSON.parse(jsonPayload) as DecodedToken;
}

export const useUserStore = defineStore("user", {
  state: () =>
    ({
      user: null,
      jwt: null,
      apiTokensMapping: {},
    }) as {
      user: UserOutExtended | null;
      jwt: DecodedToken | null;
      apiTokensMapping: IdMapping<ApiTokenOut>;
    },
  getters: {
    roles(): string[] {
      return (
        this.user?.roles?.map((role) => role.toString()) ??
        JSON.parse(localStorage.getItem("roles") ?? "[]")
      );
    },
    apiTokens(): ApiTokenOut[] {
      return Object.values(this.apiTokensMapping);
    },
    authenticated(): boolean {
      return this.jwt != null || this.user != null;
    },
    currentUID(): string {
      return this.user?.uid ?? this.jwt?.sub ?? "";
    },
    foreignUser(): boolean {
      return this.roles.length === 0;
    },
    normalUser(): boolean {
      return this.roles.includes(RoleEnum.USER);
    },
    reviewer(): boolean {
      return this.roles.includes(RoleEnum.REVIEWER);
    },
    workflowDev(): boolean {
      return this.roles.includes(RoleEnum.DEVELOPER);
    },
    resourceMaintainer(): boolean {
      return this.roles.includes(RoleEnum.DB_MAINTAINER);
    },
    admin(): boolean {
      return this.roles.includes(RoleEnum.ADMINISTRATOR);
    },
  },
  actions: {
    getCurrentUser(): Promise<UserOutExtended> {
      return UserService.userGetLoggedInUser({})
        .then((response) => response.data!)
        .then((user) => {
          this.updateUser(user);
          return user;
        });
    },
    updateJWT(jwt: string | null) {
      this.jwt = jwt != null ? parseJwt(jwt) : null;
    },
    updateUser(user: UserOutExtended) {
      this.user = user;
      localStorage.setItem("currentUser", user.uid);
      localStorage.setItem("roles", JSON.stringify(user.roles));
      useNameStore().addNameToMapping(user.uid, user.display_name);
    },
    logout() {
      window._paq?.push(["resetUserId"]);
      this.$reset();
      const activeTheme = localStorage.getItem("theme");
      localStorage.clear();
      if (activeTheme != undefined) {
        localStorage.setItem("theme", activeTheme);
      }
      dbclear();
      useWorkflowExecutionStore().$reset();
      useBucketStore().$reset();
      useWorkflowStore().$reset();
      useS3KeyStore().$reset();
      useS3ObjectStore().$reset();
    },
    fetchUsers(
      searchString?: string,
      filterRoles?: RoleEnum[],
    ): Promise<UserOutExtended[]> {
      return UserService.userListUsers({
        query: {
          name_substring: searchString,
          filter_roles: filterRoles,
        },
      })
        .then((response) => response.data!)
        .then((users) => {
          const nameStore = useNameStore();
          for (const user of users) {
            nameStore.addNameToMapping(user.uid, user.display_name);
          }
          return users;
        });
    },
    fetchApiTokens(uid?: string): Promise<ApiTokenOut[]> {
      return ApiTokenService.apiTokenListToken({
        query: { uid: uid },
      }).then((response) => response.data!);
    },

    fetchMyApiTokens(onFinally?: () => void) {
      if (this.apiTokens.length > 0) {
        onFinally?.();
      }
      this.fetchApiTokens(useUserStore().currentUID)
        .then((tokens) => {
          for (const token of tokens) {
            this.apiTokensMapping[token.token_id] = token;
          }
          return tokens;
        })
        .finally(onFinally);
    },
    createApiToken(tokenIn: ApiTokenIn): Promise<ApiTokenPrivateOut> {
      return ApiTokenService.apiTokenCreateToken({
        body: tokenIn,
      })
        .then((response) => response.data!)
        .then((token) => {
          this.apiTokensMapping[token.token_id] = token as ApiTokenOut;
          return token;
        });
    },
    deleteApiToken(tokenId: string): Promise<void> {
      return ApiTokenService.apiTokenDeleteToken({
        path: { tid: tokenId },
      })
        .then((response) => response.data!)
        .then(() => {
          delete this.apiTokensMapping[tokenId];
        });
    },
    updateUserRoles(uid: string, roles: RoleEnum[]): Promise<UserOutExtended> {
      return UserService.userUpdateRoles({
        path: { uid: uid },
        body: {
          roles: roles,
        },
      }).then((response) => response.data!);
    },
    inviteUser(userIn: UserIn): Promise<UserOutExtended> {
      return UserService.userCreateUser({ body: userIn })
        .then((response) => response.data!)
        .then((user) => {
          useNameStore().addNameToMapping(user.uid, user.display_name);
          return user;
        });
    },
    resendInvitationEmail(uid: string): Promise<UserOutExtended> {
      return UserService.userResendInvitation({
        path: { uid: uid },
      })
        .then((response) => response.data!)
        .then((user) => {
          useNameStore().addNameToMapping(user.uid, user.display_name);
          return user;
        });
    },
    searchUser(searchString: string): Promise<UserOut[]> {
      return UserService.userSearchUsers({
        query: { name_substring: searchString },
      })
        .then((response) => response.data!)
        .then((users) => {
          const nameStore = useNameStore();
          for (const user of users) {
            nameStore.addNameToMapping(user.uid, user.display_name);
          }
          return users;
        });
    },
    async fetchUsernames(
      uids: (string | null | undefined)[],
    ): Promise<string[]> {
      const nameStore = useNameStore();
      const filteredIds = uids
        .map((uid) => uid ?? "")
        .filter((uid) => uid.length > 0)
        .filter((uid) => !nameStore.getName(uid)) // filter already present UIDs
        .filter(
          // filter unique UIDs
          (modeId, index, array) =>
            array.findIndex((val) => val === modeId) === index,
        );
      // If all uids are already in the store, then return them
      if (filteredIds.length === 0) {
        return uids.map((uid) => nameStore.getName(uid)!);
      }
      const missingIds: string[] = [];
      const storedNames = filteredIds.map((uid) => localStorage.getItem(uid));

      for (const index in storedNames) {
        // if uid was not cached, mark it to fetch it from backend
        if (storedNames[index] == null) {
          missingIds.push(filteredIds[index]);
        } else {
          nameStore.addNameToMapping(filteredIds[index], storedNames[index]!);
        }
      }
      // fetch missing users from backend
      const fetchedUsers = await Promise.all(
        missingIds.map((uid) =>
          UserService.userGetUser({ path: { uid: uid } }).then(
            (response) => response.data!,
          ),
        ),
      );
      // Put users in store
      for (const user of fetchedUsers) {
        nameStore.addNameToMapping(user.uid, user.display_name);
      }

      return uids.map((uid) => nameStore.getName(uid)!);
    },
    deleteUser(uid: string): Promise<void> {
      // return Promise.resolve();
      return UserService.userDeleteUser({
        path: { uid: uid },
      }).then((response) => response.data!);
    },
  },
});
