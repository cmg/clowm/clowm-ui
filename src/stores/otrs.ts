import { defineStore } from "pinia";
import type {
  OwnershipTransferRequestIn,
  OwnershipTransferRequestOut,
} from "@/client/types.gen";
import { OwnershipTypeEnum } from "@/client/types.gen";
import {
  BucketService,
  ResourceService,
  WorkflowService,
} from "@/client/sdk.gen";
import { useUserStore } from "@/stores/users";
import { useBucketStore } from "@/stores/buckets";
import { useWorkflowStore } from "@/stores/workflows";
import { useResourceStore } from "@/stores/resources";
import type { IdMapping } from "@/types/Store.ts";

export const useOTRStore = defineStore("otrs", {
  state: () =>
    ({
      otrMapping: {},
      __otrLoaded: {
        bucket: false,
        resource: false,
        workflow: false,
      },
    }) as {
      otrMapping: IdMapping<OwnershipTransferRequestOut>;
      __otrLoaded: Record<OwnershipTypeEnum, boolean>;
    },
  getters: {
    bucketOtrs(): OwnershipTransferRequestOut[] {
      const currentUID = useUserStore().currentUID;
      return Object.values(this.otrMapping).filter(
        (otr) =>
          otr.target_type === OwnershipTypeEnum.BUCKET &&
          (otr.current_owner_uid === currentUID ||
            otr.new_owner_uid === currentUID),
      );
    },
    workflowOtrs(): OwnershipTransferRequestOut[] {
      const currentUID = useUserStore().currentUID;
      return Object.values(this.otrMapping).filter(
        (otr) =>
          otr.target_type === OwnershipTypeEnum.WORKFLOW &&
          (otr.current_owner_uid === currentUID ||
            otr.new_owner_uid === currentUID),
      );
    },
    resourceOtrs(): OwnershipTransferRequestOut[] {
      const currentUID = useUserStore().currentUID;
      return Object.values(this.otrMapping).filter(
        (otr) =>
          otr.target_type === OwnershipTypeEnum.RESOURCE &&
          (otr.current_owner_uid === currentUID ||
            otr.new_owner_uid === currentUID),
      );
    },
  },
  actions: {
    __filterOTRMapping(type: OwnershipTypeEnum) {
      for (const otr_id of Object.keys(this.otrMapping)) {
        if (this.otrMapping[otr_id].target_type === type) {
          delete this.otrMapping[otr_id];
        }
      }
    },
    fetchOtr(
      targetType: OwnershipTypeEnum,
      otr_id: string,
    ): Promise<OwnershipTransferRequestOut> {
      let promise: Promise<OwnershipTransferRequestOut>;
      switch (targetType) {
        case OwnershipTypeEnum.BUCKET:
          promise = BucketService.bucketGetBucketOtr({
            path: {
              bucket_name: otr_id,
            },
          }).then((response) => response.data!);
          break;
        case OwnershipTypeEnum.WORKFLOW:
          promise = WorkflowService.workflowGetWorkflowOtr({
            path: {
              wid: otr_id,
            },
          }).then((response) => response.data!);
          break;
        case OwnershipTypeEnum.RESOURCE:
          promise = ResourceService.resourceGetResourceOtr({
            path: {
              rid: otr_id,
            },
          }).then((response) => response.data!);
          break;
      }
      promise.then((otr) => {
        useUserStore().fetchUsernames([
          otr.new_owner_uid,
          otr.current_owner_uid,
        ]);
        return otr;
      });
      return promise;
    },
    fetchOtrs(
      targetType: OwnershipTypeEnum,
      currentOwnerId?: string,
      newOwnerId?: string,
    ): Promise<OwnershipTransferRequestOut[]> {
      const options = {
        query: {
          current_owner_id: currentOwnerId,
          new_owner_id: newOwnerId,
        },
      };
      switch (targetType) {
        case OwnershipTypeEnum.BUCKET:
          return BucketService.bucketListBucketOtrs(options).then(
            (response) => response.data!,
          );
        case OwnershipTypeEnum.WORKFLOW:
          return WorkflowService.workflowListWorkflowOtrs(options).then(
            (response) => response.data!,
          );
        case OwnershipTypeEnum.RESOURCE:
          return ResourceService.resourceListResourceOtrs(options).then(
            (response) => response.data!,
          );
      }
    },
    fetchOwnOtrs(
      targetType: OwnershipTypeEnum,
      onFinally?: () => void,
    ): Promise<OwnershipTransferRequestOut[]> {
      if (this.__otrLoaded[targetType]) {
        onFinally?.();
      }
      const userRepo = useUserStore();
      return Promise.all([
        this.fetchOtrs(targetType, userRepo.currentUID),
        this.fetchOtrs(targetType, undefined, userRepo.currentUID),
      ])
        .then((otrs) => otrs.flat())
        .then((otrs) => {
          this.__filterOTRMapping(targetType);
          for (const otr of otrs) {
            this.otrMapping[otr.target_id] = otr;
          }
          userRepo.fetchUsernames(
            otrs
              .map((otr) => [otr.new_owner_uid, otr.current_owner_uid])
              .flat(),
          );
          return otrs;
        })
        .finally(() => {
          this.__otrLoaded[targetType] = true;
          onFinally?.();
        });
    },
    acceptOtr(otr: OwnershipTransferRequestOut): Promise<string> {
      const promise = (function () {
        switch (otr.target_type) {
          case OwnershipTypeEnum.BUCKET:
            return useBucketStore()
              .acceptBucketOtr(otr)
              .then((bucket) => {
                return bucket.name;
              });
          case OwnershipTypeEnum.WORKFLOW:
            return useWorkflowStore()
              .acceptWorkflowOtr(otr)
              .then((workflow) => {
                return workflow.workflow_id;
              });
          case OwnershipTypeEnum.RESOURCE:
            return useResourceStore()
              .acceptResourceOtr(otr)
              .then((resource) => {
                return resource.resource_id;
              });
        }
      })();
      return promise.then((target_id) => {
        delete this.otrMapping[otr.target_id];
        return target_id;
      });
    },
    deleteOtr(otr: OwnershipTransferRequestOut): Promise<void> {
      const promise = (function () {
        switch (otr.target_type) {
          case OwnershipTypeEnum.BUCKET:
            return BucketService.bucketDeleteBucketOtr({
              path: { bucket_name: otr.target_id },
            });
          case OwnershipTypeEnum.WORKFLOW:
            return WorkflowService.workflowDeleteWorkflowOtr({
              path: { wid: otr.target_id },
            });
          case OwnershipTypeEnum.RESOURCE:
            return ResourceService.resourceDeleteResourceOtr({
              path: { rid: otr.target_id },
            });
        }
      })();
      return promise
        .then((response) => response.data!)
        .then((target_id) => {
          delete this.otrMapping[otr.target_id];
          return target_id;
        });
    },
    createOtr(
      targetId: string,
      otr: OwnershipTransferRequestIn,
      targetType: OwnershipTypeEnum,
    ): Promise<OwnershipTransferRequestOut> {
      const promise = (function () {
        switch (targetType) {
          case OwnershipTypeEnum.BUCKET:
            return BucketService.bucketCreateBucketOtr({
              path: { bucket_name: targetId },
              body: otr,
            });
          case OwnershipTypeEnum.WORKFLOW:
            return WorkflowService.workflowCreateWorkflowOtr({
              path: { wid: targetId },
              body: otr,
            });
          case OwnershipTypeEnum.RESOURCE:
            return ResourceService.resourceCreateResourceOtr({
              path: { rid: targetId },
              body: otr,
            });
        }
      })();
      return promise
        .then((response) => response.data!)
        .then((newOtr) => {
          this.otrMapping[newOtr.target_id] = newOtr;
          return newOtr;
        });
    },
  },
});
