import { defineStore } from "pinia";
import type {
  FileTree,
  OwnershipTransferRequestOut,
  ResourceIn,
  ResourceOut,
  ResourceVersionIn,
  ResourceVersionOut,
  UserRequestAnswer,
  UserSynchronizationRequestIn,
  UserSynchronizationRequestOut,
} from "@/client/types.gen";
import type { IdMapping } from "@/types/Store.ts";
import { ResourceVersionStatus } from "@/client/types.gen";
import { ResourceService, ResourceVersionService } from "@/client/sdk.gen";
import { useUserStore } from "@/stores/users";
import { useNameStore } from "@/stores/names";

function parseFileTree(parent: string, tree?: FileTree | null): string[] {
  if (tree == undefined || tree.contents == undefined) {
    return [];
  }
  let files: string[] = [];
  const currentName = parent.length > 0 ? parent + tree.name + "/" : "/";
  for (const dir of tree.contents) {
    files.push(currentName + dir.name);
    if (dir.type === "directory") {
      files = files.concat(parseFileTree(currentName, dir));
    }
  }
  return files;
}

export const useResourceStore = defineStore("resources", {
  state: () =>
    ({
      resourceMapping: {},
      ownResourceMapping: {},
      reviewableResourceMapping: {},
      syncRequestMapping: {},
      resourceTree: {},
      resourceTreeList: {},
      __syncRequestsFetched: false,
    }) as {
      resourceMapping: IdMapping<ResourceOut>;
      ownResourceMapping: IdMapping<ResourceOut>;
      reviewableResourceMapping: IdMapping<ResourceOut>;
      syncRequestMapping: IdMapping<UserSynchronizationRequestOut>;
      resourceTree: IdMapping<FileTree | null>;
      resourceTreeList: IdMapping<string[]>;
      __syncRequestsFetched: boolean;
    },
  getters: {
    resources(): ResourceOut[] {
      return Object.values(this.resourceMapping);
    },
    versionMapping(): IdMapping<ResourceVersionOut> {
      const mapping: IdMapping<ResourceVersionOut> = {};
      for (const resource of this.resources) {
        for (const version of resource.versions) {
          mapping[version.resource_version_id] = version;
        }
      }
      return mapping;
    },
    getLatestVersion(): (resource_id: string) => string {
      return (resource_id) => {
        for (const version of this.resourceMapping[resource_id]?.versions ??
          []) {
          if (version.status === ResourceVersionStatus.LATEST) {
            return version.resource_version_id;
          }
        }
        return "";
      };
    },
    ownResources(): ResourceOut[] {
      return Object.values(this.ownResourceMapping);
    },
    reviewableResources(): ResourceOut[] {
      return Object.values(this.reviewableResourceMapping);
    },
    syncRequests(): UserSynchronizationRequestOut[] {
      return Object.values(this.syncRequestMapping);
    },
  },
  actions: {
    fetchResource(
      resource_id: string,
      versionStatus?: ResourceVersionStatus[],
    ): Promise<ResourceOut> {
      return ResourceService.resourceGetResource({
        path: { rid: resource_id },
        query: { version_status: versionStatus },
      })
        .then((response) => response.data!)
        .then((resource) => {
          const nameStore = useNameStore();
          nameStore.addNameToMapping(resource.resource_id, resource.name);
          for (const version of resource.versions) {
            nameStore.addNameToMapping(
              version.resource_version_id,
              version.release,
            );
          }
          return resource;
        });
    },
    fetchSyncRequests(
      onFinally?: () => void,
    ): Promise<UserSynchronizationRequestOut[]> {
      if (this.__syncRequestsFetched) {
        onFinally?.();
      }
      return ResourceService.resourceListSyncRequests({})
        .then((response) => response.data!)
        .then((requests) => {
          this.__syncRequestsFetched = true;
          const userStore = useUserStore();
          userStore.fetchUsernames(
            requests.map((request) => request.requester_id),
          );
          const newMapping: IdMapping<UserSynchronizationRequestOut> = {};
          for (const request of requests) {
            newMapping[request.resource_version_id] = request;
          }
          this.syncRequestMapping = newMapping;
          return requests;
        })
        .finally(onFinally);
    },
    fetchResources(
      maintainerId?: string,
      versionStatus?: ResourceVersionStatus[],
      searchString?: string,
      _public?: boolean,
    ): Promise<ResourceOut[]> {
      return ResourceService.resourceListResources({
        query: {
          maintainer_id: maintainerId,
          version_status: versionStatus,
          name_substring: searchString,
          public: _public,
        },
      })
        .then((response) => response.data!)
        .then((resources) => {
          const userStore = useUserStore();
          userStore.fetchUsernames(
            resources.map((resource) => resource.maintainer_id),
          );
          const nameStore = useNameStore();
          for (const resource of resources) {
            nameStore.addNameToMapping(resource.resource_id, resource.name);
            for (const version of resource.versions) {
              nameStore.addNameToMapping(
                version.resource_version_id,
                version.release,
              );
            }
          }
          return resources;
        });
    },
    fetchResourceTree(
      resource_id: string,
      resource_version_id: string,
      onFinally?: () => void,
    ): Promise<FileTree> {
      if (this.resourceTree[resource_version_id] === null) {
        onFinally?.();
      }
      return ResourceVersionService.resourceVersionResourceFileTree({
        path: {
          rvid: resource_version_id,
          rid: resource_id,
        },
      })
        .then((response) => response.data!)
        .then((tree) => {
          this.resourceTree[resource_version_id] = tree[0];
          this.resourceTreeList[resource_version_id] = parseFileTree(
            "",
            tree[0],
          );
          return tree;
        })
        .catch((err) => {
          this.resourceTree[resource_version_id] = null;
          return err;
        })
        .finally(onFinally);
    },
    fetchReviewableResources(onFinally?: () => void): Promise<ResourceOut[]> {
      if (Object.keys(this.reviewableResourceMapping).length > 0) {
        onFinally?.();
      }
      return this.fetchResources(undefined, [
        ResourceVersionStatus.WAIT_FOR_REVIEW,
      ])
        .then((resources) => {
          const newMapping: IdMapping<ResourceOut> = {};
          for (const resource of resources) {
            newMapping[resource.resource_id] = resource;
          }
          this.reviewableResourceMapping = newMapping;
          return resources;
        })
        .finally(onFinally);
    },
    fetchPublicResources(onFinally?: () => void): Promise<ResourceOut[]> {
      if (this.resources.length > 0) {
        onFinally?.();
      }
      return this.fetchResources(undefined, [
        ResourceVersionStatus.APPROVED,
        ResourceVersionStatus.SYNC_REQUESTED,
        ResourceVersionStatus.SYNCHRONIZING,
        ResourceVersionStatus.SYNC_ERROR,
        ResourceVersionStatus.SYNCHRONIZED,
        ResourceVersionStatus.SETTING_LATEST,
        ResourceVersionStatus.LATEST,
        ResourceVersionStatus.CLUSTER_DELETE_ERROR,
      ])
        .then((resources) => {
          const newMapping: IdMapping<ResourceOut> = {};
          for (const resource of resources) {
            newMapping[resource.resource_id] = resource;
          }
          this.resourceMapping = newMapping;
          return resources;
        })
        .finally(onFinally);
    },
    fetchOwnResource(
      resource_id: string,
      onFinally?: () => void,
    ): Promise<ResourceOut> {
      if (this.ownResourceMapping[resource_id]) {
        onFinally?.();
      }
      return this.fetchResource(resource_id)
        .then((resource) => {
          this.ownResourceMapping[resource.resource_id] = resource;
          return resource;
        })
        .finally(onFinally);
    },
    fetchOwnResources(onFinally?: () => void): Promise<ResourceOut[]> {
      const authStore = useUserStore();
      if (this.ownResources.length > 0) {
        onFinally?.();
      }
      return this.fetchResources(authStore.currentUID)
        .then((resources) => {
          const newMapping: IdMapping<ResourceOut> = {};
          for (const resource of resources) {
            newMapping[resource.resource_id] = resource;
          }
          this.ownResourceMapping = newMapping;
          return resources;
        })
        .finally(onFinally);
    },
    async createResource(resource: ResourceIn): Promise<ResourceOut> {
      const createdResource = (
        await ResourceService.resourceCreateResource({
          body: resource,
        })
      ).data!;
      this.ownResourceMapping[createdResource.resource_id] = createdResource;
      const nameStore = useNameStore();
      nameStore.addNameToMapping(
        createdResource.resource_id,
        createdResource.name,
      );
      nameStore.addNameToMapping(
        createdResource.versions[0].resource_version_id,
        createdResource.versions[0].release,
      );

      return createdResource;
    },
    requestSynchronization(
      resourceVersion: ResourceVersionOut,
      request: UserSynchronizationRequestIn,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionRequestResourceVersionSync({
        path: {
          rid: resourceVersion.resource_id,
          rvid: resourceVersion.resource_version_id,
        },
        body: request,
      })
        .then((response) => response.data!)
        .then((changedResourceVersion) => {
          const versionIndex = this.resourceMapping[
            changedResourceVersion.resource_id
          ]?.versions?.findIndex(
            (version) =>
              version.resource_version_id ==
              changedResourceVersion.resource_version_id,
          );
          if (versionIndex != undefined && versionIndex > -1) {
            this.resourceMapping[changedResourceVersion.resource_id].versions[
              versionIndex
            ] = changedResourceVersion;
          }
          return changedResourceVersion;
        })
        .then((changedResourceVersion) => {
          const versionIndex = this.ownResourceMapping[
            changedResourceVersion.resource_id
          ]?.versions?.findIndex(
            (version) =>
              version.resource_version_id ==
              changedResourceVersion.resource_version_id,
          );
          if (versionIndex != undefined && versionIndex > -1) {
            this.ownResourceMapping[
              changedResourceVersion.resource_id
            ].versions[versionIndex] = changedResourceVersion;
          }
          return changedResourceVersion;
        });
    },
    requestReview(
      resourceVersion: ResourceVersionOut,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionRequestResourceVersionReview(
        {
          path: {
            rid: resourceVersion.resource_id,
            rvid: resourceVersion.resource_version_id,
          },
        },
      )
        .then((response) => response.data!)
        .then((changedResourceVersion) => {
          if (
            this.ownResourceMapping[changedResourceVersion.resource_id] ==
            undefined
          ) {
            this.fetchOwnResource(resourceVersion.resource_id);
            return changedResourceVersion;
          }
          const versionIndex = this.ownResourceMapping[
            changedResourceVersion.resource_id
          ].versions.findIndex(
            (version) =>
              version.resource_version_id ==
              changedResourceVersion.resource_version_id,
          );
          if (versionIndex > -1) {
            this.ownResourceMapping[
              changedResourceVersion.resource_id
            ].versions[versionIndex] = changedResourceVersion;
          } else {
            this.ownResourceMapping[
              changedResourceVersion.resource_id
            ].versions.push(changedResourceVersion);
          }
          return changedResourceVersion;
        });
    },
    updateResource(
      resource_id: string,
      version: ResourceVersionIn,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionRequestResourceVersion({
        path: {
          rid: resource_id,
        },
        body: version,
      })
        .then((response) => response.data!)
        .then((versionOut) => {
          if (this.ownResourceMapping[versionOut.resource_id] == undefined) {
            this.fetchOwnResource(versionOut.resource_id);
            return versionOut;
          }
          useNameStore().addNameToMapping(
            versionOut.resource_version_id,
            versionOut.release,
          );
          this.ownResourceMapping[versionOut.resource_id].versions.push(
            versionOut,
          );
          return versionOut;
        });
    },
    reviewResource(
      resourceVersion: ResourceVersionOut,
      requestAnswer: UserRequestAnswer,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionResourceVersionReview({
        path: {
          rid: resourceVersion.resource_id,
          rvid: resourceVersion.resource_version_id,
        },
        body: requestAnswer,
      })
        .then((response) => response.data!)
        .then(this._updateReviewableResourceVersion);
    },
    syncResource(
      resourceVersion: ResourceVersionOut,
      requestAnswer: UserRequestAnswer,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionResourceVersionSync({
        path: {
          rid: resourceVersion.resource_id,
          rvid: resourceVersion.resource_version_id,
        },
        body: requestAnswer,
      })
        .then((response) => response.data!)
        .then((version) => {
          delete this.syncRequestMapping[version.resource_version_id];
          return version;
        });
    },
    _updateReviewableResourceVersion(
      version: ResourceVersionOut,
    ): ResourceVersionOut {
      if (this.reviewableResourceMapping[version.resource_id] == undefined) {
        return version;
      }
      const versionIndex = this.reviewableResourceMapping[
        version.resource_id
      ].versions.findIndex(
        (version) => version.resource_version_id == version.resource_version_id,
      );
      if (versionIndex > -1) {
        this.reviewableResourceMapping[version.resource_id].versions[
          versionIndex
        ] = version;
      } else {
        this.reviewableResourceMapping[version.resource_id].versions.push(
          version,
        );
      }
      return version;
    },
    setLatestResource(
      resourceVersion: ResourceVersionOut,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionResourceVersionLatest({
        path: {
          rid: resourceVersion.resource_id,
          rvid: resourceVersion.resource_version_id,
        },
      }).then((response) => response.data!);
    },
    deleteOnCluster(
      resourceVersion: ResourceVersionOut,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionDeleteResourceVersionCluster(
        {
          path: {
            rid: resourceVersion.resource_id,
            rvid: resourceVersion.resource_version_id,
          },
        },
      ).then((response) => response.data!);
    },
    deleteInS3(
      resourceVersion: ResourceVersionOut,
    ): Promise<ResourceVersionOut> {
      return ResourceVersionService.resourceVersionDeleteResourceVersionS3({
        path: {
          rid: resourceVersion.resource_id,
          rvid: resourceVersion.resource_version_id,
        },
      }).then((response) => response.data!);
    },
    acceptResourceOtr(otr: OwnershipTransferRequestOut): Promise<ResourceOut> {
      return ResourceService.resourceAcceptResourceOtr({
        path: { rid: otr.target_id },
      })
        .then((response) => response.data!)
        .then((resource) => {
          this.fetchOwnResource(resource.resource_id);
          return resource;
        });
    },
    delete(resourceId: string): Promise<void> {
      return ResourceService.resourceDeleteResource({
        path: {
          rid: resourceId,
        },
      }).then(() => {
        delete this.resourceMapping[resourceId];
        delete this.ownResourceMapping[resourceId];
        delete this.reviewableResourceMapping[resourceId];
        delete this.syncRequestMapping[resourceId];
      });
    },
  },
});
