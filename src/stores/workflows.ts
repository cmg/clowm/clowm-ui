import { defineStore } from "pinia";
import type {
  BodyWorkflowVersionUploadWorkflowVersionIcon,
  IconUpdateOut,
  OwnershipTransferRequestOut,
  ParameterExtensionOutput,
  WorkflowCredentialsIn,
  WorkflowIn,
  WorkflowModeOut,
  WorkflowOut,
  WorkflowUpdate,
  WorkflowVersionOut,
  WorkflowVersionMetadataIn,
  WorkflowVersionMetadataOut,
  PublicWorkflowOut,
} from "@/client/types.gen";
import { DocumentationEnum, WorkflowVersionStatus } from "@/client/types.gen";
import {
  PublicService,
  WorkflowCredentialsService,
  WorkflowModeService,
  WorkflowService,
  WorkflowVersionService,
} from "@/client/sdk.gen";
import { useUserStore } from "@/stores/users";
import { get, set } from "idb-keyval";
import { useNameStore } from "@/stores/names";
import type { IdMapping } from "@/types/Store.ts";

export type DocLocations = Record<DocumentationEnum, string>;

interface WorkflowWithDocLocations {
  workflow: WorkflowIn;
  docs: DocLocations;
}

export const useWorkflowStore = defineStore("workflows", {
  state: () =>
    ({
      workflowMapping: {},
      comprehensiveWorkflowMapping: {},
      modeMapping: {},
      documentationFiles: {},
      publicWorkflowMapping: {},
    }) as {
      workflowMapping: IdMapping<WorkflowOut>;
      comprehensiveWorkflowMapping: IdMapping<WorkflowOut>;
      modeMapping: IdMapping<WorkflowModeOut>;
      publicWorkflowMapping: IdMapping<PublicWorkflowOut>;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      documentationFiles: IdMapping<Record<DocumentationEnum, any>>;
    },
  getters: {
    workflows(): WorkflowOut[] {
      return Object.values(this.workflowMapping);
    },
    ownWorkflows(): WorkflowOut[] {
      const authStore = useUserStore();
      return Object.values(this.comprehensiveWorkflowMapping).filter(
        (workflow) => workflow.developer_id === authStore.currentUID,
      );
    },
    reviewableWorkflows(): WorkflowOut[] {
      return Object.values(this.comprehensiveWorkflowMapping).filter(
        (workflow) =>
          workflow.versions.filter(
            (version) => version.status === WorkflowVersionStatus.CREATED,
          ).length > 0,
      );
    },
    versionMapping(): Record<string, WorkflowVersionOut> {
      const mapping: Record<string, WorkflowVersionOut> = {};
      for (const workflow of this.workflows) {
        for (const version of workflow.versions) {
          mapping[version.workflow_version_id] = version;
        }
      }
      for (const workflow of this.ownWorkflows) {
        for (const version of workflow.versions) {
          mapping[version.workflow_version_id] = version;
        }
      }
      return mapping;
    },
    getArbitraryWorkflow(): (
      wid: string,
    ) => Promise<WorkflowWithDocLocations | undefined> {
      return (wid: string) => get(wid);
    },
  },
  actions: {
    __addNameToMapping(key: string, value: string) {
      useNameStore().addNameToMapping(key, value);
    },
    fetchVersionMetadata(
      workflowId: string,
      versionId: string,
    ): Promise<WorkflowVersionMetadataOut> {
      return WorkflowVersionService.workflowVersionGetWorkflowVersionMetadata({
        path: {
          wid: workflowId,
          git_commit_hash: versionId,
        },
      }).then((response) => response.data!);
    },
    updateVersionMetadata(
      workflowId: string,
      versionId: string,
      metadata: WorkflowVersionMetadataIn,
    ): Promise<WorkflowVersionMetadataOut> {
      return WorkflowVersionService.workflowVersionUpdateWorkflowVersionMetadata(
        {
          path: {
            wid: workflowId,
            git_commit_hash: versionId,
          },
          body: metadata,
        },
      ).then((response) => response.data!);
    },
    fetchPublicWorkflows(onFinally?: () => void): Promise<PublicWorkflowOut[]> {
      if (Object.keys(this.publicWorkflowMapping).length > 0) {
        onFinally?.();
      }
      return PublicService.publicGetPublicWorkflows({})
        .then((response) => response.data!)
        .then((workflows) => {
          const tempMapping: IdMapping<PublicWorkflowOut> = {};
          for (const workflow of workflows) {
            tempMapping[workflow.workflow_id] = workflow;
            this.__addNameToMapping(workflow.workflow_id, workflow.name);
          }
          this.publicWorkflowMapping = tempMapping;
          return workflows;
        });
    },
    fetchWorkflows(onFinally?: () => void): Promise<WorkflowOut[]> {
      if (Object.keys(this.workflowMapping).length > 0) {
        onFinally?.();
      }
      return WorkflowService.workflowListWorkflows({})
        .then((response) => response.data!)
        .then((workflows) => {
          for (const workflow of workflows) {
            this.workflowMapping[workflow.workflow_id] = workflow;
            this.__addNameToMapping(workflow.workflow_id, workflow.name);
            for (const version of workflow.versions) {
              this.__addNameToMapping(
                version.workflow_version_id,
                version.version,
              );
            }
          }
          this.fetchWorkflowModes(
            workflows
              .map((workflow) => workflow.versions ?? [])
              .flat()
              .map((version) => version.modes ?? [])
              .flat(),
          );
          return workflows;
        })
        .finally(onFinally);
    },
    fetchOwnWorkflows(onFinally?: () => void): Promise<WorkflowOut[]> {
      const authStore = useUserStore();
      if (this.ownWorkflows.length > 0) {
        onFinally?.();
      }
      return WorkflowService.workflowListWorkflows({
        query: {
          version_status: Object.values(WorkflowVersionStatus),
          developer_id: authStore.currentUID,
        },
      })
        .then((response) => response.data!)
        .then((workflows) => {
          for (const workflow of workflows) {
            this.comprehensiveWorkflowMapping[workflow.workflow_id] = workflow;
            this.__addNameToMapping(workflow.workflow_id, workflow.name);
            for (const version of workflow.versions) {
              this.__addNameToMapping(
                version.workflow_version_id,
                version.version,
              );
            }
          }
          this.fetchWorkflowModes(
            workflows
              .map((workflow) => workflow.versions ?? [])
              .flat()
              .map((version) => version.modes ?? [])
              .flat(),
          );
          return workflows;
        })
        .finally(onFinally);
    },
    fetchWorkflowDocumentation(
      workflow_id: string,
      workflow_version_id: string,
      document = DocumentationEnum.USAGE_MD,
      mode_id?: string,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): Promise<any> {
      if (this.documentationFiles[workflow_version_id] == undefined) {
        this.documentationFiles[workflow_version_id] = {
          "changelog.md": undefined,
          "clowm_info.json": undefined,
          "input.md": undefined,
          "output.md": undefined,
          "parameter_schema.json": undefined,
          "usage.md": undefined,
          "CITATIONS.md": undefined,
        };
      }
      if (
        mode_id == undefined &&
        this.documentationFiles[workflow_version_id]?.[document] != undefined
      ) {
        return Promise.resolve(
          this.documentationFiles[workflow_version_id][document],
        );
      }
      return WorkflowVersionService.workflowVersionDownloadWorkflowDocumentation(
        {
          path: {
            wid: workflow_id,
            git_commit_hash: workflow_version_id,
          },
          query: {
            document: document,
            mode_id: mode_id,
          },
        },
      )
        .then((response) => response.data!)
        .then((response) => {
          this.documentationFiles[workflow_version_id][document] = response;
          return response;
        })
        .catch((err) => {
          if (document === DocumentationEnum.CLOWM_INFO_JSON) {
            this.documentationFiles[workflow_version_id][
              DocumentationEnum.CLOWM_INFO_JSON
            ] = {};
          }
          return err;
        });
    },
    fetchReviewableWorkflows(onFinally?: () => void): Promise<WorkflowOut[]> {
      if (this.reviewableWorkflows.length > 0) {
        onFinally?.();
      }
      return WorkflowService.workflowListWorkflows({
        query: {
          version_status: [WorkflowVersionStatus.CREATED],
        },
      })
        .then((response) => response.data!)
        .then((workflows) => {
          for (const workflow of workflows) {
            this.__addNameToMapping(workflow.workflow_id, workflow.name);
            for (const version of workflow.versions) {
              this.__addNameToMapping(
                version.workflow_version_id,
                version.version,
              );
            }
            if (
              this.comprehensiveWorkflowMapping[workflow.workflow_id] !=
              undefined
            ) {
              // merge cached workflow versions and fetched workflow versions
              for (const version of workflow.versions) {
                // if version is not present, push version into versions list
                if (
                  this.comprehensiveWorkflowMapping[
                    workflow.workflow_id
                  ].versions.findIndex(
                    (workflowVersion) =>
                      workflowVersion.workflow_version_id ===
                      version.workflow_version_id,
                  ) < 0
                ) {
                  this.comprehensiveWorkflowMapping[
                    workflow.workflow_id
                  ].versions.push(version);
                }
              }
            } else {
              this.comprehensiveWorkflowMapping[workflow.workflow_id] =
                workflow;
            }
          }
          return workflows;
        })
        .finally(onFinally);
    },
    fetchWorkflow(
      workflow_id: string,
      comprehensive = false,
      onFinally?: () => void,
    ): Promise<WorkflowOut> {
      if (
        comprehensive &&
        this.comprehensiveWorkflowMapping[workflow_id] != undefined
      ) {
        onFinally?.();
      }
      if (!comprehensive && this.workflowMapping[workflow_id] != undefined) {
        onFinally?.();
      }
      return WorkflowService.workflowGetWorkflow({
        path: {
          wid: workflow_id,
        },
        query: {
          version_status: comprehensive
            ? Object.values(WorkflowVersionStatus)
            : undefined,
        },
      })
        .then((response) => response.data!)
        .then((workflow) => {
          this.__addNameToMapping(workflow.workflow_id, workflow.name);
          for (const version of workflow.versions) {
            this.__addNameToMapping(
              version.workflow_version_id,
              version.version,
            );
          }
          if (comprehensive) {
            this.comprehensiveWorkflowMapping[workflow_id] = workflow;
          } else {
            this.workflowMapping[workflow_id] = workflow;
          }
          this.fetchWorkflowModes(
            workflow.versions.map((version) => version.modes ?? []).flat(),
          );
          return workflow;
        })
        .finally(onFinally);
    },
    fetchWorkflowModes(modeIds?: string[]): Promise<WorkflowModeOut[]> {
      if (modeIds == undefined) {
        return Promise.resolve([]);
      }
      const filteredIds = modeIds
        .filter((modeId) => !this.modeMapping[modeId]) // filter null modes and already present modes
        .filter(
          // filter unique workflow versions
          (modeId, index, array) =>
            array.findIndex((val) => val === modeId) === index,
        );
      return Promise.all(
        filteredIds.map((modeId) =>
          WorkflowModeService.workflowModeGetWorkflowMode({
            path: { mode_id: modeId },
          }).then((response) => response.data!),
        ),
      ).then((modes) => {
        for (const mode of modes) {
          this.__addNameToMapping(mode.mode_id, mode.name);
          this.modeMapping[mode.mode_id] = mode;
        }
        return modes;
      });
    },
    async setArbitraryWorkflow(
      workflow: WorkflowIn,
      docLocations: DocLocations,
    ) {
      const wid = crypto.randomUUID();
      await set(wid, {
        workflow: workflow,
        docs: docLocations,
      } as WorkflowWithDocLocations);
      return wid;
    },
    deprecateWorkflowVersion(
      workflow_id: string,
      version_id: string,
    ): Promise<WorkflowVersionOut> {
      return WorkflowVersionService.workflowVersionDeprecateWorkflowVersion({
        path: {
          wid: workflow_id,
          git_commit_hash: version_id,
        },
      })
        .then((response) => response.data!)
        .then((version) => {
          // Update version in workflowMapping
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            const versionIndex1 = this.workflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex1 > -1) {
              this.workflowMapping[workflow_id].versions[versionIndex1] =
                version;
            } else {
              this.workflowMapping[workflow_id].versions.push(version);
            }
          }

          // Update version in comprehensiveWorkflowMapping
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            const versionIndex2 = this.comprehensiveWorkflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex2 > -1) {
              this.comprehensiveWorkflowMapping[workflow_id].versions[
                versionIndex2
              ] = version;
            } else {
              this.comprehensiveWorkflowMapping[workflow_id].versions.push(
                version,
              );
            }
          }
          return version;
        });
    },
    updateWorkflow(
      workflow_id: string,
      version: WorkflowUpdate,
    ): Promise<WorkflowVersionOut> {
      return WorkflowService.workflowUpdateWorkflow({
        path: { wid: workflow_id },
        body: version,
      })
        .then((response) => response.data!)
        .then((updatedVersion) => {
          this.__addNameToMapping(
            updatedVersion.workflow_version_id,
            updatedVersion.version,
          );
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            this.comprehensiveWorkflowMapping[workflow_id].versions.push(
              updatedVersion,
            );
          }
          return updatedVersion;
        });
    },
    deleteWorkflow(workflow_id: string): Promise<void> {
      return WorkflowService.workflowDeleteWorkflow({
        path: { wid: workflow_id },
      })
        .then((response) => response.data!)
        .then(() => {
          useNameStore().deleteNameFromMapping(workflow_id);
          delete this.workflowMapping[workflow_id];
          delete this.comprehensiveWorkflowMapping[workflow_id];
        });
    },
    createWorkflow(workflow: WorkflowIn): Promise<WorkflowOut> {
      return WorkflowService.workflowCreateWorkflow({
        body: workflow,
      })
        .then((response) => response.data!)
        .then((workflowOut) => {
          this.comprehensiveWorkflowMapping[workflowOut.workflow_id] =
            workflowOut;
          this.__addNameToMapping(workflowOut.workflow_id, workflowOut.name);
          this.__addNameToMapping(
            workflow.git_commit_hash,
            workflowOut.versions[0].version,
          );
          return workflowOut;
        });
    },
    updateWorkflowCredentials(
      workflow_id: string,
      credentials: WorkflowCredentialsIn,
    ): Promise<void> {
      return WorkflowCredentialsService.workflowCredentialsUpdateWorkflowCredentials(
        {
          path: { wid: workflow_id },
          body: credentials,
        },
      )
        .then((response) => response.data!)
        .then(() => {
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            this.workflowMapping[workflow_id].private = true;
          }
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            this.comprehensiveWorkflowMapping[workflow_id].private = true;
          }
        });
    },
    deleteWorkflowCredentials(workflow_id: string): Promise<void> {
      return WorkflowCredentialsService.workflowCredentialsDeleteWorkflowCredentials(
        { path: { wid: workflow_id } },
      )
        .then((response) => response.data!)
        .then(() => {
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            this.workflowMapping[workflow_id].private = false;
          }
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            this.comprehensiveWorkflowMapping[workflow_id].private = false;
          }
        });
    },
    updateVersionIcon(
      workflow_id: string,
      version_id: string,
      icon: BodyWorkflowVersionUploadWorkflowVersionIcon,
    ): Promise<IconUpdateOut> {
      return WorkflowVersionService.workflowVersionUploadWorkflowVersionIcon({
        path: {
          wid: workflow_id,
          git_commit_hash: version_id,
        },
        body: icon,
      })
        .then((response) => response.data!)
        .then((response) => {
          // Update version in workflowMapping
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            const versionIndex1 = this.workflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex1 > -1) {
              this.workflowMapping[workflow_id].versions[
                versionIndex1
              ].icon_url = response.icon_url;
            }
          }

          // Update version in comprehensiveWorkflowMapping
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            const versionIndex2 = this.comprehensiveWorkflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex2 > -1) {
              setTimeout(() => {
                this.comprehensiveWorkflowMapping[workflow_id].versions[
                  versionIndex2
                ].icon_url = response.icon_url;
              }, 500);
            }
          }
          return response;
        });
    },
    deleteVersionIcon(workflow_id: string, version_id: string): Promise<void> {
      return WorkflowVersionService.workflowVersionDeleteWorkflowVersionIcon({
        path: {
          wid: workflow_id,
          git_commit_hash: version_id,
        },
      })
        .then((response) => response.data!)
        .then(() => {
          // Update version in workflowMapping
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            const versionIndex1 = this.workflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex1 > -1) {
              this.workflowMapping[workflow_id].versions[
                versionIndex1
              ].icon_url = null;
            }
          }

          // Update version in comprehensiveWorkflowMapping
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            const versionIndex2 = this.comprehensiveWorkflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex2 > -1) {
              this.comprehensiveWorkflowMapping[workflow_id].versions[
                versionIndex2
              ].icon_url = null;
            }
          }
        });
    },
    updateVersionStatus(
      workflow_id: string,
      version_id: string,
      status: WorkflowVersionStatus,
    ): Promise<void> {
      return WorkflowVersionService.workflowVersionUpdateWorkflowVersionStatus({
        path: {
          wid: workflow_id,
          git_commit_hash: version_id,
        },
        body: { status: status },
      })
        .then((response) => response.data!)
        .then(() => {
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            const versionIndex = this.comprehensiveWorkflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex > -1) {
              this.comprehensiveWorkflowMapping[workflow_id].versions[
                versionIndex
              ].status = status;
            }
          }
        });
    },
    updateWorkflowExtension(
      workflow_id: string,
      version_id: string,
      extension: ParameterExtensionOutput,
    ): Promise<WorkflowVersionOut> {
      return WorkflowVersionService.workflowVersionUpdateWorkflowVersionParameterExtension(
        {
          path: {
            wid: workflow_id,
            git_commit_hash: version_id,
          },
          body: extension,
        },
      )
        .then((response) => response.data!)
        .then((extension) => {
          // Update version in workflowMapping
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            const versionIndex1 = this.workflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex1 > -1) {
              this.workflowMapping[workflow_id].versions[versionIndex1] =
                extension;
            }
          }

          // Update version in comprehensiveWorkflowMapping
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            const versionIndex2 = this.comprehensiveWorkflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex2 > -1) {
              this.comprehensiveWorkflowMapping[workflow_id].versions[
                versionIndex2
              ] = extension;
            }
          }
          return extension;
        });
    },
    deleteWorkflowExtension(
      workflow_id: string,
      version_id: string,
    ): Promise<void> {
      return WorkflowVersionService.workflowVersionUpdateWorkflowVersionParameterExtension(
        {
          path: {
            wid: workflow_id,
            git_commit_hash: version_id,
          },
          body: {
            parameter_visibility:
              this.versionMapping[version_id]?.parameter_extension
                ?.parameter_visibility,
          },
        },
      )
        .then((response) => response.data!)
        .then((extension) => {
          // Update version in workflowMapping
          if (this.workflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, false);
          } else {
            const versionIndex1 = this.workflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex1 > -1) {
              this.workflowMapping[workflow_id].versions[versionIndex1] =
                extension;
            }
          }

          // Update version in comprehensiveWorkflowMapping
          if (this.comprehensiveWorkflowMapping[workflow_id] == undefined) {
            this.fetchWorkflow(workflow_id, true);
          } else {
            const versionIndex2 = this.comprehensiveWorkflowMapping[
              workflow_id
            ].versions.findIndex(
              (version) => version.workflow_version_id == version_id,
            );
            if (versionIndex2 > -1) {
              this.comprehensiveWorkflowMapping[workflow_id].versions[
                versionIndex2
              ] = extension;
            }
          }
        });
    },
    acceptWorkflowOtr(otr: OwnershipTransferRequestOut): Promise<WorkflowOut> {
      return WorkflowService.workflowAcceptWorkflowOtr({
        path: { wid: otr.target_id },
      })
        .then((response) => response.data!)
        .then((workflow) => {
          this.fetchWorkflow(workflow.workflow_id, true);
          return workflow;
        });
    },
  },
});
