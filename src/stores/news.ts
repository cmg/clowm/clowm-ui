import { defineStore } from "pinia";
import { type NewsIn, type NewsOut, NewsService } from "@/client";
import { useUserStore } from "@/stores/users";

export const useNewsStore = defineStore("news", {
  state: () =>
    ({
      newsMapping: {},
      __loaded: false,
    }) as {
      newsMapping: Record<string, NewsOut>;
      __loaded: boolean;
    },
  getters: {
    getCurrentNews(): NewsOut[] {
      const tmp = Object.values(this.newsMapping);
      tmp.sort((a, b) => (a.created_at < b.created_at ? 1 : -1));
      return tmp;
    },
  },
  actions: {
    createNewsEvent(newsIn: NewsIn): Promise<NewsOut> {
      return NewsService.newsCreateNews({
        body: newsIn,
      })
        .then((resp) => resp.data!)
        .then((news) => {
          this.newsMapping[news.news_id] = news;
          return news;
        });
    },
    fetchNewsById(newsId: string): Promise<NewsOut> {
      if (this.newsMapping[newsId] != undefined) {
        return Promise.resolve(this.newsMapping[newsId]);
      }
      return NewsService.newsGetNews({
        path: { nid: newsId },
      })
        .then((resp) => resp.data!)
        .then((news) => {
          this.newsMapping[newsId] = news;
          useUserStore().fetchUsernames([news.creator_id]);
          return news;
        });
    },
    fetchCurrentNews(onFinally?: () => void): Promise<NewsOut[]> {
      if (this.__loaded) {
        onFinally?.();
      }
      return NewsService.newsListLatestNews({})
        .then((resp) => resp.data!)
        .then((news) => {
          useUserStore().fetchUsernames(news.map((event) => event.creator_id));
          this.__loaded = true;
          const newMapping: Record<string, NewsOut> = {};
          for (const event of news) {
            newMapping[event.news_id] = event;
          }
          this.newsMapping = newMapping;
          return news;
        });
    },
  },
});
