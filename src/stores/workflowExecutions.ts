import { defineStore } from "pinia";
import type {
  AnonymizedWorkflowExecution,
  DevWorkflowExecutionIn,
  WorkflowExecutionIn,
  WorkflowExecutionOut,
} from "@/client/types.gen";
import { WorkflowExecutionStatus } from "@/client/types.gen";
import { WorkflowExecutionService, WorkflowService } from "@/client/sdk.gen";
import { useUserStore } from "@/stores/users";
import dayjs from "dayjs";
import { set, get } from "idb-keyval";
import type {
  FlatWorkflowParameters,
  WorkflowMetaParameters,
  TemporaryParams,
} from "@/types/WorkflowParameters";
import { parse as query_parse } from "qs";
import type { IdMapping } from "@/types/Store.ts";

function extractNextPageQuery(linkHeader?: string): string | undefined {
  if (linkHeader == undefined) {
    return undefined;
  }
  for (const link of linkHeader.split(",")) {
    const linkParts = link.split(";");
    if (linkParts[1]?.trim()?.split("=")?.[1]?.slice(1, -1) === "next") {
      return linkParts[0].slice(1, -1).split("?")[1];
    }
  }
  return undefined;
}

export const useWorkflowExecutionStore = defineStore("executions", {
  state: () =>
    ({
      executionMapping: {},
      anonymizedExecutions: [],
      parameters: {},
      __temporaryParameters: undefined,
      __temporaryMetaParameters: undefined,
    }) as {
      executionMapping: IdMapping<WorkflowExecutionOut>;
      anonymizedExecutions: AnonymizedWorkflowExecution[];
      parameters: IdMapping<FlatWorkflowParameters>;
      __temporaryParameters?: FlatWorkflowParameters;
      __temporaryMetaParameters?: IdMapping<string | undefined | null>;
    },
  getters: {
    executions(): WorkflowExecutionOut[] {
      return Object.values(this.executionMapping);
    },
  },
  actions: {
    fetchExecutionsRaw: async function* (
      executorId?: string,
      executionStatus?: Array<WorkflowExecutionStatus>,
      workflowVersionId?: string,
      workflowId?: string,
      startAfter?: number,
      startBefore?: number,
      perPage: number = 20,
      sort: "asc" | "desc" = "desc",
    ): AsyncGenerator<WorkflowExecutionOut[], void, unknown> {
      const userRepository = useUserStore();

      let response =
        await WorkflowExecutionService.workflowExecutionListWorkflowExecutions({
          query: {
            executor_id: executorId,
            execution_status: executionStatus,
            workflow_version_id: workflowVersionId,
            workflow_id: workflowId,
            start_after: startAfter,
            start_before: startBefore,
            per_page: perPage,
            sort: sort,
          },
        });
      userRepository.fetchUsernames(
        response.data!.map((execution) => execution.executor_id),
      );
      yield response.data!;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      let nextUrlQuery = extractNextPageQuery(response.headers["link"]);
      while (nextUrlQuery != undefined) {
        response =
          await WorkflowExecutionService.workflowExecutionListWorkflowExecutions(
            { query: query_parse(nextUrlQuery) },
          );
        userRepository.fetchUsernames(
          response.data!.map((execution) => execution.executor_id),
        );
        yield response.data!;
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        nextUrlQuery = extractNextPageQuery(response.headers["link"]);
      }
    },
    pushTemporaryParameters(
      params: FlatWorkflowParameters,
      metaParams?: WorkflowMetaParameters,
    ) {
      this.__temporaryParameters = params;
      this.__temporaryMetaParameters = metaParams ?? {};
    },
    popTemporaryParameters(): TemporaryParams {
      const params = this.__temporaryParameters;
      this.__temporaryParameters = undefined;
      const metaParams = this.__temporaryMetaParameters;
      this.__temporaryMetaParameters = undefined;
      return {
        metaParams: metaParams ?? {},
        params: params ?? {},
      };
    },
    fetchExecutionsForDevStatistics(
      onFinally?: () => void,
    ): Promise<AnonymizedWorkflowExecution[]> {
      if (this.anonymizedExecutions.length > 0) {
        onFinally?.();
      }
      const userStore = useUserStore();
      return WorkflowService.workflowGetDeveloperWorkflowStatistics({
        query: { developer_id: userStore.currentUID },
      })
        .then((response) => response.data!)
        .then((executions) => {
          this.anonymizedExecutions = executions;
          return executions;
        })
        .finally(onFinally);
    },
    async fetchExecutions(
      onFinally?: () => void,
    ): Promise<WorkflowExecutionOut[]> {
      if (this.executions.length > 0) {
        onFinally?.();
      }
      const userStore = useUserStore();
      const newMapping: IdMapping<WorkflowExecutionOut> = {};
      const allExecutions: WorkflowExecutionOut[] = [];
      try {
        for await (const executions of this.fetchExecutionsRaw(
          userStore.currentUID,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          50,
        )) {
          onFinally?.();
          for (const execution of executions) {
            newMapping[execution.execution_id] = execution;
          }
          allExecutions.push(...executions);
        }
        this.executionMapping = newMapping;
        return allExecutions;
      } finally {
        onFinally?.();
      }
    },
    fetchExecution(
      executionId: string,
      onFinally?: () => void,
    ): Promise<WorkflowExecutionOut> {
      if (this.executionMapping[executionId] != undefined) {
        onFinally?.();
      }
      return WorkflowExecutionService.workflowExecutionGetWorkflowExecution({
        path: { eid: executionId },
      })
        .then((response) => response.data!)
        .then((execution) => {
          this.executionMapping[execution.execution_id] = execution;
          return execution;
        })
        .finally(onFinally);
    },
    fetchExecutionParameters(
      executionId: string,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): Promise<Record<string, any>> {
      if (Object.keys(this.parameters).includes(executionId)) {
        return Promise.resolve(this.parameters[executionId]);
      }
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return get<Record<string, any>>(executionId)
        .then((parameters) => {
          if (parameters != undefined) {
            return parameters;
          }
          return WorkflowExecutionService.workflowExecutionGetWorkflowExecutionParams(
            { path: { eid: executionId } },
          )
            .then((response) => response.data!)
            .then((parameters) => {
              set(executionId, parameters);
              return parameters;
            });
        })
        .then((parameters) => {
          this.parameters[executionId] = parameters;
          return parameters;
        });
    },
    deleteExecution(executionId: string): Promise<void> {
      return WorkflowExecutionService.workflowExecutionDeleteWorkflowExecution({
        path: { eid: executionId },
      })
        .then((response) => response.data!)
        .then(() => {
          delete this.executionMapping[executionId];
        });
    },
    cancelExecution(executionId: string): Promise<void> {
      return WorkflowExecutionService.workflowExecutionCancelWorkflowExecution({
        path: { eid: executionId },
      })
        .then((response) => response.data!)
        .then(() => {
          if (this.executionMapping[executionId] == undefined) {
            this.fetchExecution(executionId);
          } else {
            this.executionMapping[executionId].status =
              WorkflowExecutionStatus.CANCELED;
            this.executionMapping[executionId].end_time = dayjs().unix();
          }
        });
    },
    startExecution(
      executionIn: WorkflowExecutionIn,
    ): Promise<WorkflowExecutionOut> {
      return WorkflowExecutionService.workflowExecutionStartWorkflow({
        body: executionIn,
      })
        .then((response) => response.data!)
        .then((execution) => {
          this.executionMapping[execution.execution_id] = execution;
          return execution;
        });
    },
    startDevExecution(
      executionIn: DevWorkflowExecutionIn,
    ): Promise<WorkflowExecutionOut> {
      return WorkflowExecutionService.workflowExecutionStartArbitraryWorkflow({
        body: executionIn,
      })
        .then((response) => response.data!)
        .then((execution) => {
          this.executionMapping[execution.execution_id] = execution;
          return execution;
        });
    },
  },
});
