import { defineStore } from "pinia";
import { useUserStore } from "@/stores/users";
import type { S3Key } from "@/client/types.gen";
import { S3KeyService } from "@/client/sdk.gen";
import { useS3ObjectStore } from "@/stores/s3objects";
import type { IdMapping } from "@/types/Store.ts";

export const useS3KeyStore = defineStore("s3keys", {
  state: () =>
    ({
      keyMapping: {},
    }) as {
      keyMapping: IdMapping<S3Key>;
    },
  getters: {
    keys(): S3Key[] {
      const tempList = Object.values(this.keyMapping);
      tempList.sort((keyA, keyB) =>
        keyA.access_key > keyB.access_key ? 1 : -1,
      );
      return tempList;
    },
  },
  actions: {
    fetchS3Keys(onFinally?: () => void): Promise<S3Key[]> {
      if (this.keys.length > 0) {
        onFinally?.();
      }
      return S3KeyService.s3KeyGetUserKeys({
        path: { uid: useUserStore().currentUID },
      })
        .then((response) => response.data!)
        .then((keys) => {
          const s3ObjectRepository = useS3ObjectStore();
          s3ObjectRepository.updateS3Client(keys[0]);
          const newMapping: IdMapping<S3Key> = {};
          for (const key of keys) {
            newMapping[key.access_key] = key;
          }
          this.keyMapping = newMapping;
          return keys;
        })
        .finally(onFinally);
    },
    deleteS3Key(access_id: string): Promise<void> {
      const userRepository = useUserStore();
      return S3KeyService.s3KeyDeleteUserKey({
        path: {
          access_id: access_id,
          uid: userRepository.currentUID,
        },
      })
        .then((response) => response.data!)
        .then(() => {
          const s3ObjectRepository = useS3ObjectStore();
          delete this.keyMapping[access_id];
          s3ObjectRepository.updateS3Client(this.keys[0]);
        });
    },
    createS3Key(): Promise<S3Key> {
      const userRepository = useUserStore();
      return S3KeyService.s3KeyCreateUserKey({
        path: { uid: userRepository.currentUID },
      })
        .then((response) => response.data!)
        .then((key) => {
          this.keyMapping[key.access_key] = key;
          return key;
        });
    },
  },
});
