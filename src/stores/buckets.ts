import { defineStore } from "pinia";
import { BucketPermissionService, BucketService } from "@/client/sdk.gen";
import type {
  BucketOut,
  BucketIn,
  OwnershipTransferRequestOut,
  BucketPermissionOut,
  BucketPermissionIn,
  BucketPermissionParameters,
  BucketType,
} from "@/client/types.gen";
import { useUserStore } from "@/stores/users";
import type { IdMapping } from "@/types/Store.ts";

export const useBucketStore = defineStore("buckets", {
  state: () =>
    ({
      bucketMapping: {},
      ownPermissions: {},
      bucketPermissionsMapping: {},
    }) as {
      bucketMapping: IdMapping<BucketOut>;
      ownPermissions: IdMapping<BucketPermissionOut>;
      bucketPermissionsMapping: IdMapping<BucketPermissionOut[]>;
    },
  getters: {
    buckets(): BucketOut[] {
      const tempList = Object.values(this.bucketMapping);
      tempList.sort((bucketA, bucketB) => {
        return bucketA.name > bucketB.name ? 1 : -1;
      });
      return tempList;
    },
    ownBuckets(): BucketOut[] {
      const authStore = useUserStore();
      return this.buckets.filter(
        (bucket) => bucket.owner_id === authStore.currentUID,
      );
    },
    getBucketPermissions(): (bucketName: string) => BucketPermissionOut[] {
      return (bucketName) => this.bucketPermissionsMapping[bucketName] ?? [];
    },
    permissionFeatureAllowed(): (bucketName: string) => boolean {
      const authStore = useUserStore();
      return (bucketName) => {
        // If a permission for the bucket exist, then false
        if (this.ownPermissions[bucketName] != undefined) {
          return false;
        }
        // If the bucket doesn't exist, then false
        return this.bucketMapping[bucketName]?.owner_id == authStore.currentUID;
      };
    },
    writableBucket(): (bucketName: string) => boolean {
      const authStore = useUserStore();
      return (bucketName) => {
        // If this is a foreign bucket, check that the user has write permission
        if (this.ownPermissions[bucketName] != undefined) {
          return this.ownPermissions[bucketName].scopes.includes("write");
        }
        return this.bucketMapping[bucketName]?.owner_id == authStore.currentUID;
      };
    },
    readableBucket(): (bucketName: string) => boolean {
      const authStore = useUserStore();
      return (bucketName) => {
        // If this is a foreign bucket, check that the user has read permission
        if (this.ownPermissions[bucketName] != undefined) {
          return this.ownPermissions[bucketName].scopes.includes("read");
        }
        return this.bucketMapping[bucketName]?.owner_id == authStore.currentUID;
      };
    },
  },
  actions: {
    fetchBuckets(
      ownerId?: string,
      bucketType?: BucketType,
    ): Promise<BucketOut[]> {
      return BucketService.bucketListBuckets({
        query: { owner_id: ownerId, bucket_type: bucketType },
      })
        .then((response) => response.data!)
        .then((buckets) => {
          const userRepository = useUserStore();
          userRepository.fetchUsernames(
            buckets.map((bucket) => bucket.owner_id),
          );
          return buckets;
        });
    },
    fetchOwnPermissions(
      onFinally?: () => void,
    ): Promise<BucketPermissionOut[]> {
      const authStore = useUserStore();
      if (Object.keys(this.ownPermissions).length > 0) {
        onFinally?.();
      }
      return BucketPermissionService.bucketPermissionListPermissionsPerUser({
        path: { uid: authStore.currentUID },
      })
        .then((response) => response.data!)
        .then((permissions) => {
          const new_permissions: IdMapping<BucketPermissionOut> = {};
          for (const perm of permissions) {
            new_permissions[perm.bucket_name] = perm;
          }
          this.ownPermissions = new_permissions;
          return permissions;
        })
        .finally(onFinally);
    },
    deleteOwnPermission(bucketName: string) {
      delete this.ownPermissions[bucketName];
      delete this.bucketMapping[bucketName];
    },
    fetchOwnBuckets(onFinally?: () => void): Promise<BucketOut[]> {
      if (this.ownBuckets.length > 0) {
        onFinally?.();
      }
      const userStore = useUserStore();
      return this.fetchBuckets(userStore.currentUID)
        .then((buckets) => {
          for (const bucketName of Object.keys(this.bucketMapping)) {
            if (
              this.bucketMapping[bucketName].owner_id === userStore.currentUID
            ) {
              delete this.bucketMapping[bucketName];
            }
          }
          for (const bucket of buckets) {
            this.bucketMapping[bucket.name] = bucket;
          }
          return buckets;
        })
        .finally(onFinally);
    },
    fetchBucket(
      bucketName: string,
      onFinally?: () => void,
    ): Promise<BucketOut> {
      if (this.bucketMapping[bucketName] != undefined) {
        onFinally?.();
      }
      return BucketService.bucketGetBucket({
        path: { bucket_name: bucketName },
      })
        .then((response) => response.data!)
        .then((bucket) => {
          this.bucketMapping[bucket.name] = bucket;
          return bucket;
        })
        .finally(onFinally);
    },
    deleteBucket(bucketName: string): Promise<void> {
      return BucketService.bucketDeleteBucket({
        path: { bucket_name: bucketName },
        query: { force_delete: true },
      })
        .then((response) => response.data!)
        .then(() => {
          delete this.bucketMapping[bucketName];
        });
    },
    createBucket(bucket: BucketIn): Promise<BucketOut> {
      return BucketService.bucketCreateBucket({
        body: bucket,
      })
        .then((response) => response.data!)
        .then((createdBucket) => {
          this.bucketMapping[createdBucket.name] = createdBucket;
          return createdBucket;
        });
    },
    fetchBucketPermissions(
      bucketName: string,
      onFinally?: () => void,
    ): Promise<BucketPermissionOut[]> {
      const authStore = useUserStore();
      if (authStore.foreignUser) {
        return Promise.resolve([]).finally(onFinally);
      }
      if (this.bucketPermissionsMapping[bucketName] != undefined) {
        onFinally?.();
      }
      return BucketPermissionService.bucketPermissionListPermissionsPerBucket({
        path: { bucket_name: bucketName },
      })
        .then((response) => response.data!)
        .then((permissions) => {
          this.bucketPermissionsMapping[bucketName] = permissions;
          return permissions;
        })
        .finally(onFinally);
    },
    deleteBucketPermission(bucketName: string, uid: string): Promise<void> {
      return BucketPermissionService.bucketPermissionDeletePermission({
        path: {
          bucket_name: bucketName,
          uid: uid,
        },
      })
        .then((response) => response.data!)
        .then(() => {
          const userRepository = useUserStore();
          if (uid == userRepository.currentUID) {
            this.deleteOwnPermission(bucketName);
          }
          if (this.bucketPermissionsMapping[bucketName] == undefined) {
            this.fetchBucketPermissions(bucketName);
          } else {
            this.bucketPermissionsMapping[bucketName] =
              this.bucketPermissionsMapping[bucketName].filter(
                (permission) => permission.uid != uid,
              );
          }
        });
    },
    createBucketPermission(
      permissionIn: BucketPermissionIn,
    ): Promise<BucketPermissionOut> {
      return BucketPermissionService.bucketPermissionCreatePermission({
        body: permissionIn,
      })
        .then((response) => response.data!)
        .then((permission) => {
          if (
            this.bucketPermissionsMapping[permission.bucket_name] == undefined
          ) {
            this.fetchBucketPermissions(permission.bucket_name);
          } else {
            this.bucketPermissionsMapping[permission.bucket_name].push(
              permission,
            );
          }
          return permission;
        });
    },
    updateBucketPermission(
      bucketName: string,
      uid: string,
      permissionParams: BucketPermissionParameters,
    ): Promise<BucketPermissionOut> {
      return BucketPermissionService.bucketPermissionUpdatePermission({
        path: {
          bucket_name: bucketName,
          uid: uid,
        },
        body: permissionParams,
      })
        .then((response) => response.data!)
        .then((permissionOut) => {
          if (this.bucketPermissionsMapping[bucketName] == undefined) {
            this.fetchBucketPermissions(bucketName);
          } else {
            const index = this.bucketPermissionsMapping[bucketName].findIndex(
              (permission) => (permission.uid = uid),
            );
            if (index > -1) {
              this.bucketPermissionsMapping[bucketName][index] = permissionOut;
            } else {
              this.bucketPermissionsMapping[bucketName].push(permissionOut);
            }
          }
          return permissionOut;
        });
    },
    async updatePublicState(
      bucketName: string,
      public_: boolean,
    ): Promise<BucketOut> {
      const bucket = (
        await BucketService.bucketUpdateBucketPublicState({
          path: { bucket_name: bucketName },
          body: {
            public: public_,
          },
        })
      ).data!;
      this.bucketMapping[bucketName] = bucket;
      return bucket;
    },
    updateBucketLimits(
      bucketName: string,
      size_limit?: number | null,
      object_limit?: number | null,
    ): Promise<BucketOut> {
      return BucketService.bucketUpdateBucketLimits({
        path: { bucket_name: bucketName },
        body: {
          size_limit: size_limit,
          object_limit: object_limit,
        },
      }).then((response) => response.data!);
    },
    acceptBucketOtr(otr: OwnershipTransferRequestOut): Promise<BucketOut> {
      return BucketService.bucketAcceptBucketOtr({
        path: { bucket_name: otr.target_id },
      })
        .then((response) => response.data!)
        .then((bucket) => {
          this.bucketMapping[bucket.name] = bucket;
          this.bucketPermissionsMapping[bucket.name] =
            this.bucketPermissionsMapping[bucket.name]?.filter(
              (permission) => permission.uid != otr.new_owner_uid,
            );
          return bucket;
        });
    },
  },
});
