(function (window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["s3Url"] = "${S3_URL}";
  window["env"]["apiUrl"] = "${API_BASE_URL}";
  window["env"]["devSystem"] = "${DEV_SYSTEM}";
  window["env"]["matomoHost"] = "${MATOMO_HOST}";
  window["env"]["matomoSiteId"] = "${MATOMO_SITE_ID}";
})(this);
