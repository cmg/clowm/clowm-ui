import { createApp } from "vue";
import { createPinia } from "pinia";
import type { PossibleThemes } from "@/stores/settings";
import { useSettingsStore } from "@/stores/settings";

import App from "./App.vue";
import router from "./router";
import { Chart, Colors } from "chart.js";
import zoomPlugin from "chartjs-plugin-zoom";
import VueMatomo from "vue-matomo";
import { environment } from "@/environment";

Chart.register(zoomPlugin, Colors);

import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import duration from "dayjs/plugin/duration";
import isSameOrBefore from "dayjs/plugin/isSameOrBefore";
import "dayjs/locale/en-gb";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";

dayjs.extend(duration);
dayjs.extend(relativeTime); // use plugin
dayjs.extend(isSameOrBefore);
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(dayjs.tz.guess());

import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-free/css/fontawesome.css";
import "@fortawesome/fontawesome-free/css/solid.css";
import "@fortawesome/fontawesome-free/css/brands.css";

import "./assets/main.css";

import { globalCookiesConfig } from "vue3-cookies";

globalCookiesConfig({
  expireTimes: "8d",
  domain: window.location.hostname,
});

const app = createApp(App);

declare global {
  // tslint:disable-next-line:interface-name
  interface Window {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _paq: any[];
  }
}

if (environment.MATOMO_HOST && environment.MATOMO_SITE_ID) {
  app.use(VueMatomo, {
    host: environment.MATOMO_HOST,
    siteId: environment.MATOMO_SITE_ID,
    disableCookies: false,
  });
}

app.use(createPinia());
app.use(router);

const theme = localStorage.getItem("theme") as PossibleThemes;
if (theme != undefined) {
  useSettingsStore().setTheme(theme);
} else if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
  useSettingsStore().setTheme("dark");
}

app.mount("#app");

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Modal, Collapse, Dropdown, Tooltip, Toast } from "bootstrap";
