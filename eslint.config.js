import pluginVue from "eslint-plugin-vue";
import vueTsEslintConfig from "@vue/eslint-config-typescript";
import prettierConfig from "@vue/eslint-config-prettier";

import { includeIgnoreFile } from "@eslint/compat";
import path from "node:path";
import { fileURLToPath } from "node:url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const gitignorePath = path.resolve(__dirname, ".gitignore");

export default [
  // add more generic rulesets here, such as:
  // js.configs.recommended,
  ...pluginVue.configs["flat/recommended"],
  ...vueTsEslintConfig(),
  prettierConfig,
  includeIgnoreFile(gitignorePath),
  {
    files: ["**/*.ts", "**/*.js", "**/*.vue"],
  },
  {
    files: ["src/components/parameter-schema/**/*.vue"],
    rules: {
      "vue/require-default-prop": "off",
      "vue/require-prop-types": "off",
    },
  },
  {
    files: ["src/client/*.ts"],
    rules: {
      "@typescript-eslint/no-explicit-any": "off",
    },
  },
  {
    ignores: ["src/utils/md5.js"],
  },
];
