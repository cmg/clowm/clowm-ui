import { defineConfig } from "@hey-api/openapi-ts";

export default defineConfig({
  input: "./openapi-clowm.json",
  output: {
    lint: "eslint",
    format: "prettier",
    path: "src/client",
  },
  plugins: [
    { name: "@hey-api/transformers", dates: false },
    { enums: "typescript", name: "@hey-api/typescript" },
    { asClass: true, name: "@hey-api/sdk", transformer: true },
    { name: "@hey-api/client-axios", throwOnError: true },
  ],
});
